<?php
defined('ABSPATH') or die("ERROR: You do not have permission to access this page");

if(!class_exists('AAOStracts_EmailsTemplates')){
    require_once( AAOSTRACTS_PLUGIN_DIR . 'inc/aaostracts_classes.php' );
}

if(is_admin() && isset($_GET['tab']) && ($_GET["tab"]=="emails")){
    if(isset($_GET['task'])){
        $task = sanitize_text_field($_GET['task']);
        $id = intval($_GET['id']);
        switch($task){
            case 'edit':
                aaostracts_editEmail($id);
                break;
            default :
                aaostracts_showEmails();
                break;
        }
    }else{
        aaostracts_showEmails();
    }
}

function aaostracts_editEmail($id) {
    global $wpdb;

    if($_POST){
        $template_name = sanitize_text_field($_POST["template_name"]);
        $from_name = sanitize_text_field($_POST["from_name"]);
        $from_email = sanitize_text_field($_POST["from_email"]);
        $email_subject = sanitize_text_field($_POST["email_subject"]);
        $email_body = wp_kses_post($_POST["email_body"]);
        $wpdb->show_errors();
        $data = array(
            'name' => $template_name, 'subject' => $email_subject, 'message' => $email_body,
            'from_name' => $from_name, 'from_email' => $from_email);
        $where = array( 'ID' => $id);
        $wpdb->update($wpdb->prefix."aaostracts_emailtemplates", $data, $where);

        aaostracts_redirect('?page=aaostracts&tab=emails');

    }else{
        aaostracts_getEditView('EmailTemplate', $id, 'backend');
    }
}


function aaostracts_showEmails(){ ?>
        <form id="showReviews" method="get">
            <input type="hidden" name="page" value="aaostracts" />
            <input type="hidden" name="tab" value="emails" />
            <?php
            $showEmails = new AAOStracts_EmailsTemplates();
            $showEmails->prepare_items();
            $showEmails->display(); ?>
            </form>
    <?php
}

