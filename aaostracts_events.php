<?php
defined('ABSPATH') or die("ERROR: You do not have permission to access this page");
if(!class_exists('AAOStract_Abstracts_Table')){
    require_once( AAOSTRACTS_PLUGIN_DIR . 'inc/aaostracts_classes.php' );
}
    if($_GET["tab"]=="events") {
        if(isset($_GET["task"])){
            $task = $_GET["task"];
            switch($task){
                case 'new':
                    aaostracts_addEvent();
                    break;
                case 'edit':
                    aaostracts_editEvent($_GET["id"]);
                    break;
                case 'delete':
                    aaostracts_deleteEvent($_GET['id']);
                default :
                    aaostracts_showEvents();
                    break;
            }
        }else{
            aaostracts_showEvents();
        }
    }
    else{
        echo "You do not have permission to view this page";
    }

function aaostracts_addEvent() {
    global $wpdb;
    $tab = "?page=aaostracts&tab=events";
        if ($_POST) {
            $abs_event_name = sanitize_text_field($_POST["abs_event_name"]);
            $abs_event_desc = wp_kses_post($_POST["abs_event_desc"]);
            $abs_event_address = sanitize_text_field($_POST["abs_event_address"]);
            $abs_event_host = sanitize_text_field($_POST["abs_event_host"]);
            $abs_event_start = sanitize_text_field($_POST["abs_event_start"]);
            $abs_event_end = sanitize_text_field($_POST["abs_event_end"]);
            $abs_event_deadline = sanitize_text_field($_POST["abs_event_deadline"]);
            // get and sanitize topics
            if(sizeof($_POST["topics"])>1) {
                foreach($_POST["topics"] as $key=>$topic) {
                    $topic = sanitize_text_field($_POST["topics"][$key]);
                    $topics[] = $topic;
                }
            $event_topics = implode(', ',$topics);
            } else {
                $topic = sanitize_text_field($_POST["topics"][0]);
                $event_topics = $topic;
            }
            $wpdb->show_errors();
            $wpdb->query($wpdb->prepare("INSERT INTO ".$wpdb->prefix."aaostracts_events (name, description, address, host, topics, start_date, end_date, deadline)
                                        VALUES (%s,%s,%s,%s,%s,%s,%s,%s)",$abs_event_name,$abs_event_desc,$abs_event_address,$abs_event_host,$event_topics,$abs_event_start,$abs_event_end,$abs_event_deadline));

            aaostracts_redirect($tab);
        }
        else {
            aaostracts_getAddView('Event', null, 'backend');
        }
}

function aaostracts_editEvent($id){
    global $wpdb;
    $tab = "?page=aaostracts&tab=events";
        if ($_POST) {
            $abs_event_name = sanitize_text_field($_POST["abs_event_name"]);
            $abs_event_desc = wp_kses_post($_POST["abs_event_desc"]);
            $abs_event_address = sanitize_text_field($_POST["abs_event_address"]);
            $abs_event_host = sanitize_text_field($_POST["abs_event_host"]);
            $abs_event_start = sanitize_text_field($_POST["abs_event_start"]);
            $abs_event_end = sanitize_text_field($_POST["abs_event_end"]);
            $abs_event_deadline = sanitize_text_field($_POST["abs_event_deadline"]);
            // get and sanitize topics
            if(sizeof($_POST["topics"])>1) {
                foreach($_POST["topics"] as $key=>$topic) {
                    $topic = sanitize_text_field($_POST["topics"][$key]);
                    $topics[] = $topic;
                }
            $event_topics = implode(', ',$topics);
            } else {
                $topic = sanitize_text_field($_POST["topics"][0]);
                $event_topics = $topic;
            }
            $wpdb->show_errors();
            $wpdb->query("UPDATE ".$wpdb->prefix."aaostracts_events
                        SET name = '$abs_event_name', description = '$abs_event_desc', address = '$abs_event_address', "
                    . "host = '$abs_event_host', topics = '$event_topics', start_date = '$abs_event_start', "
                    . "end_date = '$abs_event_end', deadline = '$abs_event_deadline' "
                    . "WHERE event_id = $id");

            aaostracts_redirect($tab);
        }
        else {
            aaostracts_getEditView('Event', $id, 'backend');
        }
}

function aaostracts_showEvents(){ ?>
<div class="aaostracts container-fluid aaostracts-admin-container">
    <h3><?php echo apply_filters('aaostracts_title_filter', __('Events','aaostracts'), 'events');?> <a href="?page=aaostracts&tab=events&task=new" class="aaostracts btn btn-primary" /><?php _e('Add New', 'aaostracts');?></a></h3>
</div>
    <form id="showsEvents" method="get">
        <input type="hidden" name="page" value="aaostracts" />
        <input type="hidden" name="tab" value="events" />
           <?php
                $showEvents = new AAOStract_Events_Table();
                $showEvents ->prepare_items();
                $showEvents ->display();
            ?>
    </form>


   <?php
}

function aaostracts_deleteEvent($id){
    global $wpdb;
        $wpdb->show_errors();
        $wpdb->query("delete from ".$wpdb->prefix."aaostracts_events where event_id=".$id);
        ?>
    <div id="message" class="updated fade"><p><strong><?php _e('Event deleted', 'aaostracts');?>.</strong></p></div>
        <?php
}

function aaostracts_loadTopics(){
    global $wpdb;
    if($_POST[event_id]){
        $event_id = intval($_POST[event_id]);
        $event = $wpdb->get_row("SELECT topics FROM ".$wpdb->prefix."aaostracts_events Where event_id = $event_id");
        $topics = explode(',',$event->topics);
        foreach($topics as $topic){ ?>
            <option value="<?php echo esc_attr($topic);?>"><?php echo esc_attr($topic);?></option>;
        <?php }
    }else{
   _e("Error!", 'aaostracts');
    }
   die();
}