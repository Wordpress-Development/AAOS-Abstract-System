<?php
defined('ABSPATH') or die("ERROR: You do not have permission to access this page");

global $wpdb;
$abstracts = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS * FROM ".$wpdb->prefix."aaostracts_abstracts ORDER BY abstract_id DESC LIMIT 0,5");
$abs_count = $wpdb->get_var("SELECT FOUND_ROWS()");
$reviews = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS * FROM ".$wpdb->prefix."aaostracts_reviews ORDER BY review_id DESC LIMIT 0,5");
$approved_abs = $wpdb->get_var("SELECT COUNT(*) FROM ".$wpdb->prefix."aaostracts_abstracts WHERE status = 'Approved'");
$rejected_abs = $wpdb->get_var("SELECT COUNT(*) FROM ".$wpdb->prefix."aaostracts_abstracts WHERE status = 'Rejected'");
$pending_abs = $wpdb->get_var("SELECT COUNT(*) FROM ".$wpdb->prefix."aaostracts_abstracts WHERE status = 'Pending'");
$attachments = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS * FROM ".$wpdb->prefix."aaostracts_attachments ORDER BY attachment_id DESC LIMIT 0,5");
$attachments_count = $wpdb->get_var("SELECT FOUND_ROWS()");
?>

<br>
<div class="aaostracts container-fluid aaostracts-admin-container">
    <div class="aaostracts row">
    <div class="aaostracts col-xs-12 col-md-9">
        <div class="aaostracts panel panel-primary">
            <div class="aaostracts panel-heading">
                <h4><?php echo apply_filters('aaostracts_title_filter', __('Recent Abstracts','aaostracts'), 'recent_abstracts');?></h4>
            </div>
            <div class="aaostracts panel-body">
                <table class="aaostracts table table-striped">
                    <thead>
                        <tr>
                        <th><?php _e('Abstract Title', 'aaostracts'); ?></th>
                        <?php if(get_option('aaostracts_show_author')){ ?>
                        <th><?php _e('Author', 'aaostracts'); ?></th>
                        <th><?php _e('Author Email', 'aaostracts'); ?></th>
                        <?php } ?>
                        <th><?php _e('Submit by', 'aaostracts'); ?></th>
                        <th><?php _e('Date', 'aaostracts'); ?></th>
                        </tr>
                    </thead>
                        <tbody>
                    <?php
                    foreach($abstracts as $abstract) {
                    ?>
                            <tr>
                                <td><a href="?page=aaostracts&tab=abstracts&task=edit&id=<?php echo $abstract->abstract_id ?>"><?php echo $abstract->title ?></a></td>
                                <?php if(get_option('aaostracts_show_author')){ ?>
                                <td><?php echo $abstract->author?></td>
                                <td><a href="mailto:<?php echo $abstract->author_email?>"><?php echo $abstract->author_email?></a></td>
                                <?php } ?>
                                <td><?php
                                    $user_info = get_userdata($abstract->submit_by);
                                    if($user_info){
                                        echo $user_info->display_name;
                                    }else{
                                        _e('--Deleted--', 'aaostracts');
                                    }
                                    ?>
                                </td>

                                <td><?php echo date_i18n(get_option('date_format') . ' ' . get_option('time_format'), strtotime($abstract->submit_date)); ?></td>

                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>

            </div>

        </div>

    </div>


    <div class="aaostracts col-xs-12 col-md-3">
        <div class="aaostracts panel panel-primary">
            <div class="aaostracts panel-heading">
                <h4><?php echo apply_filters('aaostracts_title_filter', __('Analytics','aaostracts'), 'analytics');?></h4>
            </div>
            <div class="aaostracts panel-body">
                <table class="aaostracts table table-striped">
                    <tr><td><?php _e('Total Abstracts', 'aaostracts'); ?></td><td><?php echo esc_attr($abs_count); ?></td></tr>
                    <tr><td><?php _e('Approved', 'aaostracts'); ?></td><td><?php echo $approved_abs; ?></td></tr>
                    <tr><td><?php _e('Pending', 'aaostracts'); ?></td><td><?php echo $pending_abs; ?></td></tr>
                    <tr><td><?php _e('Rejected', 'aaostracts'); ?></td><td><?php echo $rejected_abs; ?></td></tr>
                    <tr><td><?php _e('Attachments', 'aaostracts'); ?></td><td><?php echo esc_attr($attachments_count); ?></td></tr>
                </table>
            </div>
        </div>
    </div>

    <div class="aaostracts col-xs-12 col-md-9">
        <div class="aaostracts panel panel-primary">
            <div class="aaostracts panel-heading">
                <h4><?php echo apply_filters('aaostracts_title_filter', __('Recent Reviews','aaostracts'), 'recent_reviews');?></h4>
            </div>
            <div class="aaostracts panel-body">
                <table class="aaostracts table table-striped">
                        <thead>
                        <tr>
                        <th><?php _e('Recent Reviews', 'aaostracts'); ?></th>
                        <th><?php _e('Comments', 'aaostracts'); ?></th>
                        <th><?php _e('Reviewer', 'aaostracts'); ?></th>
                        <th><?php _e('Suggested Status', 'aaostracts'); ?></th>
                        <th><?php _e('Date', 'aaostracts'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                    <?php foreach($reviews as $review) {
                        $currentAbstract = $wpdb->get_row("SELECT title FROM ".$wpdb->prefix."aaostracts_abstracts WHERE abstract_id = $review->abstract_id");
                        ?>
                            <tr>
                                <td><a href="?page=aaostracts&tab=reviews&task=edit&id=<?php echo$review->review_id?>"><?php echo $currentAbstract->title; ?></a></td>
                                <td><?php echo $review->comments; ?></td>
                            <?php
                            $user_info = get_userdata($review->user_id);
                            if($user_info){
                                $reviewer = $user_info->display_name;
                            }
                            else{
                                $reviewer = __("Not Assigned",'aaostracts');
                            }
                        ?>
                                <td><?php echo $reviewer; ?></td>
                                 <td><?php echo $review->status; ?></td>
                                <td><?php echo date_i18n(get_option('date_format') . ' ' . get_option('time_format'), strtotime($review->review_date)); ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>

    <div class="aaostracts col-xs-12 col-md-3">
        <div class="aaostracts panel panel-primary">
            <div class="aaostracts panel-heading">
                <h4><?php echo apply_filters('aaostracts_title_filter', __('Help us Improve','aaostracts'), 'help_improve');?></h4>
            </div>
            <div class="aaostracts panel-body">
                <p><a href="http://www.aaostracts.com/wishlist" target="_blank"><?php _e('Suggest', 'aaostracts'); ?></a> <?php _e('features', 'aaostracts'); ?>.</p>
                <p><a href="http://wordpress.org/plugins/wp-abstracts-manuscripts-manager/" target="_blank"><?php _e('Rate', 'aaostracts'); ?></a> <?php _e('the plugin 5 stars on WordPress.org.', 'aaostracts');?></p>
                <p><a href="http://www.facebook.com/aaostracts" target="_blank"><?php _e('Like us', 'aaostracts'); ?></a> on Facebook. </p>
            </div>
        </div>
    </div>

    <div class="aaostracts col-xs-12 col-md-9">
        <div class="aaostracts panel panel-primary">
            <div class="aaostracts panel-heading">
                <h4><?php echo apply_filters('aaostracts_title_filter', __('Recent Attachments','aaostracts'), 'recent_attachments');?></h4>
            </div>
            <div class="aaostracts panel-body">
                <table class="aaostracts table table-striped">
                    <thead>
                        <tr>
                        <th><?php _e('File Name', 'aaostracts'); ?></th>
                        <th><?php _e('File Type', 'aaostracts'); ?></th>
                        <th><?php _e('File Size', 'aaostracts'); ?></th>
                        </thead>
                        <tbody>
                        <?php foreach($attachments as $attachment) { ?>
                            <tr>
                                <td><?php echo $attachment->filename ?></td>
                                <td><?php echo $attachment->filetype?></td>
                                <td><?php echo number_format(($attachment->filesize/1048576), 2);?>MB</td>
                            </tr>
                        <?php } ?>
                        </tbody>
                </table>
            </div>
        </div>

    </div>
    </div>
</div>