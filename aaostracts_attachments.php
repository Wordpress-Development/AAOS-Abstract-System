<?php
defined('ABSPATH') or die("ERROR: You do not have permission to access this page");

if(!class_exists('AAOStract_Attachments_Table')){
    require_once( AAOSTRACTS_PLUGIN_DIR . 'inc/aaostracts_classes.php' );
}
if(is_admin() && isset($_GET['tab']) && $_GET["tab"] == "attachments"){
    if(isset($_GET["task"]) && $_GET["task"]){

        $task = sanitize_text_field($_GET["task"]);

        switch($task){
            case 'delete':
                if(current_user_can(AAOSTRACTS_ACCESS_LEVEL)){
                    aaostracts_deleteAttachment(intval($_GET['id']), true );
                }
            default :
                aaostracts_showAttachments();
        }
    }else{
        aaostracts_showAttachments();
    }
}

function aaostracts_deleteAttachment($id, $message){
    global $wpdb;
    $wpdb->query("DELETE FROM " . $wpdb->prefix."aaostracts_attachments WHERE attachment_id = " . $id);
    if($message){
        aaostracts_showMessage("Attachment ID ". $id . " was successfully deleted", 'alert-success');
    }
}

function aaostracts_showAttachments(){ ?>
    <div class="aaostracts container-fluid aaostracts-admin-container">
        <h3><?php echo apply_filters('aaostracts_title_filter', __('Attachments','aaostracts'), 'abstracts');?>  <a href="?page=aaostracts&tab=attachments&task=download&type=zip" role="button" class="aaostracts btn btn-primary"><?php _e('Export Attachments', 'aaostracts');?></a></h3>
    </div>
    <form id="showAttachments" method="get">
    <input type="hidden" name="page" value="aaostracts" />
    <input type="hidden" name="tab" value="attachments" />
       <?php
            $showAttachments = new AAOStract_Attachments_Table();
            $showAttachments->prepare_items();
            $showAttachments->display();
        ?>
</form>
    <?php
}