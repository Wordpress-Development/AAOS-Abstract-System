<?php
/*
Plugin Name:AAOStract
Plugin URI: http://www.aaostracts.com
Description: Allow and manage Abstracts and Manuscripts submissions on your site. </br>Manage authors, reviews, events and more
Version: 
Author: Austin Vern Songer
Author URI: http://www.austinvernsonger.com
*/
defined('ABSPATH') or die("ERROR: You do not have permission to access this page");
define('AAOSTRACTS_ACCESS_LEVEL', 'manage_options');
define('AAOSTRACTS_PLUGIN_DIR', dirname(__FILE__).'/' );
define('AAOSTRACTS_VERSION', '1.2');

add_action('init', 'aaostracts_init');
add_action('admin_menu', 'aaostracts_register_menu');
add_action('admin_init', 'aaostracts_disable_dashboard');
add_action('wp_ajax_getreviewers', 'aaostracts_getreviewers_ajax');
add_action('wp_ajax_managereviews', 'aaostracts_checkreviews_ajax');
add_action('wp_ajax_loadtopics', 'aaostracts_loadtopics_ajax');
add_filter('show_admin_bar', 'aaostracts_disable_adminbar');
add_filter('plugin_row_meta', 'aaostracts_plugin_links', 10, 2);
add_filter('tiny_mce_before_init', 'aaostracts_editor_init' );
add_filter('pre_set_site_transient_update_plugins', 'aaostracts_updater');
add_filter('plugins_api', 'aaostracts_api_call', 10, 3);
add_action('plugins_loaded', 'aaostracts_version_check');
add_shortcode('aaostracts', 'aaostracts_dashboard_shortcode');
add_shortcode('aaostracts_author', 'aaostracts_author_shortcode');
add_shortcode('aaostracts_reviewer', 'aaostracts_reviewer_shortcode');
register_activation_hook(__FILE__, 'aaostracts_install');

global $pagenow;
if ('admin.php' == $pagenow && isset($_GET['page']) && ($_GET['page'] == 'aaostracts')){
    add_action('admin_head', 'aaostracts_loadJS');
    add_action('admin_init', 'aaostracts_loadCSS');
    add_action('admin_init', 'aaostracts_editor_admin_init');
}
if(isset($_REQUEST['action']) && $_REQUEST['action']=='loadtopics'):
        do_action( 'wp_ajax_' . $_REQUEST['action'] );
endif;
function aaostracts_init() {
    load_plugin_textdomain('aaostracts', false, dirname(plugin_basename(__FILE__)) . '/languages/');
    include( AAOSTRACTS_PLUGIN_DIR . 'inc/aaostracts_functions.php' );
    include( apply_filters('aaostracts_page_include', AAOSTRACTS_PLUGIN_DIR . 'inc/aaostracts_downloads.php') );
}

function aaostracts_register_menu(){
    $page_title = __('AAOStract', 'aaostracts');
    add_menu_page( $page_title, $page_title, 'manage_options', 'aaostracts', 'aaostracts_admin_dashboard', plugins_url( 'images/icon.png', __FILE__), 99 );
    $submenus = array(
        array('page_title' => $page_title, 'menu_name' => apply_filters('aaostracts_title_filter', __('Summary','aaostracts'), 'summary'), 'capability' => 'manage_options', 'url' => 'admin.php?page=aaostracts&tab=summary'),
        array('page_title' => $page_title, 'menu_name' => apply_filters('aaostracts_title_filter', __('Abstracts','aaostracts'), 'abstracts'), 'capability' => 'manage_options', 'url' => 'admin.php?page=aaostracts&tab=abstracts'),
        array('page_title' => $page_title, 'menu_name' => apply_filters('aaostracts_title_filter', __('Events','aaostracts'), 'events'), 'capability' => 'manage_options', 'url' => 'admin.php?page=aaostracts&tab=events'),
        array('page_title' => $page_title, 'menu_name' => apply_filters('aaostracts_title_filter', __('Reviews','aaostracts'), 'reviews'), 'capability' => 'manage_options', 'url' => 'admin.php?page=aaostracts&tab=reviews'),
        array('page_title' => $page_title, 'menu_name' => apply_filters('aaostracts_title_filter', __('Attachments','aaostracts'), 'attachments'), 'capability' => 'manage_options', 'url' => 'admin.php?page=aaostracts&tab=attachments'),
        array('page_title' => $page_title, 'menu_name' => apply_filters('aaostracts_title_filter', __('Users','aaostracts'), 'users'), 'capability' => 'manage_options', 'url' => 'admin.php?page=aaostracts&tab=users'),
        array('page_title' => $page_title, 'menu_name' => apply_filters('aaostracts_title_filter', __('Reports','aaostracts'), 'reports'), 'capability' => 'manage_options', 'url' => 'admin.php?page=aaostracts&tab=reports'),
        array('page_title' => $page_title, 'menu_name' => apply_filters('aaostracts_title_filter', __('Settings','aaostracts'), 'settings'), 'capability' => 'manage_options', 'url' => 'admin.php?page=aaostracts&tab=settings'),
        array('page_title' => $page_title, 'menu_name' => apply_filters('aaostracts_title_filter', __('Emails','aaostracts'), 'emails'), 'capability' => 'manage_options', 'url' => 'admin.php?page=aaostracts&tab=emails'),
        array('page_title' => $page_title, 'menu_name' => apply_filters('aaostracts_title_filter', __('Help','aaostracts'), 'help'), 'capability' => 'manage_options', 'url' => 'admin.php?page=aaostracts&tab=help'),
    );

    $filter_menus = apply_filters('aaostracts_menu_filter', $submenus);

    foreach($filter_menus as $submenu){
        add_submenu_page( 'aaostracts',  $submenu['page_title'], $submenu['menu_name'], $submenu['capability'], $submenu['url'] );
    }
    remove_submenu_page('aaostracts','aaostracts');
}

function aaostracts_dashboard_shortcode($atts) {
    global $wpdb;
    aaostracts_loadCSS(); //load css only on dashboard pages
    aaostracts_loadJS();  //load js only on dashboard pages
    $args = array('event_id' => 0); // shortcode args with defaults
    $a = shortcode_atts( $args, $atts);
    $event_id = intval($a['event_id']);
    $event = $wpdb->get_var("SELECT COUNT(*) FROM " . $wpdb->prefix."aaostracts_events WHERE event_id = ".$event_id);
    if(!$event_id || !$event){
        $errorMsg = __('Please enter a valid event ID', 'aaostracts') . '<br>' .
                    __('The shortcode in this page should be similar to [aaostracts event_id=EventID]', 'aaostracts');
        aaostracts_showMessage($errorMsg, 'alert-danger');
        return;
    }
    ob_start();
    $dashboard = apply_filters('aaostracts_page_include', 'html/aaostracts_dashboard.php');
    include($dashboard);
    $html = ob_get_contents();
    ob_end_clean();
    return $html;
}
function aaostracts_author_shortcode($atts) {
    global $wpdb;
    aaostracts_loadCSS(); //load css only on dashboard pages
    aaostracts_loadJS();  //load js only on dashboard pages
    $args = array('event_id' => 0); // shortcode args with defaults
    $a = shortcode_atts( $args, $atts);
    $event_id = intval($a['event_id']);
    $event = $wpdb->get_var("SELECT COUNT(*) FROM " . $wpdb->prefix."aaostracts_events WHERE event_id = ". $event_id);
    if(!$event_id || !$event){
        $errorMsg = __('Please enter a valid event ID', 'aaostracts') . '<br>' .
                    __('The shortcode in this page should be similar to [aaostracts event_id=EventID]', 'aaostracts');
        aaostracts_showMessage($errorMsg, 'alert-danger');
        return;
    }
    ob_start();
    $dashboard = apply_filters('aaostracts_page_include', 'html/aaostracts_dashboard.php');
    include($dashboard);
    $html = ob_get_contents();
    ob_end_clean();
    return $html;
}
function aaostracts_reviewer_shortcode($atts) {
    global $wpdb;
    aaostracts_loadCSS(); //load css only on dashboard pages
    aaostracts_loadJS();  //load js only on dashboard pages
    $args = array('event_id' => 0); // shortcode args with defaults
    $a = shortcode_atts( $args, $atts);
    $event_id = intval($a['event_id']);
    $event = $wpdb->get_var("SELECT COUNT(*) FROM " . $wpdb->prefix."aaostracts_events WHERE event_id = ".$event_id);
    if(!$event_id || !$event){
        $errorMsg = __('Please enter a valid event ID', 'aaostracts') . '<br>' .
                    __('The shortcode in this page should be similar to [aaostracts event_id=EventID]', 'aaostracts');
        aaostracts_showMessage($errorMsg, 'alert-danger');
        return;
    }
    ob_start();
    $dashboard = apply_filters('aaostracts_page_include', 'html/aaostracts_dashboard.php');
    include($dashboard);
    $html = ob_get_contents();
    ob_end_clean();
    return $html;
}

function aaostracts_disable_dashboard() {
    if(!get_option('aaostracts_frontend_dashboard')) {
       if (is_admin() && !current_user_can( 'administrator' ) && !( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
            wp_redirect( home_url() );
            exit;
        }
    }
}

function aaostracts_disable_adminbar() {
    if(is_user_logged_in() && get_option('aaostracts_show_adminbar')){
	return true;
    }
    return false;
}

function aaostracts_plugin_links($links, $file) {

    if ($file == plugin_basename(__FILE__)) {
        $links[] = '<a href="http://www.aaostracts.com/contact/requests/" target="_blank">' . __('Request Customization', 'aaostracts') . '</a>';
        $links[] = '<a href="http://www.aaostracts.com/support" target="_blank">' . __('Support', 'aaostracts') . '</a>';
    }
    return $links;
}

function aaostracts_admin_header(){
    $header = '<div class="aaostracts container-fluid">' .
        '<div class="aaostracts row">' .
            '<div class="aaostracts logo col-xs-12">' .
            '<a href="?page=aaostracts"><img src="'. plugins_url("images/admin_logo.png", __FILE__) . '"></a>' .
            '<span style="vertical-align: middle; font-size: 11px; color: #44648A;">Pro v' . AAOSTRACTS_VERSION . '</span>' .
        '</div></div></div>';
    echo apply_filters('aaostracts_admin_header', $header);
}
function aaostracts_admin_dashboard() {
    global $pagenow;

    if ( $pagenow == 'admin.php' && $_GET['page'] == 'aaostracts' ){

        $tab = isset($_GET['tab']) ? $_GET['tab']  : 'summary';

        aaostracts_admin_header();
        aaostracts_admin_tabs($tab);

        switch ($tab){
            case 'summary' :
                $page = 'aaostracts_summary.php';
                break;
            case 'abstracts' :
                $page = 'aaostracts_abstracts.php';
                break;
            case 'events' :
                $page =  'aaostracts_events.php';
                break;
            case 'reviews' :
                $page = 'aaostracts_reviews.php';
                break;
            case 'attachments' :
                $page = 'aaostracts_attachments.php';
                break;
            case 'users' :
                $page = 'aaostracts_users.php';
                break;
             case 'reports' :
                $page = 'aaostracts_reports.php';
                break;
	    case 'settings' :
                $page = 'aaostracts_settings.php';
                break;
            case 'emails' :
                $page = 'aaostracts_emails.php';
                break;
            case 'help' :
                $page = 'aaostracts_help.php';
                break;
            default:
                $page = 'aaostracts_summary.php';
	}

        $page = apply_filters('aaostracts_page_include', $page);
        ob_start();
        include $page;
        $html = ob_get_contents();
        ob_end_clean();
        echo apply_filters('aaostracts_admin_pages', $html, $tab);

    }

}

function aaostracts_admin_tabs( $current = 'summary' ) {
    $basic_tabs = array(
        'summary' => apply_filters('aaostracts_title_filter', __('Summary','aaostracts'), 'summary'),
        'abstracts' => apply_filters('aaostracts_title_filter', __('Abstracts','aaostracts'), 'abstracts'),
        'events' => apply_filters('aaostracts_title_filter', __('Events','aaostracts'), 'events'),
        'reviews' => apply_filters('aaostracts_title_filter', __('Reviews','aaostracts'), 'reviews'),
        'attachments' => apply_filters('aaostracts_title_filter', __('Attachments','aaostracts'), 'attachments'),
        'users' => apply_filters('aaostracts_title_filter', __('Users','aaostracts'), 'users'),
        'reports' => apply_filters('aaostracts_title_filter', __('Reports','aaostracts'), 'reports'),
        'emails' => apply_filters('aaostracts_title_filter', __('Emails','aaostracts'), 'emails')
    );

    $tabs = apply_filters('aaostracts_admin_tabs', $basic_tabs);
    $tabs['settings'] = apply_filters('aaostracts_title_filter', __('Settings','aaostracts'), 'settings');
    $tabs['help'] = apply_filters('aaostracts_title_filter', __('Help','aaostracts'), 'help');

    $top_menu = '<div class="aaostracts container-fluid">';
    $top_menu .= '<ul class="aaostracts nav nav-tabs">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? "aaostracts active" : "";
        $top_menu .= "<li role='presentation' class='".$class."'><a href='?page=aaostracts&tab=$tab'><strong>$name</strong></a></li>";
    }
    $top_menu .= '</ul>';
    $top_menu .= '</div>';
    echo $top_menu;
}

function aaostracts_install() {
   global $wpdb;
   require_once(ABSPATH.'wp-admin/includes/upgrade.php');
   // reviews table
   $table_name = $wpdb->prefix."aaostracts_reviews";
   $sql = "CREATE TABLE ".$table_name." (
		  review_id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
		  abstract_id int(11),
		  user_id int(11),
		  status varchar(25),
		  relevance varchar(25),
		  quality varchar(25),
		  comments text,
                  recommendation varchar(25),
                  customized int(11),
                  review_date datetime,
		  PRIMARY KEY (review_id)

	  );";

      dbDelta($sql);
    $table_name = $wpdb->prefix."aaostracts_abstracts";
    $sql = "CREATE TABLE ".$table_name." (
		  abstract_id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
		  title text,
		  text longtext,
		  event int(11),
                  topic text,
                  status varchar(25),
		  author text,
		  author_email text,
                  author_affiliation text,
		  presenter varchar(255),
		  presenter_email varchar(255),
		  presenter_preference varchar(255),
                  keywords text,
                  reviewer_id1 int(11),
                  reviewer_id2 int(11),
                  reviewer_id3 int(11),
		  submit_by int(11),
		  submit_date datetime,
                  customized int(11),
		  PRIMARY KEY (abstract_id)
	  );";

    $updateEmail = "UPDATE " . $table_name . " SET author_email = REPLACE(author_email, ',', ' | ')";
    $updateAffiliation = "UPDATE " . $table_name . " SET author_affiliation = REPLACE(author_affiliation, ',', ' | ')";
    dbDelta($updateEmail);
    dbDelta($updateAffiliation);
    dbDelta($sql);

      // Events Table
   $table_name = $wpdb->prefix."aaostracts_events";
   $sql = "CREATE TABLE " . $table_name." (
		  event_id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
		  name varchar(255),
		  description longtext,
		  address longtext,
		  host varchar(255),
                  topics text,
		  start_date date,
		  end_date date,
                  deadline date,
                  customized int(11),
		  PRIMARY KEY  (event_id)
	  );";

      dbDelta($sql);

    $table_name = $wpdb->prefix."aaostracts_attachments";
    $sql = "CREATE TABLE ".$table_name." (
	  	attachment_id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
		abstracts_id int(11),
		filecontent longblob,
		filename varchar(255),
		filetype varchar(255),
		filesize varchar(255),
                customized int(11),
		PRIMARY KEY  (attachment_id)
	  );";
      dbDelta($sql);
    $table_name = $wpdb->prefix."aaostracts_emailtemplates";
    //$wpdb->query("drop table ".$table_name);
    $sql = "CREATE TABLE ".$table_name." (
	  	ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
		name varchar(255),
                subject varchar(255),
		message text,
                from_name varchar(255),
                from_email varchar(255),
                receiver varchar(255),
                customized int(11),
		PRIMARY KEY (ID)
	  );";
      dbDelta($sql);

      // settings tab
	add_option("aaostracts_chars_count", 250);
        add_option("aaostracts_upload_limit", 3);
	add_option("aaostracts_max_attach_size", 2048000);
        add_option('aaostracts_author_instructions', "Enter description here.");
        add_option("aaostracts_presenter_preference", "Poster,Panel,Roundtable,Projector");
	add_option("aaostracts_email_admin", 1);
	add_option("aaostracts_email_author", 1);
	add_option("aaostracts_frontend_dashboard", 1);
        add_option("aaostracts_reviewer_submit", 0);
        add_option("aaostracts_reviewer_edit", 0);
        add_option("aaostracts_blind_review", 0);
	add_option("aaostracts_show_adminbar", 0);
	add_option("aaostracts_permitted_attachments", 'pdf,doc,xls,docx,xlsx,txt,rtf');
	add_option("aaostracts_change_ownership", 1);
        add_option("aaostracts_status_notification", 1);
        add_option("aaostracts_review_notification", 0);
        add_option("aaostracts_show_reviews", 1);
        add_option("aaostracts_show_author", 1);
        add_option("aaostracts_show_presenter", 1);
        add_option("aaostracts_show_attachments", 1);
        add_option("aaostracts_show_keywords", 0);
        add_option("aaostracts_show_conditions", 0);
        add_option("aaostracts_terms_conditions", "Enter your terms and conditions here.");
        add_option("aaostracts_sync_status", 0);
        update_option("aaostracts_version", AAOSTRACTS_VERSION);

        $sql = "SELECT COUNT(*) FROM ". $wpdb->prefix."aaostracts_emailtemplates";
        if($wpdb->get_var($sql) < 1){
            aaostracts_installEmailTemplates();
        }else{
            add_option("aaostracts_submit_templateId", 1);
            add_option("aaostracts_assignment_templateId", 2);
            add_option("aaostracts_admin_templateId", 3);
            add_option("aaostracts_approval_templateId", 4);
            add_option("aaostracts_rejected_templateId", 5);
            aaostracts_installEmailTemplates();
        }
}

function aaostracts_installEmailTemplates(){
    global $wpdb;
    $from_name = get_option('blogname');
    $from_email = get_option('admin_email');

    // submission confirmation template
    if(!get_option('aaostracts_submit_templateId')){
        $submitConfirmationMsg = 'Hi {DISPLAY_NAME},
                You have successfully submitted your abstract.
                Abstracts Title: {ABSTRACT_TITLE}
                Abstracts ID: {ABSTRACT_ID}
                Event: {EVENT_NAME}
                To make changes to your submission or view the status visit {SITE_URL} and sign in to your dashboard.
                Regards,
                WP Abstracts Team
                {SITE_NAME}
                {SITE_URL}';

        $submitConfirmationTemplate = array(
                    'name' => "Abstracts Submission Acknowledgement",
                    'subject' => "Abstract Submitted Successfully",
                    'message'=> $submitConfirmationMsg,
                    'from_name' => $from_name,
                    'from_email' => $from_email,
                    'receiver' => "Authors"
                );
        $wpdb->insert($wpdb->prefix.'aaostracts_emailtemplates', $submitConfirmationTemplate);
        add_option("aaostracts_submit_templateId", $wpdb->insert_id);
    }

    // reviewer assignment template
    if(!get_option('aaostracts_assignment_templateId')){
        $reviewerAssignmentMsg = 'Hello {DISPLAY_NAME},
                    You have been assigned a new abstract for review.
                    To review this or other abstracts please sign in at: {SITE_URL}
                    Regards,
                    WP Abstracts Team
                    {SITE_NAME}
                    {SITE_URL}';

        $reviewerAssignmentTemplate = array(
                    'name' => "Reviewer Assignment",
                    'subject' => "New Abstract Assigned",
                    'message'=> $reviewerAssignmentMsg,
                    'from_name' => $from_name,
                    'from_email' => $from_email,
                    'receiver' => "Reviewers"
                );

        $wpdb->insert($wpdb->prefix.'aaostracts_emailtemplates', $reviewerAssignmentTemplate);
        add_option("aaostracts_assignment_templateId", $wpdb->insert_id);
    }

    // admin submission notification template
    if(!get_option('aaostracts_admin_templateId')){
        $adminNotications = 'Hello {DISPLAY_NAME},
                        You have a new abstract for {SITE_NAME}
                        Abstract Title: {ABSTRACT_TITLE}
                        Abstract ID: {ABSTRACT_ID}
                        Regards,
                        WP Abstracts Team
                        {SITE_NAME}
                        {SITE_URL}';
        $adminEmailTemplate = array(
                    'name' => "Abstract Submission Notification",
                    'subject' => "New Abstract Submitted",
                    'message'=> $adminNotications,
                    'from_name' => $from_name,
                    'from_email' => $from_email,
                    'receiver' => "Administrators"
                );
        $wpdb->insert($wpdb->prefix.'aaostracts_emailtemplates', $adminEmailTemplate);
        add_option("aaostracts_admin_templateId", $wpdb->insert_id);
    }

function aaostracts_version_check(){
    if(!(get_option( "aaostracts_db_upgraded") == "Y")){
         aaostracts_upgrade_db();
    }
    if(!(get_option( "aaostracts_version") == AAOSTRACTS_VERSION)){
         aaostracts_install();
    }
}

function aaostracts_upgrade_db(){
    global $wpdb;
    // upgrade abstracts table
    $oldtable_name = $wpdb->prefix."aaostracts_submissions";
    $newtable_name = $wpdb->prefix."aaostracts_abstracts";
    $sql = "ALTER TABLE ".$oldtable_name." RENAME TO $newtable_name";
    $wpdb->query($sql);
    $sql = "ALTER TABLE ".$newtable_name." ADD status varchar(25);";
    $wpdb->query($sql);
    $sql = "ALTER TABLE ".$newtable_name." CHANGE id abstract_id int(11) UNSIGNED NOT NULL AUTO_INCREMENT;";
    $wpdb->query($sql);
    $sql = "ALTER TABLE ".$newtable_name." CHANGE event event int(11);";
    $wpdb->query($sql);
    $sql = "ALTER TABLE ".$newtable_name." ADD topic varchar(55);";
    $wpdb->query($sql);
    $sql = "ALTER TABLE ".$newtable_name." CHANGE rid reviewer_id1 int(11)";
    $wpdb->query($sql);
    $sql = "ALTER TABLE ".$newtable_name." ADD reviewer_id2 int(11);";
    $wpdb->query($sql);
    $sql = "ALTER TABLE ".$newtable_name." ADD reviewer_id3 int(11);";
    $wpdb->query($sql);
    $sql = "ALTER TABLE ".$newtable_name." CHANGE submit_date submit_date datetime;";
    $wpdb->query($sql);
    $sql = "ALTER TABLE ".$newtable_name." ADD author_affiliation varchar(255);";
    $wpdb->query($sql);
    // upgrade reviews table
    $reviews_table = $wpdb->prefix."aaostracts_reviews";
    $sql = "SHOW INDEXES FROM " .$reviews_table;
    $indexes = $wpdb->get_results($sql);
    foreach($indexes as $index){
        if($index->Column_name == "abstract_id" ){
            $sql = "DROP INDEX " . $index->Key_name . " ON " . $reviews_table;
            $wpdb->query($sql);
        }
    }
    // upgrade event table
    $events_table = $wpdb->prefix."aaostracts_events";
    $sql = "ALTER TABLE ".$events_table." ADD topics text;";
    $wpdb->query($sql);
    $sql = "ALTER TABLE ".$events_table." CHANGE id event_id int(11) UNSIGNED NOT NULL AUTO_INCREMENT;";
    $wpdb->query($sql);
    // upgrade attachments table
    $attachments_table = $wpdb->prefix."aaostracts_attachments";
    $sql = "ALTER TABLE ".$attachments_table." CHANGE id attachment_id int(11) UNSIGNED NOT NULL AUTO_INCREMENT;";
    $wpdb->query($sql);
    // store option to show DB updated and current version
    update_option('aaostracts_db_upgraded', 'Y');
}
function aaostracts_loadJS() {
    wp_enqueue_script('aaostracts-scripts', plugins_url('js/aaostracts.js', __FILE__), array( 'jquery' ));
    wp_enqueue_script('aaostracts-bootstrap-js', plugins_url('js/bootstrap.min.js', __FILE__), array( 'jquery' ));
    wp_enqueue_script('aaostracts-jquery-ui-js', plugins_url('js/jquery-ui.min.js', __FILE__), array( 'jquery' ));
    wp_enqueue_script('aaostracts-alertify-js', plugins_url('js/alertify.js', __FILE__), array( 'jquery' ));
    aaostracts_localize();
}

function aaostracts_loadCSS(){
    wp_enqueue_style('aaostracts-style', plugins_url('css/aaostracts.css', __FILE__));
    wp_enqueue_style('aaostracts-alertify-css', plugins_url('css/alertify.css', __FILE__));
    wp_enqueue_style('aaostracts-jquery-ui-css', plugins_url('css/jquery-ui.css', __FILE__));
    wp_enqueue_style('aaostracts-jquery-ui-ie-css', plugins_url('css/jquery-ui-ie.css', __FILE__));
}

function aaostracts_localize(){
    wp_localize_script('aaostracts-scripts', 'front_ajax',
            array('ajaxurl' => admin_url('admin-ajax.php'),
                'name_event' => __('Please enter a name for your event', 'aaostracts'),
                'authorName' => __('Name', 'aaostracts'),
                'authorEmail' => __('Email', 'aaostracts'),
                'confirmdelete' => __('Do you really want to delete this abstract and all its attachments?', 'aaostracts'),
                'confirmdeleteEvent' => __('Do you really want to delete this event?', 'aaostracts'),
                'confirmdeleteReview' => __('Do you really want to delete this review?', 'aaostracts'),
                'confirmdeleteAttachment' => __('Do you really want to delete this attachment?', 'aaostracts'),
                'confirmdeleteUser' => __('Are you sure you want to delete this user?', 'aaostracts'),
                'hostedby' => __('hosted by', 'aaostracts'),
                'assign_reviewer' => apply_filters('aaostracts_title_filter', __("Assign Reviewer", 'aaostracts'), 'assign_reviewer'),
                'review_alert' => __('Review Alert', 'aaostracts'),
                'fillin' => __('Please fill in all required fields.', 'aaostracts'),
                'topic' => __('Topic', 'aaostracts'),
                'affiliation' => apply_filters('aaostracts_title_filter', __("Affiliation", 'aaostracts'), 'affiliation'),
            )
    );
}


function aaostracts_getreviewers_ajax(){
    require_once(apply_filters('aaostracts_page_include', AAOSTRACTS_PLUGIN_DIR . 'aaostracts_abstracts.php'));
    aaostracts_getReviewers();
}

function aaostracts_checkreviews_ajax(){
    require_once(apply_filters('aaostracts_page_include', AAOSTRACTS_PLUGIN_DIR . 'aaostracts_reviews.php'));
    aaostracts_checkReviews();
}

function aaostracts_loadtopics_ajax(){
    require_once(apply_filters('aaostracts_page_include', AAOSTRACTS_PLUGIN_DIR . 'aaostracts_events.php'));
    aaostracts_loadTopics();
}

function aaostracts_editor_admin_init() {
    wp_enqueue_script('post');
    wp_enqueue_script('editor');
    wp_enqueue_script('media-upload');
}

function aaostracts_set_html_content_type() {
    return 'text/html';
}

function aaostracts_editor_init( $initArray ){
    $initArray['setup'] = <<<JS
[function(ed) {
    ed.onKeyUp.add(function(ed, e){
        aaostracts_updateWordCount();
    });
}][0]
JS;
    return $initArray;
}
/****************** AUTO-UPDATER *******************/
$api_url = 'http://updates.aaostracts.com/';

$plugin_slug = basename(dirname(__FILE__));

function aaostracts_updater($checked_data) {
    global $api_url, $plugin_slug, $wp_version;

    if (empty($checked_data->checked)){
        return $checked_data;
    }

    $args = array(
            'slug' => $plugin_slug,
            'version' => $checked_data->checked[$plugin_slug .'/'. $plugin_slug .'.php'],
    );
    $request_string = array(
                    'body' => array(
                            'action' => 'basic_check',
                            'request' => serialize($args),
                            'api-key' => md5(get_bloginfo('url'))
                    ),
                    'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
            );

    // Start checking for an update
    $raw_response = wp_remote_post($api_url, $request_string);

    if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200)){
        $response = unserialize($raw_response['body']);
    }

    if (is_object($response) && !empty($response)){
        $checked_data->response[$plugin_slug .'/'.$plugin_slug.'.php'] = $response;
    }

    return $checked_data;
}
function aaostracts_api_call($def, $action, $args) {
    global $plugin_slug, $api_url, $wp_version;

    if (isset($args->slug) && ($args->slug != $plugin_slug)){
        return false;
    }
    // Get the current version
    $plugin_info = get_site_transient('update_plugins');
    $current_version = $plugin_info->checked[$plugin_slug .'/'.$plugin_slug.'.php'];
    $args->version = $current_version;

    $request_string = array(
                    'body' => array(
                            'action' => $action,
                            'request' => serialize($args),
                            'api-key' => md5(get_bloginfo('url'))
                    ),
                    'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
            );

    $request = wp_remote_post($api_url, $request_string);
    $res = unserialize($request['body']);

    return $res;
}