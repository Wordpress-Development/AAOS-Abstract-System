<?php
defined('ABSPATH') or die("ERROR: You do not have permission to access this page");?>
<div class="metabox-holder has-right-sidebar">
            <div class="inner-sidebar">
                <div class="postbox"><!-- Event -->
                    <h3><?php _e('Important Information', 'aaostracts'); ?></h3>
                    <div class="inside">
                        <p><?php _e('At least one event must be created to allow users to submit abstracts.', 'aaostracts'); ?></p>
                    </div>
                </div>
                <div class="postbox">
                    <h3><span><?php _e('Help us Improve', 'aaostracts'); ?></span></h3>
                    <div class="inside">
                        <p><a href="" target="_blank"><?php _e('Suggest', 'aaostracts'); ?></a> <?php _e('features', 'aaostracts'); ?>.</p>
                        <p><a href="" target="_blank"><?php _e('Rate', 'aaostracts'); ?></a> <?php _e('the plugin 5 stars on WordPress.org', 'aaostracts'); ?>.</p>
                        <p><a href="" target="_blank"><?php _e('Like us', 'aaostracts'); ?></a> <?php _e('on Facebook', 'aaostracts'); ?>. </p>
                    </div>
		</div>
            </div>
            <div id="post-body">
                <div id="post-body-content">
                    <div class="postarea">
                        <h2><?php _e('Get setup and running in three simple steps', 'aaostracts'); ?></h2>
                        <div class="steps"><img src="<?php echo plugins_url('images/one.png', __FILE__); ?>" />
                            <p><?php _e('Create an event. This may be the name of a conference or anything relating to the submission of abstracts.', 'aaostracts'); ?></p>
                        </div>
			    <div class="steps"><img src="<?php echo plugins_url('images/two.png', __FILE__);?>" />
                                <p><?php _e('Visit the settings page and choose your preference for the plugin. Also ensure your permanent links are set to post-name or custom.', 'aaostracts'); ?></p>
                            </div>
			    <div class="steps"><img src="<?php echo plugins_url('images/three.png', __FILE__);?>" />
                                <p><?php _e('Create a dashboard page and add [aaostracts event_id="EVENT_ID_HERE"] and link this page to your site menu to allow frontend access.', 'aaostracts'); ?></p>
                            </div>
                    </div>
                </div>
            </div>
    </div>