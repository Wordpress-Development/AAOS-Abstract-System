<?php
defined('ABSPATH') or die("ERROR: You do not have permission to access this page");
if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class AAOStract_Abstracts_Table extends WP_List_Table {

    function __construct(){
        global $status, $page;

        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'abstract',     //singular name of the listed records
            'plural'    => 'abstracts',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );

    }

    function column_title($item){
        global $wpdb;
        $paged = ($_POST && isset($_POST["paged"])) ? intval($_POST["paged"]) : (isset($_GET["paged"]) && !empty($_GET["paged"])) ? intval($_GET["paged"]) : 1; //send paged to ajax to maintain current paged
        $reviews_count = $wpdb->get_var("SELECT COUNT(*) FROM ".$wpdb->prefix."aaostracts_reviews WHERE abstract_id = $item->abstract_id");
        $prefilter_actions = array(
            'accept' => '<a href="?page=aaostracts&tab=abstracts&task=accept&id='.$item->abstract_id.'">'.__('Accept','aaostracts').'</a>',
            'pending' => '<a href="?page=aaostracts&tab=abstracts&task=pending&id='.$item->abstract_id.'">'.__('Pending','aaostracts').'</a>',
            'reject' => '<a href="?page=aaostracts&tab=abstracts&task=reject&id=' . $item->abstract_id . '">' . __('Reject', 'aaostracts') . '</a>',
            'edit' => '<a href="?page=aaostracts&tab=abstracts&task=edit&id=' . $item->abstract_id . '">' . __('Edit', 'aaostracts') . '</a>',
            'assign' => '<a href="#assign" onclick="aaostracts_assign_reviewer(' . $item->abstract_id . ', '.$paged.');">' . __('Assign', 'aaostracts') . '</a>',
            'reviews' => '<a href="?page=aaostracts&tab=reviews&task=view&id=' . $item->abstract_id . '";>' . __('Reviews', 'aaostracts') . ' (' . $reviews_count . ')</a>',
            'pdf' => '<a href="?page=aaostracts&tab=abstracts&task=download&type=pdf&id=' . $item->abstract_id . '">' . __('Export PDF', 'aaostracts') . '</a>',
            'delete' => '<a href="javascript:aaostracts_delete_abstract(' . $item->abstract_id . ')">' . __('Delete', 'aaostracts') . '</a>',
        );
        $actions = apply_filters('aaostracts_abstract_actions', $prefilter_actions, $item);
        return sprintf('%1$s <span style="color:silver">[ID:%2$s]</span> %3$s',$item->title, $item->abstract_id, $this->row_actions($actions));
    }

    function column_cb($item) {
        return sprintf('<input type="checkbox" name="%1$s[]" value="%2$s" />', $this->_args['singular'], $item->abstract_id);
    }

    function column_topic($item){
        global $wpdb;
        $event = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."aaostracts_events WHERE event_id = $item->event");
        echo $event->name . " / " . $item->topic;
    }

    function column_reviewers($item){
        $reviewers = array();
        $reviewers[] = get_userdata($item->reviewer_id1);
        $reviewers[] = get_userdata($item->reviewer_id2);
        $reviewers[] = get_userdata($item->reviewer_id3);
        $reviewer_list = null;
        foreach($reviewers AS $reviewer){
            if($reviewer){
               $reviewer_list .= "<span class=\"reviewerList\">". $reviewer->display_name . "</span>";
            }
        }
        $current_reviewers = (empty($reviewer_list)) ? __("Not Assigned",'aaostracts') : $reviewer_list;
        echo apply_filters('aaostracts_abstracts_reviewers', $current_reviewers, $item);
    }

     function column_default( $item, $column_name ) {
        global $wpdb;
        switch ($column_name) {
            case 'author': echo $item->$column_name; break;
            case 'status': _e($item->$column_name, 'aaostracts'); break;
            case 'submit_date': echo date_i18n(get_option('date_format') . ' ' . get_option('time_format'), strtotime($item->$column_name)); break;
            case 'attachments':
                $attachments_count = $wpdb->get_var("SELECT COUNT(*) FROM ". $wpdb->prefix."aaostracts_attachments WHERE abstracts_id = $item->abstract_id");
                echo $attachments_count; break;
        }
    }

    function get_columns(){
        $columns = array();
        $columns['cb'] = '<input type="checkbox" />';
        $columns['title'] = __('Title', 'aaostracts');
        $columns['topic'] = __('Event / Topic', 'aaostracts');
        if(get_option('aaostracts_show_author')){
            $columns['author'] = __('Author', 'aaostracts');
        }
        $columns['status'] = __('Status', 'aaostracts');
        $columns['reviewers'] = __('Reviewers', 'aaostracts');
        $columns['submit_date'] = __('Submitted', 'aaostracts');
        if(get_option('aaostracts_show_attachments')){
            $columns['attachments'] = '';
        }
        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'title'     => array('title',false),     //true means it's already sorted
            'rid'    => array('rid',false),
            'author'    => array('author',false),
            'topic'    => array('topic',false),
            'status'    => array('status',false),
            'submit_date'    => array('submit_date',false)
        );
        return $sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'accept' => __('Accept', 'aaostracts'),
            'pending' => __('Pending', 'aaostracts'),
            'reject' => __('Reject', 'aaostracts'),
            'delete' => __('Delete', 'aaostracts')
        );
        return $actions;
    }

    function process_bulk_action() {
        if ( 'accept'=== $this->current_action() ) {
            foreach($_GET['abstract'] as $abstract_id) {
                if(current_user_can(AAOSTRACTS_ACCESS_LEVEL)){
                    aaostracts_changeStatus(intval($abstract_id), "Accepted");
                }
            }
        }
        if ( 'pending'=== $this->current_action() ) {
            foreach($_GET['abstract'] as $abstract_id) {
                if(current_user_can(AAOSTRACTS_ACCESS_LEVEL)){
                    aaostracts_changeStatus(intval($abstract_id), "Pending");
                }
            }
        }
        if ( 'reject'=== $this->current_action() ) {
            foreach($_GET['abstract'] as $abstract_id) {
                if(current_user_can(AAOSTRACTS_ACCESS_LEVEL)){
                    aaostracts_changeStatus(intval($abstract_id),"Rejected");
                }
            }
        }
        if ( 'delete'=== $this->current_action() ) {
            foreach($_GET['abstract'] as $abstract_id) {
                if(current_user_can(AAOSTRACTS_ACCESS_LEVEL)){
                    aaostracts_deleteAbstract(intval($abstract_id), false);
                }
            }
        }

    }

    function get_status_filter(){
        $current = (isset($_GET['status_filter']) && $_GET['status_filter']) ? $_GET['status_filter'] : ""; ?>
        <label>Filter by Status</label>
        <select name="status_filter" onchange="abstracts_list.submit();">
            <option value="" <?php selected($current , ""); ?>>All Abstracts</option>
            <option value="Pending" <?php selected($current , "Pending"); ?>>Pending</option>
            <option value="Accepted" <?php selected($current , "Accepted"); ?>>Accepted</option>
            <option value="Rejected" <?php selected($current , "Rejected"); ?>>Rejected</option>
        </select>
    <?php
    }

    function get_topic_filter(){
        $current = (isset($_GET['topic_filter']) && $_GET['topic_filter']) ? $_GET['topic_filter'] : ""; ?>
        <select name="topic_filter">
            <option value="" <?php selected($current , ""); ?>>All Users</option>
            <option value="subscriber" <?php selected($current , "subscriber"); ?>>Authors</option>
            <option value="editor" <?php selected($current , "editor"); ?>>Reviewers</option>
            <option value="administrator" <?php selected($current , "administrator"); ?>>Administrators</option>
        </select>
        <?php submit_button( __('Filter Users', 'aaostracts'), 'submit', 'secondary', false, false);
        echo "&nbsp;&nbsp;&nbsp;";
    }

    function get_search_box() { ?>
        <label>Search Abstracts</label>
        <input type="search" class="aaostracts form-control" id="abstract_search" placeholder="ID, Title or Topic" name="s" value="<?php _admin_search_query(); ?>" />
        <?php submit_button( __('Go', 'aaostracts'), 'secondary', false, false ); ?>
    <?php
    }

    function extra_tablenav( $which ) {

	if ( $which == "top" ){
            $this->get_status_filter();
            $this->get_search_box();
	}
    }

    function prepare_items() {
        global $wpdb, $_wp_column_headers;
	$screen = get_current_screen();
        $table_name = $wpdb->prefix."aaostracts_abstracts";
        $query = "SELECT * FROM " . $table_name;
        $searched = false;
        if(isset($_GET['s']) && $_GET['s']){
            $term = trim(sanitize_text_field($_GET['s']));
            $query .= " WHERE (abstract_id LIKE '%%%$term%%' OR title LIKE '%%%$term%%' OR topic LIKE '%%%$term%%')";
            $searched = true;
        }
        if(isset($_GET['status_filter']) && $_GET['status_filter']){
            $filter = trim(sanitize_text_field($_GET['status_filter']));
            if($searched){
                $query .= " AND status = '$filter'";
            }else{
                $query .= " WHERE status = '$filter'";
            }
        }
        $orderby = !empty($_GET["orderby"]) ? sanitize_text_field($_GET["orderby"]) : 'abstract_id';
        $order = !empty($_GET["order"]) ? sanitize_text_field($_GET["order"]) : 'desc';
        if(!empty($orderby) & !empty($order)){ $query.=' ORDER BY '.$orderby.' '.$order; }
        $totalitems = $wpdb->get_var("SELECT COUNT(*) FROM " . $table_name); // get  the total number of rows
        //How many to display per page?
        $perpage = 20;
        //Which page is this?
        $paged = $this->get_pagenum();
        //How many pages do we have in total?
        $totalpages = ceil($totalitems/$perpage);
        //adjust the query to take pagination into account
        if(!empty($paged) && !empty($perpage)){
            $offset = ($paged-1)*$perpage;
            $query.=' LIMIT '.(int)$offset.','.(int)$perpage;
        }

        /* -- Register the pagination -- */
        $this->set_pagination_args( array(
            "total_items" => $totalitems,
            "total_pages" => $totalpages,
            "per_page" => $perpage,
        ) );
        /* -- Register the pagination -- */
        $this->process_bulk_action();

        /* -- Fetch the items -- */
        $this->items = $wpdb->get_results($query);

        /* -- Register the Columns -- */
        $columns = $this->get_columns();
        $_wp_column_headers[$screen->id]=$columns;
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);
    }

} // END ABSTRACTS TABLE CLASS

class AAOStract_Reviews_Table extends WP_List_Table {

    function __construct(){
        global $status, $page;

        //Set parent defaults
        parent::__construct( array(
            'singular' => 'review', //singular name of the listed records
            'plural'    => 'reviews',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );

    }

    function column_abstract_id($item){
        global $wpdb;
        $abstract = $wpdb->get_row("SELECT title FROM ".$wpdb->prefix."aaostracts_abstracts WHERE abstract_id = $item->abstract_id", ARRAY_A);
        $actions = array(
            'edit' => '<a href="?page=aaostracts&tab=reviews&task=edit&id=' . $item->review_id . '">' . __('Edit', 'aaostracts') . '</a>',
            'delete' => '<a href="javascript:aaostracts_delete_review(' . $item->review_id . ')">' . __('Delete', 'aaostracts') . '</a>',
        );
        return sprintf('%1$s<span style="color:silver"> [%2$s]</span>%3$s', $abstract['title'], $item->abstract_id, $this->row_actions($actions));
    }

    function column_comments($item){
        $user_info = get_userdata($item->user_id);
             if($user_info){
                 $reviewer = $user_info->display_name;
             }else{
                 $reviewer = __("User deleted", 'aaostracts');
        }
        $lastUpdated = date_i18n(get_option('date_format') . ' ' . get_option('time_format'), strtotime($item->review_date));
        return sprintf('%1$s<br><span style="color:silver">' . __('Reviewed by', 'aaostracts') . ': %2$s | %3$s</span>', $item->comments, $reviewer, $lastUpdated);
    }

    function column_cb($item) {
        return sprintf('<input type="checkbox" name="%1$s[]" value="%2$s" />', $this->_args['singular'], $item->review_id);
    }

    function column_default( $item, $column_name ) {
        switch( $column_name ) {
            case 'status': _e($item->$column_name, 'aaostracts'); break;
            case 'recommendation': _e($item->$column_name, 'aaostracts'); break;
        }
    }

    function column_criteria($item){
        if(!$item->customized){
            $relevance = __('Relevance', 'aaostracts') . ' : <strong>' . $item->relevance . '</strong>';
            $quality = __('Quality', 'aaostracts') . ' : <strong>' . $item->quality . '</strong>';
            echo $relevance . '<br>' . $quality . '<br>';
        }else{
            _e('The review entered for this item matches a different criteria.', 'aaostracts');
        }
    }

    function extra_tablenav( $which ) {

	if ( $which == "top" ){
            echo "<span class=\"settings_tip\" data-tip=\"";
            _e('TIP: The suggested status on reviews is only a recommendation, administrators have the final decision to accept or reject submissions.', 'aaostracts');
            echo "\"><img src=" . plugins_url("aaostracts_pro/images/settings_help.png") . "></span>";
	}
    }

    function get_columns(){
        $columns = array(
            'cb'        => '<input type="checkbox" />',
            'abstract_id' => __('Abstract', 'aaostracts'),
            'comments' => __('Comments', 'aaostracts'),
            'criteria' => __('Criteria', 'aaostracts'),
            'recommendation' => __('Type', 'aaostracts'),
            'status' => __('Suggested', 'aaostracts')
        );
        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'abstract_id'    => array('abstract_id',true),
            'status'    => array('status',false),
            'recommendaton'    => array('recommendaton',false)
        );
        return $sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'delete' => __('Delete', 'aaostracts')
        );
        return $actions;
    }

    function process_bulk_action() {
        if ( 'delete'=== $this->current_action() ) {
            foreach($_GET['review'] as $review) {
                aaostracts_deleteReview($review, false);
            }
        }
    }

    function prepare_items() {
        global $wpdb, $_wp_column_headers;
	$screen = get_current_screen();
        $table_name = $wpdb->prefix."aaostracts_reviews";
        $query = "SELECT * FROM " . $table_name;
        $orderby = !empty($_GET["orderby"]) ? sanitize_text_field($_GET["orderby"]) : 'review_id';
        $order = !empty($_GET["order"]) ? sanitize_text_field($_GET["order"]) : 'desc';
        if(!empty($orderby) & !empty($order)){ $query.=' ORDER BY '.$orderby.' '.$order; }
        $totalitems = $wpdb->get_var("SELECT COUNT(*) FROM " . $table_name); // get  the total number of rows
        $perpage = 15;
        $paged = !empty($_GET["paged"]) ? intval($_GET["paged"]) : '';
        if(empty($paged) || !is_numeric($paged) || $paged<=0 ){ $paged=1; }
        $totalpages = ceil($totalitems/$perpage);
        if(!empty($paged) && !empty($perpage)){
            $offset=($paged-1)*$perpage;
            $query.=' LIMIT '.(int)$offset.','.(int)$perpage;
        }

        $this->set_pagination_args( array(
            "total_items" => $totalitems,
            "total_pages" => $totalpages,
            "per_page" => $perpage,
        ) );

        $this->process_bulk_action();

        $this->items = $wpdb->get_results($query);

        $columns = $this->get_columns();
        $_wp_column_headers[$screen->id]=$columns;
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);

    } // end prepare items

} // end REVIEW Table class

class AAOStract_Attachments_Table extends WP_List_Table {

    function __construct(){

        global $status, $page;

        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'attachment',     //singular name of the listed records
            'plural'    => 'attachments',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );

    }

    function column_filename($item){
        $actions = array(
            'download' => '<a href="?page=aaostracts&tab=attachments&task=download&type=attachment&id=' . $item->attachment_id . '" ">' . __('Download', 'aaostracts') . '</a>',
            'delete' => '<a href="javascript:aaostracts_delete_attachment(' . $item->attachment_id . ');">' . __('Delete', 'aaostracts') . '</a>'
        );
        return sprintf('%1$s <span style="color:silver"></span>%2$s',$item->filename. " [" . $item->attachment_id . "]", $this->row_actions($actions));
    }

    function column_cb($item) {
        return sprintf('<input type="checkbox" name="%1$s[]" value="%2$s" />', $this->_args['singular'], $item->attachment_id);
    }

    function column_author( $item ) {
        $user = get_user_by( 'id', $item->submit_by );
        echo $user->display_name . " (" . $user->user_login . ")";
    }

    function column_default( $item, $column_name ) {
        switch( $column_name ) {
            case 'abstracts_id': echo $item->title . " [" . $item->abstract_id . "]"; break;
            case 'filetype': $filetype = wp_check_filetype($item->filename); echo $filetype['ext'];break;
            case 'filesize': echo number_format(($item->filesize/1048576), 2) . " MB"; break;
        }
    }

    function column_download($item) {
        return sprintf('<a href="?page=aaostracts&tab=attachments&task=download&type=attachment&id=' . $item->attachment_id . '" "><span class="dashicons dashicons-download"></span></a>');
    }

    function get_columns(){
        $columns = array(
            'cb'        => '<input type="checkbox" />',
            'filename' => __('File Name [ID]', 'aaostracts'),
            'abstracts_id' => __('Uploaded to [ID]', 'aaostracts'),
            'author' => __('Author', 'aaostracts'),
            'filetype' => __('File Type', 'aaostracts'),
            'filesize' => __('File Size', 'aaostracts'),
            'download' => __('Download', 'aaostracts')
        );
        return $columns;
    }

    /**
     *
     * @return array
     */
    function get_sortable_columns() {
        $sortable_columns = array(
            'filename' => array('filename',true),
            'filesize' => array('filesize',false),
            'filetype' => array('filetype',false),
            'abstracts_id' => array('abstracts_id',false)
        );
        return $sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'delete' => __('Delete', 'aaostracts')
        );
        return $actions;
    }

    function process_bulk_action() {
        if ( 'delete'=== $this->current_action() ) {
            foreach($_GET['attachment'] as $attachment) {
                aaostracts_deleteAttachment($attachment, false);
            }
        }
    }

    function get_search_box() { ?>
        <label>Search Abstracts</label>
        <input type="search" class="aaostracts form-control" id="abstract_search" placeholder="abstract title or file name" name="s" value="<?php _admin_search_query(); ?>" />
        <?php submit_button( __('Search', 'aaostracts'), 'secondary', false, false ); ?>
    <?php
    }

    function extra_tablenav( $which ) {

	if ( $which == "top" ){
            $this->get_search_box();
	}
    }

    function prepare_items() {
        global $wpdb, $_wp_column_headers;
	$screen = get_current_screen();
        $attachments_tbl = $wpdb->prefix."aaostracts_attachments";
        $abstracts_tbl = $wpdb->prefix."aaostracts_abstracts";
        $query = "SELECT * FROM " . $attachments_tbl . " AS atts ";
        $query .="LEFT JOIN " . $abstracts_tbl . " AS abs ";
        $query .="ON atts.abstracts_id = abs.abstract_id";
        if(isset($_GET['s']) && $_GET['s']){
            $term = trim(sanitize_text_field($_GET['s']));
            $query .= " WHERE (abs.abstract_id LIKE '%%%$term%%' OR abs.title LIKE '%%%$term%%' OR atts.filename LIKE '%%%$term%%')";
        }
        $orderby = !empty($_GET["orderby"]) ? sanitize_text_field($_GET["orderby"]) : 'attachment_id';
        $order = !empty($_GET["order"]) ? sanitize_text_field($_GET["order"]) : 'desc';
        if(!empty($orderby) & !empty($order)){ $query.=' ORDER BY '.$orderby.' '.$order; }
        $totalitems = $wpdb->get_var("SELECT COUNT(*) FROM " . $attachments_tbl); // get  the total number of rows
        //How many to display per page?
        $perpage = 15;
        //Which page is this?
        $paged = !empty($_GET["paged"]) ? intval($_GET["paged"]) : '';
        //Page Number
        if(empty($paged) || !is_numeric($paged) || $paged<=0 ){ $paged=1; }
        //How many pages do we have in total?
        $totalpages = ceil($totalitems/$perpage);
        //adjust the query to take pagination into account
        if(!empty($paged) && !empty($perpage)){
            $offset=($paged-1)*$perpage;
            $query.=' LIMIT '.(int)$offset.','.(int)$perpage;
        }

        /* -- Register the pagination -- */
        $this->set_pagination_args( array(
            "total_items" => $totalitems,
            "total_pages" => $totalpages,
            "per_page" => $perpage,
        ) );
        /* -- Register the pagination -- */
        $this->process_bulk_action();

        /* -- Fetch the items -- */
        $this->items = $wpdb->get_results($query);

        /* -- Register the Columns -- */
        $columns = $this->get_columns();
        $_wp_column_headers[$screen->id]=$columns;
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);
    } // end prepare items

} // end Event Table class

class AAOStract_singleItem_Reviews_Table extends WP_List_Table {


    function __construct($id){

        global $status, $page;


        //Set parent defaults
        parent::__construct( array(
            'singular' => __('review', 'aaostracts'), //singular name of the listed records
            'plural' => __('reviews', 'aaostracts'), //plural name of the listed records
            'ajax'      => false,        //does this table support ajax?
            'id' => $id
        ) );

    }

    function column_title($item){
        global $wpdb;
        $abstract = $wpdb->get_row("SELECT title FROM ".$wpdb->prefix."aaostracts_abstracts WHERE abstract_id = $item->abstract_id", ARRAY_A);
        $actions = array(
            'edit' => '<a href="?page=aaostracts&tab=reviews&task=edit&id=' . $item->review_id . '">' . __('Edit', 'aaostracts') . '</a>',
            'delete' => '<a href="javascript:aaostracts_delete_review(' . $item->review_id . ')">' . __('Delete', 'aaostracts') . '</a>',
        );
        return sprintf('%1$s<span style="color:silver"></span>%2$s',$abstract['title'], $this->row_actions($actions));
    }

    function column_comments($item){
        $user_info = get_userdata($item->user_id);
             if($user_info){
                 $reviewer = $user_info->display_name;
             }else{
                 $reviewer = __("User deleted", 'wpabastracts');
             }
        $lastUpdated = date('M d Y g:i a', strtotime($item->review_date));
        return sprintf('%1$s<br><span style="color:silver">' . __('Reviewed by', 'aaostracts') . ': %2$s | %3$s</span>', $item->comments, $reviewer, $lastUpdated);
    }

    function column_cb($item) {
        return sprintf('<input type="checkbox" name="%1$s[]" value="%2$s" />', $this->_args['singular'], $item->review_id);
    }

    function column_default( $item, $column_name ) {
        switch( $column_name ) {
          case 'status': _e($item->$column_name, 'aaostracts');
                break;
            case 'recommendation': _e($item->$column_name, 'aaostracts');
                break;
        }
    }

    function column_criteria($item){
        if(!$item->customized){
            $relevance = __('Relevance', 'aaostracts') . ' : <strong>' . $item->relevance . '</strong>';
            $quality = __('Quality', 'aaostracts') . ' : <strong>' . $item->quality . '</strong>';
            echo $relevance . '<br>' . $quality . '<br>';
        }else{
            _e('The review entered for this item matches a different criteria.', 'aaostracts');
        }
    }

    function get_columns(){
        $columns = array(
            'cb'        => '<input type="checkbox" />',
            'title' => __('Abstract', 'aaostracts'),
            'comments' => __('Comments', 'aaostracts'),
            'criteria' => __('Criteria', 'aaostracts'),
            'recommendation' => __('Type', 'aaostracts'),
            'status' => __('Status', 'aaostracts')
        );
        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'title'     => array('title',false),
            'status'    => array('status',false),
            'recommendaton'    => array('recommendaton',false)
        );
        return $sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'delete' => __('Delete', 'aaostracts')
        );
        return $actions;
    }

    function process_bulk_action() {
        if ( 'delete'=== $this->current_action() ) {
            foreach($_GET['review'] as $review) {
                aaostracts_deleteReview($review, false);
            }
        }
    }

    function prepare_items() {
        global $wpdb, $aID, $_wp_column_headers;
        $abstract_id = $this->_args['id'];

	$screen = get_current_screen();
        $table_name = $wpdb->prefix."aaostracts_reviews";
        $query = "SELECT * FROM " . $table_name . " WHERE abstract_id = " . $abstract_id;
        $orderby = !empty($_GET["orderby"]) ? sanitize_text_field($_GET["orderby"]) : 'review_id';
        $order = !empty($_GET["order"]) ? sanitize_text_field($_GET["order"]) : 'desc';
        if(!empty($orderby) & !empty($order)){ $query.=' ORDER BY '.$orderby.' '.$order; }
        $totalitems = $wpdb->get_var("SELECT COUNT(*) FROM " . $table_name); // get  the total number of rows
        $perpage = 15;
        $paged = !empty($_GET["paged"]) ? intval($_GET["paged"]) : '';
        if(empty($paged) || !is_numeric($paged) || $paged<=0 ){ $paged=1; }
        $totalpages = ceil($totalitems/$perpage);
        if(!empty($paged) && !empty($perpage)){
            $offset=($paged-1)*$perpage;
            $query.=' LIMIT '.(int)$offset.','.(int)$perpage;
        }

        $this->set_pagination_args( array(
            "total_items" => $totalitems,
            "total_pages" => $totalpages,
            "per_page" => $perpage,
        ) );

        $this->process_bulk_action();

        $this->items = $wpdb->get_results($query);

        $columns = $this->get_columns();
        $_wp_column_headers[$screen->id]=$columns;
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);

    } // end prepare items

} // end REVIEW Table class

class AAOStract_Events_Table extends WP_List_Table {

    function __construct(){

        global $status, $page;

        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'event',     //singular name of the listed records
            'plural'    => 'events',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );

    }

    function column_name($item){
        $actions = array(
            'edit' => '<a href="?page=aaostracts&tab=events&task=edit&id=' . $item->event_id . '">' . __('Edit', 'aaostracts') . '</a>',
            'delete' => '<a href="javascript:aaostracts_delete_event(' . $item->event_id . ')">' . __('Delete', 'aaostracts') . '</a>'
        );
        return sprintf('%1$s <span style="color:silver">[ID: %2$s]</span>%3$s',$item->name, $item->event_id, $this->row_actions($actions));
    }

    function column_cb($item) {
        return sprintf('<input type="checkbox" name="%1$s[]" value="%2$s" />', $this->_args['singular'], $item->event_id);
    }

    function column_count($item){
        global $wpdb;
        $count = $wpdb->get_var("SELECT COUNT(*) FROM ".$wpdb->prefix."aaostracts_abstracts WHERE event = ". $item->event_id);
        echo $count;
    }

     function column_default( $item, $column_name ) {
        switch( $column_name ) {
          case 'name': echo stripslashes($item->$column_name); break;
          case 'address': echo stripslashes($item->$column_name); break;
          case 'host': echo stripslashes($item->$column_name); break;
          case 'topics': echo stripslashes($item->$column_name); break;
          case 'start_date': echo stripslashes($item->$column_name); break;
          case 'end_date': echo stripslashes($item->$column_name); break;
          case 'deadline': echo stripslashes($item->$column_name); break;
        }
    }

    function get_columns(){
        $columns = array(
            'cb'        => '<input type="checkbox" />',
            'name' => __('Event Name', 'aaostracts'),
            'address' => __('Location', 'aaostracts'),
            'host' => __('Host', 'aaostracts'),
            'topics' => __('Topics', 'aaostracts'),
            'start_date' => __('Start Date', 'aaostracts'),
            'end_date' => __('End Date', 'aaostracts'),
            'deadline' => __('Deadline', 'aaostracts'),
            'count' => __('Submissions', 'aaostracts'),
        );
        return $columns;
    }

    /**
     *
     * @return array
     */
    function get_sortable_columns() {
        $sortable_columns = array(
            'event_id'    => array('event_id',true),
            'name'     => array('name',false),
            'host'    => array('host',false),
            'start_date'    => array('start_date',false),
            'end_date'    => array('end_date',false)
        );
        return $sortable_columns;
    }

    /**
     *
     * @return array
     */
    function get_bulk_actions() {
        $actions = array(
            'delete' => __('Delete', 'aaostracts')
        );
        return $actions;
    }

    function process_bulk_action() {
        if ( 'delete'=== $this->current_action() ) {
            foreach($_GET['event'] as $event) {
                aaostracts_deleteEvent($event, false);
            }
        }
    }

    function prepare_items() {
        global $wpdb, $_wp_column_headers;
	$screen = get_current_screen();
        $table_name = $wpdb->prefix."aaostracts_events";
        $query = "SELECT * FROM  . $table_name";
        $orderby = !empty($_GET["orderby"]) ? sanitize_text_field($_GET["orderby"]) : 'event_id';
        $order = !empty($_GET["order"]) ? sanitize_text_field($_GET["order"]) : 'desc';
        if(!empty($orderby) & !empty($order)){ $query.=' ORDER BY '.$orderby.' '.$order; }
        $totalitems = $wpdb->get_var("SELECT COUNT(*) FROM " . $table_name); // get  the total number of rows
        //How many to display per page?
        $perpage = 15;
        //Which page is this?
        $paged = !empty($_GET["paged"]) ? intval($_GET["paged"]) : '';
        //Page Number
        if(empty($paged) || !is_numeric($paged) || $paged<=0 ){ $paged=1; }
        //How many pages do we have in total?
        $totalpages = ceil($totalitems/$perpage);
        //adjust the query to take pagination into account
        if(!empty($paged) && !empty($perpage)){
            $offset=($paged-1)*$perpage;
            $query.=' LIMIT '.(int)$offset.','.(int)$perpage;
        }

        /* -- Register the pagination -- */
        $this->set_pagination_args( array(
            "total_items" => $totalitems,
            "total_pages" => $totalpages,
            "per_page" => $perpage,
        ) );
        /* -- Register the pagination -- */
        $this->process_bulk_action();

        /* -- Fetch the items -- */
        $this->items = $wpdb->get_results($query);

        /* -- Register the Columns -- */
        $columns = $this->get_columns();
        $_wp_column_headers[$screen->id]=$columns;
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);
    } // end prepare items

} // end Event Table class

class AAOStracts_EmailsTemplates extends WP_List_Table {

    function __construct(){
        global $status, $page;

        //Set parent defaults
        parent::__construct( array(
            'singular' => 'template', //singular name of the listed records
            'plural'    => 'templates',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );

    }

    function column_name($item){
        global $wpdb;
        $actions = array(
            'edit' => '<a href="?page=aaostracts&tab=emails&task=edit&id=' . $item->ID . '">' . __('Edit', 'aaostracts') . '</a>',
        );
        return sprintf('%1$s<span style="color:silver"> [%2$s]</span>%3$s', $item->name, $item->ID, $this->row_actions($actions));
    }

    function column_cb($item) {
        return sprintf('<input type="checkbox" name="%1$s[]" value="%2$s" />', $this->_args['singular'], $item->ID);
    }

    function column_default( $item, $column_name ) {
        switch( $column_name ) {
            case 'id': echo $item->$column_name; break;
            case 'name': _e($item->$column_name, 'aaostracts');break;
            case 'subject': _e($item->$column_name, 'aaostracts');break;
            case 'from_name': _e($item->$column_name, 'aaostracts');break;
            case 'from_email': _e($item->$column_name, 'aaostracts');break;
            case 'receiver': _e($item->$column_name, 'aaostracts');break;
        }
    }

    function get_columns(){
        $columns = array(
            'cb'        => '<input type="checkbox" />',
            'name' => __('Email Template', 'wpconference'),
            'subject' => __('Subject', 'wpconference'),
            'from_name' => __('From Name', 'wpconference'),
            'from_email' => __('From Email', 'wpconference'),
            'receiver' => __('Email Group', 'wpconference'),
        );
        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'name'    => array('id',true)
        );
        return $sortable_columns;
    }

    function process_bulk_action() {

        if ( 'delete'=== $this->current_action() ) {

        }
    }

    function prepare_items() {
        global $wpdb, $_wp_column_headers;
	$screen = get_current_screen();
        $table_name = $wpdb->prefix."aaostracts_emailtemplates";
        $query = "SELECT * FROM  . $table_name";
        $orderby = !empty($_GET["orderby"]) ? sanitize_text_field($_GET["orderby"]) : 'ID';
        $order = !empty($_GET["order"]) ? sanitize_text_field($_GET["order"]) : 'desc';
        if(!empty($orderby) & !empty($order)){ $query.=' ORDER BY '.$orderby.' '.$order; }
        $totalitems = $wpdb->get_var("SELECT COUNT(*) FROM " . $table_name); // get  the total number of rows
        $perpage = 15;
        $paged = !empty($_GET["paged"]) ? intval($_GET["paged"]) : '';
        if(empty($paged) || !is_numeric($paged) || $paged<=0 ){ $paged=1; }
        $totalpages = ceil($totalitems/$perpage);
        if(!empty($paged) && !empty($perpage)){
            $offset=($paged-1)*$perpage;
            $query.=' LIMIT '.(int)$offset.','.(int)$perpage;
        }
        $this->set_pagination_args( array(
            "total_items" => $totalitems,
            "total_pages" => $totalpages,
            "per_page" => $perpage,
        ) );
        /* -- Register the pagination -- */
        $this->process_bulk_action();

        /* -- Fetch the items -- */
        $this->items = $wpdb->get_results($query);

        /* -- Register the Columns -- */
        $columns = $this->get_columns();
        $_wp_column_headers[$screen->id]=$columns;
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);
    } // end prepare items

} // end

class AAOStracts_Users extends WP_List_Table {

    function __construct(){
        global $status, $page;

        //Set parent defaults
        parent::__construct( array(
            'singular' => 'user', //singular name of the listed records
            'plural'    => 'users',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );

    }

    function column_cb($item) {
        return sprintf('<input type="checkbox" name="%1$s[]" value="%2$s" />', $this->_args['singular'], $item->ID);
    }

    function column_ID($item){
        $fullname = ($item->display_name != "") ? $item->display_name : $item->user_login;
        $actions = array(
            'edit' => '<a href=' . admin_url( "user-edit.php?user_id=$item->ID" ) . '>' . __('Edit', 'aaostracts') . '</a>',
            'delete' => '<a href="javascript:aaostracts_delete_user(' . $item->ID . ')">' . __('Delete', 'aaostracts') . '</a>',
        );
        return sprintf('%1$s %2$s', $fullname, $this->row_actions($actions));
    }

    function column_default( $item, $column_name ) {
        $user = get_user_by( 'id', $item->ID );
        switch ($user->roles[0]){
            case 'subscriber':
                $user_role = "Author"; break;
            case 'editor':
                $user_role = "Reviewer"; break;
            case 'administrator':
                $user_role = "Site Admin"; break;
            default :
                $user_role = $user->roles[0]; break;
        }
        switch( $column_name ) {
            case 'user_email':
                _e($user->user_email, 'aaostracts');
                break;
            case 'user_login':
                _e($user->user_login, 'aaostracts');
                break;
            case 'account':
                _e($user_role, 'wpconference');
                break;
        }
    }

    function get_columns(){
        $columns = array(
            'cb'        => '<input type="checkbox" />',
            'ID' => __('Full Name', 'aaostracts'),
            'user_login' => __('Username', 'aaostracts'),
            'user_email' => __('Email', 'aaostracts'),
            'account' => __('Type', 'aaostracts')
        );
        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'ID'    => array('ID',true),
            'user_login'    => array('user_login',false),
            'user_email'    => array('user_email',false)
        );
        return $sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'delete' => __('Delete', 'aaostracts'),
        );
        return $actions;
    }

    function process_bulk_action() {

        if ( 'delete'=== $this->current_action() ) {
            foreach($_GET['user'] as $user) {
                wpconference_deleteUser($user, false);
            }
        }
    }

    function get_user_filter(){
        $current = (isset($_GET['user_filter']) && $_GET['user_filter']) ? $_GET['user_filter'] : ""; ?>
        <label>Filter by Role</label>
        <select name="user_filter" onchange="user_list.submit();">
            <option value="" <?php selected($current , ""); ?>>All Users</option>
            <option value="subscriber" <?php selected($current , "subscriber"); ?>>Authors</option>
            <option value="editor" <?php selected($current , "editor"); ?>>Reviewers</option>
            <option value="administrator" <?php selected($current , "administrator"); ?>>Administrators</option>
        </select>
        <?php
    }

    function get_search_box() { ?>
    <label>Search Users</label>
        <input type="search" class="aaostracts form-control" id="application_search" placeholder="name, username or email" name="s" value="<?php _admin_search_query(); ?>" />
        <?php submit_button( __('Search', 'aaostracts'), 'secondary', false, false ); ?>
    <?php
    }

    function extra_tablenav( $which ) {

	if ( $which == "top" ){
            $this->get_user_filter();
            $this->get_search_box();
	}
    }

    function prepare_items() {
        global $_wp_column_headers;
	$screen = get_current_screen();
        $orderby = !empty($_GET["orderby"]) ? sanitize_text_field($_GET["orderby"]) : 'ID';
        $order = !empty($_GET["order"]) ? sanitize_text_field($_GET["order"]) : 'desc';
        if(!empty($orderby) & !empty($order)){ $query='orderby='.$orderby.'&order='.$order; }
        if(isset($_GET['user_filter']) && $_GET['user_filter']){
            $query .= "&role=" . trim(sanitize_text_field($_GET['user_filter']));
        }
        $totalitems = count(get_users($query)); //return the total number of affected rows
        $perpage = 30;
        $paged = !empty($_GET["paged"]) ? intval($_GET["paged"]) : '';
        if(empty($paged) || !is_numeric($paged) || $paged<=0 ){ $paged=1; }
        $totalpages = ceil($totalitems/$perpage);
        if(!empty($paged) && !empty($perpage)){
            $query.='&number='.(int)$perpage;
        }

        if(isset($_GET['s'])){
            $query .= "&search=*" . trim(sanitize_text_field($_GET['s'])) . "*";
        }

        $this->set_pagination_args( array(
            "total_items" => $totalitems,
            "total_pages" => $totalpages,
            "per_page" => $perpage,
        ) );
        /* -- Register the pagination -- */
        $this->process_bulk_action();

        /* -- Fetch the items -- */
        $this->items = get_users($query);

        /* -- Register the Columns -- */
        $columns = $this->get_columns();
        $_wp_column_headers[$screen->id]=$columns;
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);
    } // end prepare items

} // end