<?php

class AAOStracts_Emailer{

    protected $abstract = null;
    protected $user = null;
    protected $event = null;
    protected $template = null;


    public function __construct($aid, $user_id, $template) {
        global $wpdb;
        $wpdb->hide_errors();
        if($aid){
            $this->abstract = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."aaostracts_abstracts WHERE abstract_id = $aid");
            $this->event = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."aaostracts_events WHERE event_id = " . $this->abstract->event . "");
            $this->user = get_user_by( 'id', $user_id );
            $this->template = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."aaostracts_emailtemplates WHERE ID = $template");
        }
    }

    private function message(){
        $keys = array(
            '{DISPLAY_NAME}',
            '{USERNAME}',
            '{ABSTRACT_TITLE}',
            '{ABSTRACT_ID}',
            '{EVENT_NAME}',
            '{EVENT_START}',
            '{EVENT_END}',
            '{PRESENTER_PREF}',
            '{SITE_NAME}',
            '{SITE_URL}',
            '{ONE_WEEK_LATER}',
            '{TWO_WEEKS_LATER}');

        $one_week_later = date_i18n(get_option('date_format'), (60 * 60 * 24 * 7) + strtotime(current_time( 'mysql' )));
        $two_weeks_later = date_i18n(get_option('date_format'), ((60 * 60 * 24 * 7) * 2) + strtotime(current_time( 'mysql' )));
        $site_name = get_option('blogname');
        $site_url = home_url();

        $values = array(
            $this->user->display_name,
            $this->user->user_login,
            $this->abstract->title,
            $this->abstract->abstract_id,
            $this->event->name,
            $this->event->start_date,
            $this->event->end_date,
            $this->abstract->presenter_preference,
            $site_name,
            $site_url,
            $one_week_later,
            $two_weeks_later
        );

        $message = str_replace($keys, $values, wpautop(stripslashes($this->template->message)));

        return $message;
    }

    public function send(){
        $to = $this->user->user_email;
        $subject = $this->template->subject;
        $headers = __('From:', 'aaostracts') . $this->template->from_name . " <" . $this->template->from_email . "> \r\n";
        $message = $this->message();
        add_filter( 'wp_mail_content_type', 'aaostracts_set_html_content_type' );
        $success = wp_mail($to, $subject, $message, $headers);
        remove_filter( 'wp_mail_content_type', 'aaostracts_set_html_content_type' );
        return $success;
    }

}


