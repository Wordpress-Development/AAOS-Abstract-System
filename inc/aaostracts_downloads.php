<?php
defined('ABSPATH') or die("ERROR: You do not have permission to access this page");

if(is_user_logged_in() && is_super_admin()){
    do_action('aaostracts_before_download');
     if (isset($_GET['task']) && ($_GET['task']) == 'download') {
        if(isset($_GET['type']) && ($_GET['type'])){
            $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
            $type = $_GET['type'];
            switch($type){
                case 'attachment':
                    if(has_action('aaostracts_download_attachment')){
                        do_action('aaostracts_download_attachment', $id);
                    }else{
                        aaostracts_downloadAttachment($id);
                    }
                            break;
                case 'pdf':
                    if(has_action('aaostracts_download_pdf')){
                        do_action('aaostracts_download_pdf', $id);
                    }else{
                        aaostracts_downloadPDF($id);
                    }
                    break;
                case 'abstracts':
                    if(has_action('aaostracts_download_report')){
                        do_action('aaostracts_download_report', 'abstracts', $id);
                    }else{
                        aaostracts_downloadReport('abstracts', $id);
                    }
                    break;
                case 'reviews':
                    if(has_action('aaostracts_download_report')){
                        do_action('aaostracts_download_report', 'reviews', $id);
                    }else{
                        aaostracts_downloadReport('reviews', $id);
                    }
                    break;
                case 'zip':
                    aaostracts_downloadZip();
                    break;
            }
        }
    }
}else if(is_user_logged_in()){
    do_action('aaostracts_before_download');
    if (isset($_GET['task']) && ($_GET['task']) == 'download') {
        if(isset($_GET['type']) && ($_GET['type'])){
            $type = $_GET['type'];
            $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
            switch($type){
                case 'attachment':
                    if(has_action('aaostracts_download_attachment')){
                        do_action('aaostracts_download_attachment', $id);
                    }else{
                        aaostracts_downloadAttachment($id);
                    }
                    break;
            }
        }
    }
}else{
    defined('ABSPATH') or die("ERROR: You do not have permission to access this resource.");
}

function aaostracts_downloadAttachment($id) {
    global $wpdb;
    $file = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "aaostracts_attachments WHERE attachment_id=" . $id);
    $content = rawurldecode($file->filecontent);
    header("Cache-Control: no-cache, must-revalidate");
    header("Content-length: " . $file->filesize);
    header("Content-type: " . $file->filetype);
    header("Content-Disposition: attachment; filename=\"$file->filename\"");
    echo $content;
    exit(0);
}

function aaostracts_downloadReport($reportType, $reportID) {
    global $wpdb;
    $reportData = null;
    if ($reportType == "abstracts") {

        $report_header = array(
            __("Abstract", 'aaostracts') . "ID",
            __("Title", 'aaostracts'),
            __("Description", 'aaostracts'),
            __("Event", 'aaostracts') . "ID",
            __("Topic", 'aaostracts'),
            __("Status", 'aaostracts'),
            __("Author", 'aaostracts'),
            __("Author Email", 'aaostracts'),
            __("Author Affiliation", 'aaostracts'),
            __("Presenter", 'aaostracts'),
            __("Presenter Email", 'aaostracts'),
            __("Preference", 'aaostracts'),
            __("Reviewer", 'aaostracts') . "ID1",
            __("Reviewer", 'aaostracts') . "ID2",
            __("Reviewer", 'aaostracts') . "ID3",
            __("User", 'aaostracts') . "ID",
            __("User Name", 'aaostracts'),
            __("Date Submitted", 'aaostracts'));

        if ($reportID == 1) {
            $reportName = __("Approved Abstracts", 'aaostracts');
            $reportData = $wpdb->get_results("SELECT abstract_id, title, text, event, topic,
                                status, author, author_email, author_affiliation, presenter, presenter_email,
                                presenter_preference, reviewer_id1, reviewer_id2, reviewer_id3, ID, display_name,
                                submit_date AS abstracts FROM " . $wpdb->prefix . "aaostracts_abstracts as abstracts
                                LEFT JOIN ". $wpdb->prefix . "users as users
                                ON users.ID = abstracts.submit_by
                                WHERE status = 'Approved' OR status = 'Accepted'", ARRAY_N);
        } else if ($reportID == 2) {
            $reportName = __("Pending Abstracts", 'aaostracts');
            $reportData = $wpdb->get_results("SELECT abstract_id, title, text, event, topic,
                                status, author, author_email, author_affiliation, presenter, presenter_email,
                                presenter_preference, reviewer_id1, reviewer_id2, reviewer_id3, ID, display_name,
                                submit_date AS abstracts FROM " . $wpdb->prefix . "aaostracts_abstracts as abstracts
                                LEFT JOIN ". $wpdb->prefix . "users as users
                                ON users.ID = abstracts.submit_by
                                WHERE status = 'Pending'", ARRAY_N);
        } else if ($reportID == 3) {
            $reportName = __("Rejected Abstracts", 'aaostracts');
            $reportData = $wpdb->get_results("SELECT abstract_id, title, text, event, topic,
                                status, author, author_email, author_affiliation, presenter, presenter_email,
                                presenter_preference, reviewer_id1, reviewer_id2, reviewer_id3, ID, display_name,
                                submit_date AS abstracts FROM " . $wpdb->prefix . "aaostracts_abstracts as abstracts
                                LEFT JOIN ". $wpdb->prefix . "users as users
                                ON users.ID = abstracts.submit_by
                                WHERE status = 'Rejected'", ARRAY_N);
        } else if ($reportID == 4) {
            $reportName = __("All Abstracts", 'aaostracts');
            $reportData = $wpdb->get_results("SELECT abstract_id, title, text, event, topic,
                                status, author, author_email, author_affiliation, presenter, presenter_email,
                                presenter_preference, reviewer_id1, reviewer_id2, reviewer_id3, ID, display_name,
                                submit_date AS abstracts FROM " . $wpdb->prefix . "aaostracts_abstracts as abstracts
                                LEFT JOIN ". $wpdb->prefix . "users as users
                                ON users.ID = abstracts.submit_by", ARRAY_N);
        }
    } else if ($reportType == "reviews") {

        $report_header = array(__("ReviewID", 'aaostracts'), __("AbstractID", 'aaostracts'), __("UserID", 'aaostracts'), __("Status", 'aaostracts'), __("Relevance", 'aaostracts'),
            __("Quality", 'aaostracts'), __("Comments", 'aaostracts'), __("Recommendation", 'aaostracts'), __("ReviewDate", 'aaostracts'));

        if ($reportID == 1) {
            $reportName = __("Excellent Reviews", 'aaostracts');
            $reportData = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "aaostracts_reviews
                                            WHERE relevance = 'Excellent'", ARRAY_N);
        } else if ($reportID == 2) {
            $reportName = __("Good Reviews", 'aaostracts');
            $reportData = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "aaostracts_reviews
                                            WHERE relevance = 'Good'", ARRAY_N);
        } else if ($reportID == 3) {
            $reportName = __("Average Reviews", 'aaostracts');
            $reportData = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "aaostracts_reviews
                                            WHERE relevance = 'Average'", ARRAY_N);
        } else if ($reportID == 4) {
            $reportName = __("Poor Reviews", 'aaostracts');
            $reportData = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "aaostracts_reviews
                                            WHERE relevance = 'Poor'", ARRAY_N);
        } else if ($reportID == 5) {
            $reportName = __("All Reviews", 'aaostracts');
            $reportData = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "aaostracts_reviews", ARRAY_N);
        }
    }

    $reportName = $reportName . ".csv";
    header("Cache-Control: no-cache, must-revalidate");
    header("Content-Type: text/csv");
    header("Content-Disposition: attachment; filename=\"$reportName\"");
    ob_start();
    $file_report = fopen('php://output', 'w');
    fputcsv($file_report, array_values($report_header), ",");
    foreach ($reportData AS $data) {
        fputcsv($file_report, array_values(stripslashes_deep($data)), ",");
    }
    fclose($file_report);
    $report = ob_get_contents();
    ob_end_clean();
    echo $report;
    exit(0);
}

function aaostracts_downloadPDF($abstractID) {
    global $wpdb;
    $abstract = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "aaostracts_abstracts WHERE abstract_id = $abstractID");
    $event = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "aaostracts_events WHERE event_id = " . $abstract[0]->event);

    include("mpdf/mpdf.php");
    $mpdf = new mPDF();
    $styleUrl = plugins_url('../css/pdf.css', __FILE__);
    $stylesheet = file_get_contents($styleUrl);
    $mpdf->WriteHTML($stylesheet, 1);
    $header = __("Abstract ID", 'aaostracts') . ": " . $abstract[0]->abstract_id . " for " . get_option(blogname) . " (" . __("Auto-Generated", 'aaostracts') . " " . date_i18n(get_option('date_format') . ' ' . get_option('time_format')) . ")";
    $mpdf->SetHeader($header);
    $filename = $abstract[0]->title . " (ID_" . $abstract[0]->abstract_id . ").pdf";
    $mpdf->SetTitle($filename);
    $mpdf->SetAuthor($abstract[0]->author);
    $footer = "Copyright " . date("Y") . " " . get_option(blogname) . " powered by AAOStracts Pro";
    $mpdf->SetFooter($footer);
    ob_start();
    ?>
    <div class="aaostracts pdf">
        <div class="aaostracts pdf_header">
            <h1><?php echo $abstract[0]->title; ?>
                <?php if(get_option('aaostracts_show_author')){?>
                <?php $author =  ($abstract[0]->author) ? "by " . $abstract[0]->author : " - No author found -"; ?>
                <span class="aaostracts pdf_author"><?php echo $author; ?></span>
                <?php } ?>
            </h1>
        </div>
        <div class="aaostracts pdf_headerbar">
            <?php _e('Abstract Id', 'aaostracts'); ?>: <strong><?php echo $abstract[0]->abstract_id; ?></strong>
            <?php _e('Submitted', 'aaostracts'); ?>: <strong><?php echo date_i18n( get_option( 'date_format' ), strtotime(  $abstract[0]->submit_date ) ); ?></strong>
            <?php _e('Event', 'aaostracts'); ?>: <strong><?php echo $event->name; ?></strong>
            <?php _e('Topic', 'aaostracts'); ?>: <strong><?php echo $abstract[0]->topic; ?></strong>
        </div>
        <div class="aaostracts pdf_body">
            <div class="pdf_text">
                <?php echo wpautop(stripslashes($abstract[0]->text)); ?>
            </div>
        </div>
    </div>

    <?php
    $html = ob_get_contents();
    ob_end_clean();
    $mpdf->WriteHTML($html, 2);
    $mpdf->Output($filename, "D");
    exit(0);
}

function aaostracts_downloadZip(){
    global $wpdb;
    $attachments = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."aaostracts_attachments");
    $tempPath = AAOSTRACTS_PLUGIN_DIR . 'temp';
    $zip = new ZipArchive();
    $tempFiles = array();

    $zip_name = 'aaostracts_attachments.zip';

    if ($zip->open($zip_name, ZipArchive::CREATE | ZipArchive::OVERWRITE) !== true) {
        wp_die("An error occurred creating your ZIP file. Maybe your server lacks support for this feature or it has no space for temp files.");
    }

    foreach($attachments as $attachment){

        $content = rawurldecode($attachment->filecontent);
        $filename = "ID_". $attachment->attachment_id . "_" . $attachment->filename;
        $file = $tempPath ."/". $filename;

        if(file_put_contents($file, $content)){
            $tempFiles[] = $file;
            $zip->addFile($file, $filename);
        }
    }

    $zip->close();

    foreach ($tempFiles as $file){
        unlink($file);
    }

    header('Content-Type: application/zip');
    header("Content-Disposition: attachment; filename=\"$zip_name\"");
    header('Content-Length: ' . filesize($zip_name));
    header("Location: " . $zip_name);
    exit(0);
}

