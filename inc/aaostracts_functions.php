<?php
defined('ABSPATH') or die("ERROR: You do not have permission to access this page");

function aaostracts_getAbstracts($field, $value, $format){
    global $wpdb;
    $wpdb->show_errors();
    $ret_value = null;
    switch ($field){
        case 'user_id':
            $sql = $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."aaostracts_abstracts
                                   WHERE submit_by = %d", $value);
            $ret_value = $wpdb->get_results($sql);
        break;
        case 'abstract_id':
            $sql = $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."aaostracts_abstracts
                                   WHERE abstract_id = %d", $value, $format);
            $ret_value = $wpdb->get_results($sql);
        break;
        case 'all':
            $sql = $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."aaostracts_abstracts");
            $ret_value = $wpdb->get_results($sql);
            break;

    }
    return $ret_value;

}

function aaostracts_getReviews($field, $value){
    global $wpdb;
    $ret_value = null;
    switch ($field){
        case 'user_id':
            $sql = $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."aaostracts_reviews
                                   WHERE user_id = %d", $value);
            $ret_value = $wpdb->get_results($sql);
        break;
        case 'abstract_id':
            $sql = $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."aaostracts_reviews
                                   WHERE abstract_id = %d", $value, ARRAY_N);
            $ret_value = $wpdb->get_results($sql);
        break;
        case 'review_id':
            $sql = $wpdb->prepare("SELECT * FROM ".$wpdb->prefix."aaostracts_reviews WHERE review_id = %d", $value);
            $ret_value = $wpdb->get_row($sql);
        break;
    }
    return $ret_value;

}

function aaostracts_getAttachments($field, $value){
    global $wpdb;
    $ret_val = null;
    switch ($field){
        case 'abstracts_id':
            $attachments = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."aaostracts_attachments WHERE abstracts_id=".$value);
            $ret_val = $attachments;
            break;

    }
    return $ret_val;

}

function aaostracts_getEvents($field, $value, $format){
    global $wpdb;
    $results = null;
    switch ($field){
        case 'event_id':
            $results = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."aaostracts_events WHERE event_id = $value", $format);
            break;
        case 'all':
            $results = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix."aaostracts_events WHERE deadline >= CURDATE()");
            break;
    }
    return $results;
}

function aaostracts_manage_abstracts($id, $action){
    global $wpdb;
    if($_POST){
        $abs_title = sanitize_text_field($_POST["abs_title"]);
        $abs_text = wp_kses_post($_POST["abstext"]);
        $abs_event = isset($_POST["abs_event"]) ? intval($_POST["abs_event"]) : '';
        $abs_keywords = isset($_POST["abs_keywords"]) ? sanitize_text_field($_POST["abs_keywords"]) : '';
        $abs_topic = isset($_POST["abs_topic"]) ? sanitize_text_field($_POST["abs_topic"]) : '';
        $abs_presenter = isset($_POST["abs_presenter"]) ? sanitize_text_field($_POST["abs_presenter"]) : '';
        $abs_presenter_email = isset($_POST["abs_presenter_email"]) ? strtolower(sanitize_email($_POST["abs_presenter_email"])) : '';
        $abs_presenter_preference = isset($_POST["abs_presenter_preference"]) ?  sanitize_text_field($_POST["abs_presenter_preference"]) : '';

        if(isset($_POST["abs_author"]) && sizeof($_POST["abs_author"]) > 1) {
            foreach($_POST["abs_author"] as $key=>$author) {
                $author = sanitize_text_field($_POST["abs_author"][$key]);
                if(strlen($author) > 0){
                    $abs_authors[] = $author;
                }

            }
            foreach($_POST["abs_author_email"] as $key=>$author_email) {
                $author_email = sanitize_email($_POST["abs_author_email"][$key]);
                if(strlen($author_email) > 0){
                    $abs_authors_email[] = $author_email;
                }

            }
            foreach($_POST["abs_author_affiliation"] as $key=>$author_affiliation) {
                $author_affiliation = sanitize_text_field($_POST["abs_author_affiliation"][$key]);
                if(strlen($author_affiliation) > 0){
                    $abs_authors_affiliation[] = $author_affiliation;
                }
            }
            $abs_authors = implode(' | ', $abs_authors);
            $abs_authors_email = implode(' | ', $abs_authors_email);
            $abs_authors_affiliation = implode(' | ', $abs_authors_affiliation);
        } else {
            $author = isset($_POST["abs_author"]) ? sanitize_text_field($_POST["abs_author"][0]) : '';
            $abs_authors = $author;
            $author_email = isset($_POST["abs_author_email"]) ? sanitize_email($_POST["abs_author_email"][0]) : '';
            $abs_authors_email = $author_email;
            $author_affiliation = isset($_POST["abs_author_affiliation"]) ? ($_POST["abs_author_affiliation"][0]) : '';
            $abs_authors_affiliation = $author_affiliation;
        }

        $user_ID = get_current_user_id();
        $date_time = current_time( 'mysql' );

        $prefilter_data = array(
            'title' => $abs_title,
            'text'      => $abs_text,
            'event'     => $abs_event,
            'keywords'  => $abs_keywords,
            'topic'     => $abs_topic,
            'status'    => "Pending",
            'author'    => $abs_authors,
            'author_email' => $abs_authors_email,
            'author_affiliation' => $abs_authors_affiliation,
            'presenter' => $abs_presenter,
            'presenter_email' => $abs_presenter_email,
            'presenter_preference' => $abs_presenter_preference,
            'submit_by' => $user_ID,
            'submit_date' => $date_time
        );

        $data = apply_filters('aaostracts_save_abstracts', $prefilter_data, $_POST);

    }

    switch($action){
        case 'insert':
            $wpdb->show_errors();
            $wpdb->insert($wpdb->prefix.'aaostracts_abstracts', $data);
            $abstract_id = $wpdb->insert_id;
            return $abstract_id;
        case 'update':
            $where = array('abstract_id' => $id);
            $wpdb->show_errors();
            $wpdb->update($wpdb->prefix.'aaostracts_abstracts', $data, $where);

            // user was changed
            if(isset($_POST['abs_user']) && $_POST['abs_user']){
                $abs_user = intval($_POST['abs_user']);
                $wpdb->query("UPDATE ".$wpdb->prefix."aaostracts_abstracts SET submit_by = " . $abs_user . " WHERE abstract_id = " . $id);
            }

            if(isset($_POST['abs_remove_attachments'])){
                $attachmentsIDs = (array) $_POST["abs_remove_attachments"];
                foreach($attachmentsIDs AS $attachID){
                    $wpdb->query("DELETE FROM ".$wpdb->prefix."aaostracts_attachments WHERE attachment_id=".intval($attachID));
                }
            }
        break;
        case 'delete':
            $wpdb->show_errors();
            $abstractID = intval($id);
            $wpdb->query("DELETE FROM ".$wpdb->prefix."aaostracts_abstracts WHERE abstract_id=".$abstractID);
            $wpdb->query("DELETE FROM ".$wpdb->prefix."aaostracts_reviews WHERE abstract_id=".$abstractID);
            $wpdb->query("DELETE FROM ".$wpdb->prefix."aaostracts_attachments WHERE abstracts_id=".$abstractID);
        break;
    }

}

function aaostracts_manageAttachments($id, $files, $action){
    global $wpdb;
     if($files) {
        do_action('aaostracts_before_upload', $id, $action);
        foreach($files['attachments']['error'] as $key=>$error) {
            if($error==0) {
                    $fileName = $files['attachments']['name'][$key];
                    $tmpName  = $files['attachments']['tmp_name'][$key];
                    $fileSize = $files['attachments']['size'][$key];
                    $fileType = $files['attachments']['type'][$key];
                    $fileExtension = explode('.',$fileName);
                    $fileExt = strtolower($fileExtension[count($fileExtension)-1]);
                    $approvedExtensions = explode(',',get_option('aaostracts_permitted_attachments'));
                    // checks for approved extension and file size
                    if(in_array($fileExt, $approvedExtensions) and $fileSize<=get_option('aaostracts_max_attach_size')) {
                            $fp = fopen($tmpName, 'r');
                            $fileContent = rawurlencode(fread($fp, $fileSize));
                            fclose($fp);
                            switch ($action){
                                case 'insert':
                                    $data = array(
                                        'abstracts_id' => $id,
                                        'filecontent' => $fileContent,
                                        'filename' => $fileName,
                                        'filetype' => $fileType,
                                        'filesize' => $fileSize
                                    );

                                    $filtered_data = apply_filters('aaostracts_add_attachment', $data, $id);
                                    $wpdb->show_errors();
                                    $wpdb->insert($wpdb->prefix."aaostracts_attachments", $filtered_data);
                                break;
                            }
                    } else{
                        // return error TODO
                    }
            }
        }
    }
}

function aaostracts_getAddView( $type, $id) {
    global $wpdb;
    $path = AAOSTRACTS_PLUGIN_DIR . 'html/aaostracts_add' . $type . '.php';
    $templatePath = apply_filters('aaostracts_template_path_addview', $path, $type);
    $html = null;
    switch($type){
        case 'Abstract' :
            $events = aaostracts_getEvents('all', 0, NULL);
            if(count($events) < 1){
                aaostracts_showMessage(__('There are no active events. Please ensure an event exists with a deadline later than yesterday.', 'aaostracts'), 'alert-danger');
                return;
            }
            ob_start();
            include ( $templatePath);
            $html = ob_get_contents();
            ob_end_clean();
            break;
        case 'Review':
            $abstract = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."aaostracts_abstracts WHERE abstract_id = $id", ARRAY_A);
            $attachments = aaostracts_getAttachments('abstracts_id', $id);
            $event = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."aaostracts_events where event_id=".$abstract['event']);
            ob_start();
            include ( $templatePath);
            $html = ob_get_contents();
            ob_end_clean();
            break;
        case 'Event' :
            ob_start();
            include ( $templatePath);
            $html = ob_get_contents();
            ob_end_clean();
            break;
        case 'User' :
            ob_start();
            include ( $templatePath);
            $html = ob_get_contents();
            ob_end_clean();
            break;
        default :
            ob_start();
            include ( $templatePath);
            $html = ob_get_contents();
            ob_end_clean();
    }
    echo $html;
}

function aaostracts_getEditView($type, $edit_id){
    global $wpdb;
    $id = intval($edit_id); // can never be too safe
    $html = null;
    $path = AAOSTRACTS_PLUGIN_DIR  . 'html/aaostracts_edit' . $type . '.php';
    $templatePath = apply_filters('aaostracts_template_path_editview', $path, $type);

    switch($type){
        case 'Abstract':
            $abstract = aaostracts_getAbstracts('abstract_id', $id, 'ARRAY_A');
            $event = aaostracts_getEvents('event_id', $abstract[0]->event, 'ARRAY_A');
            $events = aaostracts_getEvents('all', 0, NULL);
            $topics = explode(',',$event['topics']);
            $attachments = aaostracts_getAttachments('abstracts_id', $abstract[0]->abstract_id);
            ob_start();
            include ( $templatePath);
            $html = ob_get_contents();
            ob_end_clean();
            break;
        case 'Review':
            $review = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."aaostracts_reviews Where review_id = ".$id, ARRAY_A);
            $abstract = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."aaostracts_abstracts WHERE abstract_id = ".$review['abstract_id'], ARRAY_A);
            $event = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."aaostracts_events where event_id=".$abstract['event']);
            ob_start();
            include ( $templatePath);
            $html = ob_get_contents();
            ob_end_clean();
            break;
        case 'Event':
            $abs_event = aaostracts_getEvents('event_id', $id, 'ARRAY_A');
            $topics = explode(", ", $abs_event['topics']);
            ob_start();
            include ( $templatePath);
            $html = ob_get_contents();
            ob_end_clean();
            break;
        case 'EmailTemplate':
            $template = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."aaostracts_emailtemplates WHERE ID = ".$id);
            ob_start();
            include ( $templatePath);
            $html = ob_get_contents();
            ob_end_clean();
            break;
        default :
            ob_start();
            include ( $templatePath);
            $html = ob_get_contents();
            ob_end_clean();
    }
    echo $html;
}

function aaostracts_redirect($tab){ ?>
    <script type="text/javascript">
        window.location = '<?php echo $tab; ?>';
    </script>
<?php
}

function aaostracts_loadUserGuide(){
    $templatePath = AAOSTRACTS_PLUGIN_DIR . 'aaostracts_userguide.php';
    ob_start();
    include ( $templatePath);
    $html = ob_get_contents();
    ob_end_clean();
    return $html;
}

function aaostracts_showMessage($message, $alert_class){
    echo "<br><div class='aaostracts containter-fluid'>"
                . "<div class='aaostracts alert ".$alert_class."' role='alert'>"
                    . "<strong>$message</strong>"
                . "</div>"
            . "</div>";
}

function aaostracts_showAlert($message, $alert_type){ ?>
    <script type="text/javascript">
        var type = '<?php echo $alert_type;?>';
        var message = '<?php echo $message;?>';
        switch(type){
            'success':
                    alertify.success(message);
            break;
            'error':
                    alertify.error(message);
            break;

        }
    </script>
<?php
}

function aaostracts_is_event_active($event_id){
    $event = aaostracts_getEvents('event_id', $event_id, "ARRAY_A");
    $current_date = current_time('Y-m-d');
    $deadline = date_format(date_create_from_format('Y-m-d', $event['deadline']), 'Y-m-d');
    return strtotime($deadline) >= strtotime($current_date);
}

function aaostracts_getDashboard(){
    $args = array('post_type' => 'page', 'post_status' => 'publish');
    $pages = get_pages($args);

    foreach($pages as $page){
        if( has_shortcode( $page->post_content, 'aaostracts' ) ) {
            $dashboard_id = $page->ID;
        }
    }
    return get_permalink($dashboard_id);
}

function aaostracts_get_dashboard_id(){
    $args = array('post_type' => 'page', 'post_status' => 'publish');
    $pages = get_pages($args);

    foreach($pages as $page){
        if( has_shortcode( $page->post_content, 'aaostracts' ) ) {
            $dashboard_id = $page->ID;
        }
    }
    return ($dashboard_id) ? $dashboard_id : 0;
}

function aaostracts_get_abstract_user($aid){
    global $wpdb;
    $wpdb->show_errors();
    $sql = $wpdb->prepare("SELECT submit_by FROM ".$wpdb->prefix."aaostracts_abstracts WHERE abstract_id=%d", $aid);
    $abstract = $wpdb->get_row($sql);
    $user = get_user_by( 'id', $abstract->submit_by );
    return $user;
}

function aaostracts_get_login(){
    include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    _e('Please login for your conference participation', 'aaostracts');
    wp_login_form();
    if(get_option('users_can_register')) {
        $link_html = "<a href=" . wp_registration_url() .">" . apply_filters('aaostracts_title_filter', __('Create an Account','aaostracts'), 'create_account') . "</a>";
        echo apply_filters('aaostracts_title_filter', __('Need an account? ','aaostracts'), 'need_account');
        echo apply_filters('aaostracts_register_link', $link_html);
    }
}
