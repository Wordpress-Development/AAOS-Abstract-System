<?php defined('ABSPATH') or die("ERROR: You do not have permission to access this page");
global $wpdb;
if(!class_exists('AAOSTRACTS')){
    require_once( AAOSTRACTS_PLUGIN_DIR . 'inc/aaostracts_functions.php' );
}

$abstracts_submitted_count = $wpdb->get_var("SELECT COUNT(*) FROM ".$wpdb->prefix."aaostracts_abstracts");

$abstracts_approved_count = $wpdb->get_var("SELECT COUNT(*) FROM ".$wpdb->prefix."aaostracts_abstracts WHERE status = 'Approved' OR status = 'Accepted'");

$abstracts_pending_count = $wpdb->get_var("SELECT COUNT(*) FROM ".$wpdb->prefix."aaostracts_abstracts WHERE status = 'Pending'");

$abstracts_rejected_count = $wpdb->get_var("SELECT COUNT(*) FROM ".$wpdb->prefix."aaostracts_abstracts WHERE status = 'Rejected'");

$reviews_submitted_count = $wpdb->get_var("SELECT COUNT(*) FROM ".$wpdb->prefix."aaostracts_reviews");

$reviews_excellent_count = $wpdb->get_var("SELECT COUNT(*) FROM ".$wpdb->prefix."aaostracts_reviews WHERE relevance = 'Excellent'");

$reviews_good_count = $wpdb->get_var("SELECT COUNT(*) FROM ".$wpdb->prefix."aaostracts_reviews WHERE relevance = 'Good'");

$reviews_average_count = $wpdb->get_var("SELECT COUNT(*) FROM ".$wpdb->prefix."aaostracts_reviews WHERE relevance = 'Average'");

$reviews_poor_count = $wpdb->get_var("SELECT COUNT(*) FROM ".$wpdb->prefix."aaostracts_reviews WHERE relevance = 'Poor'");
?>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawAbstractsReport);
  google.setOnLoadCallback(drawReviewsReport);
  google.setOnLoadCallback(drawEventReport);

  function drawAbstractsReport() {

   var submitted =  <?php echo($abstracts_submitted_count); ?>;
   var approved =  <?php echo($abstracts_approved_count); ?>;
   var pending =  <?php echo($abstracts_pending_count); ?>;
   var rejected =  <?php echo($abstracts_rejected_count); ?>;

    var data = google.visualization.arrayToDataTable([
      ['<?php _e('Status', 'aaostracts'); ?>', '', { role: 'style' }],
      ['<?php _e('Submitted', 'aaostracts'); ?>',  submitted, 'color: #CCCCCC'],
      ['<?php _e('Approved', 'aaostracts'); ?>',  approved, 'color: #CCCCCC'],
      ['<?php _e('Pending', 'aaostracts'); ?>',  pending, 'color: #CCCCCC'],
      ['<?php _e('Rejected', 'aaostracts'); ?>',  rejected, 'color: #CCCCCC']
    ]);

    var options = {
      title: '<?php _e('ABSTRACTS BY STATUS', 'aaostracts'); ?>',
      hAxis: {title: '<?php _e('STATUS', 'aaostracts'); ?>', titleTextStyle: {color: 'black'}},
      vAxis: {title: '<?php _e('SUBMITTED', 'aaostracts'); ?>', titleTextStyle: {color: 'black'}, maxValue:'<?php echo($abstracts_submitted_count); ?>', format: '#' } };

    var chart = new google.visualization.ColumnChart(document.getElementById('aaostracts_status_report'));
    chart.draw(data, options);
  }
   function drawReviewsReport() {

    var excellent =  <?php echo($reviews_excellent_count); ?>;
    var good =  <?php echo($reviews_good_count); ?>;
    var average =  <?php echo($reviews_average_count); ?>;
    var poor =  <?php echo($reviews_poor_count); ?>;

    var data = google.visualization.arrayToDataTable([
      ['<?php _e('Status', 'aaostracts'); ?>', '', { role: 'style' }],
      ['<?php _e('Excellent', 'aaostracts'); ?>',  excellent, 'color: #CCCCCC'],
      ['<?php _e('Good', 'aaostracts'); ?>',  good, 'color: #CCCCCC'],
      ['<?php _e('Average', 'aaostracts'); ?>',  average, 'color: #CCCCCC'],
      ['<?php _e('Poor', 'aaostracts'); ?>',  poor, 'color: #CCCCCC']
    ]);

    var options = {
      title: '<?php _e('REVIEWS BY RELEVANCE', 'aaostracts'); ?>',
      hAxis: {title: '<?php _e('RELEVANCE', 'aaostracts'); ?>', titleTextStyle: {color: 'black'}},
      vAxis: {title: '<?php _e('SUBMITTED', 'aaostracts'); ?>', titleTextStyle: {color: 'black'}, maxValue:'<?php echo($reviews_submitted_count); ?>', format:'#'}};

    var chart = new google.visualization.ColumnChart(document.getElementById('aaostracts_review_report'));
    chart.draw(data, options);
  }
</script>
<br>
<div class="aaostracts container-fluid aaostracts-admin-container">

    <div class="aaostracts row">

        <div class="aaostracts col-xs-12 col-md-9">
            <div class="aaostracts panel panel-primary">
                <div class="aaostracts panel panel-heading">
                    <h4><?php _e('Abstracts by Status', 'aaostracts');?></h4>
                </div>
                <div class="aaostracts panel panel-body">
                    <div id="aaostracts_status_report"></div>
                </div>
            </div>

             <div class="aaostracts panel panel-primary">
                <div class="aaostracts panel panel-heading">
                    <h4><?php _e('Reviews by Relevance', 'aaostracts');?></h4>
                </div>
                <div class="aaostracts panel panel-body">
                    <div id="aaostracts_review_report"></div>
                </div>
            </div>
        </div>

        <div class="aaostracts col-xs-12 col-md-3">

            <div class="aaostracts panel panel-primary">
                <div class="aaostracts panel panel-heading">
                    <h4><?php _e('Abstracts - Export by status', 'aaostracts');?></h4>
                </div>
                <div class="aaostracts panel panel-body">
                    <p class="export_reports"><?php _e('Approved', 'aaostracts');?> (<?php echo $abstracts_approved_count; ?>)<a href="?page=aaostracts&tab=reports&task=download&type=abstracts&id=1"><?php _e('Export CSV', 'aaostracts');?></a></p>
                    <p class="export_reports"><?php _e('Pending', 'aaostracts');?> (<?php echo $abstracts_pending_count; ?>)<a href="?page=aaostracts&tab=reports&task=download&type=abstracts&id=2"><?php _e('Export CSV', 'aaostracts');?></a></p>
                    <p class="export_reports"><?php _e('Rejected', 'aaostracts');?> (<?php echo $abstracts_rejected_count; ?>)<a href="?page=aaostracts&tab=reports&task=download&type=abstracts&id=3"><?php _e('Export CSV', 'aaostracts');?></a></p>
                    <p class="export_reports"><?php _e('All Abstracts', 'aaostracts');?> (<?php echo $abstracts_submitted_count; ?>)<a href="?page=aaostracts&tab=reports&task=download&type=abstracts&id=4"><?php _e('Export CSV', 'aaostracts');?></a></p>
                </div>
            </div>

            <div class="aaostracts panel panel-primary">
                <div class="aaostracts panel panel-heading">
                    <h4><?php _e('Reviews - Export by relevance', 'aaostracts');?></h4>
                </div>
                <div class="aaostracts panel panel-body">
                    <p class="export_reports"> <?php _e('Excellent', 'aaostracts');?> (<?php echo $reviews_excellent_count; ?>)<a href="?page=aaostracts&tab=reports&task=download&type=reviews&id=1"><?php _e('Export CSV', 'aaostracts');?></a></p>
                    <p class="export_reports"> <?php _e('Good', 'aaostracts');?> (<?php echo $reviews_good_count; ?>)<a href="?page=aaostracts&tab=reports&task=download&type=reviews&id=2"><?php _e('Export CSV', 'aaostracts');?></a></p>
                    <p class="export_reports"> <?php _e('Average', 'aaostracts');?> (<?php echo $reviews_average_count; ?>)<a href="?page=aaostracts&tab=reports&task=download&type=reviews&id=3"><?php _e('Export CSV', 'aaostracts');?></a></p>
                    <p class="export_reports"> <?php _e('Poor', 'aaostracts');?> (<?php echo $reviews_poor_count; ?>)<a href="?page=aaostracts&tab=reports&task=download&type=reviews&id=4"><?php _e('Export CSV', 'aaostracts');?></a></p>
                    <p class="export_reports"> <?php _e('All Reviews', 'aaostracts');?> (<?php echo $reviews_submitted_count; ?>)<a href="?page=aaostracts&tab=reports&task=download&type=reviews&id=5"><?php _e('Export CSV', 'aaostracts');?></a></p>
                </div>
            </div>
        </div>
    </div>
</div>