<script type="text/javascript">
    jQuery(function() {
        jQuery("#abs_event_start").datepicker({ dateFormat: "yy-mm-dd" });
        jQuery("#abs_event_end").datepicker({ dateFormat: "yy-mm-dd" });
        jQuery("#abs_event_deadline").datepicker({ dateFormat: "yy-mm-dd" });
    });
</script>
<div class="aaostracts container-fluid">
        <form method="post" enctype="multipart/form-data" id="abs_event_form">
            <div class="aaostracts row">

            <div class="aaostracts col-xs-12 col-sm-12 col-md-8">
                <div class="aaostracts panel panel-default">
                    <div class="aaostracts panel-heading">
                        <h4><?php echo apply_filters('aaostracts_title_filter', __('Edit Event','aaostracts'), 'edit_event');?></h4>
                    </div>
                    <div class="aaostracts panel-body">
                        <input class="aaostracts form-control" type="text" name="abs_event_name" id="abs_event_name" placeholder="<?php _e('Event title','aaostracts');?>" value="<?php echo esc_attr($abs_event['name']);?>" />
                        <br>
                        <?php wp_editor(stripslashes($abs_event['description']), 'abs_event_desc', true, true); ?>
                    </div>
                 </div>
            </div>

            <div class="aaostracts col-xs-12 col-md-4">
                <div class="aaostracts panel panel-default">
                    <div class="aaostracts panel-heading">
                        <h4><?php echo apply_filters('aaostracts_title_filter', __('Event Information','aaostracts'), 'event_information');?></h4>
                    </div>
                    <div class="aaostracts panel-body">
                        <div class="aaostracts form-group">
                            <label class="aaostracts control-label" for="abs_event_host"><?php _e('Host','aaostracts');?></label>
                            <input class="aaostracts form-control" type="text" name="abs_event_host" id="abs_event_host" value="<?php echo stripslashes($abs_event['host']);?>" />

                            <label class="aaostracts control-label" for="abs_event_address"><?php _e('Location','aaostracts');?></label>
                            <input class="aaostracts form-control" type="text" name="abs_event_address" id="abs_event_address" value="<?php echo stripslashes($abs_event['address']); ?>" />

                            <label class="aaostracts control-label" for="abs_event_start"><?php _e('Start Date','aaostracts');?></label>
                            <input class="aaostracts form-control input-group date" type="text" name="abs_event_start" id="abs_event_start" value="<?php echo stripslashes($abs_event['start_date']); ?>" />

                            <label class="aaostracts control-label" for="abs_event_end"><?php _e('End Date','aaostracts');?></label>
                            <input class="aaostracts form-control" type="text" name="abs_event_end" id="abs_event_end" value="<?php echo stripslashes($abs_event['end_date']); ?>"/>

                            <label class="aaostracts control-label" for="abs_event_deadline"><?php _e('Deadline','aaostracts');?></label>
                            <input class="aaostracts form-control" type="text" name="abs_event_deadline" id="abs_event_deadline" value="<?php echo stripslashes($abs_event['deadline']); ?>"/>
                        </div>
                    </div>
                </div>
                <div class="aaostracts panel panel-default">
                    <div class="aaostracts panel-heading">
                        <h4><?php _e('Topics','aaostracts');?></h4>
                    </div>
                    <div class="aaostracts panel-body">
                        <div class="aaostracts form-group" id="topics_table">
                            <?php
                                foreach($topics as $key => $topic){ ?>
                                <p><input class="aaostracts form-control" type="text" name="topics[]" id="topics[]" value="<?php echo $topic;?>" /></p>
                            <?php } ?>
                        </div>
                        <div class="inner_btns">
                            <a class="button-secondary" href="#add-topic" onclick="aaostracts_add_topic();" style="float: left;"><?php echo apply_filters('aaostracts_title_filter', __('add topic','aaostracts'), 'add_topic');?></a>
                            <a class="button-secondary" href="#delete-topic" onclick="aaostracts_delete_topic();" style="float: right;"><?php echo apply_filters('aaostracts_title_filter', __('delete topic','aaostracts'), 'delete_topic');?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button type="button" onclick="aaostracts_validateEvent();" class="aaostracts btn btn-primary"><?php echo apply_filters('aaostracts_title_filter', __('Update Event','aaostracts'), 'update_event');?></button>
        </form>
    </div>
