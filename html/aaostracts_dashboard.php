<?php
defined('ABSPATH') or die("ERROR: You do not have permission to access this page");
include_once(apply_filters('aaostracts_page_include', AAOSTRACTS_PLUGIN_DIR . 'aaostracts_abstracts.php'));
include_once(apply_filters('aaostracts_page_include', AAOSTRACTS_PLUGIN_DIR . 'aaostracts_reviews.php'));

if(is_user_logged_in()){
    $user = wp_get_current_user();
    if($user->roles[0] == 'administrator'){
        _e("You're an Administrator. Please use the WordPress admin area to manage abstracts.", 'aaostracts');
        return;
    }else if($user->roles[0] == 'subscriber' || $user->roles[0] == 'editor'){
        aaostracts_dashboard_header($user);
        $task = isset($_GET["task"]) ? sanitize_text_field($_GET["task"]) : '';
        $id = isset($_GET["id"]) ? intval($_GET["id"]) : 0;

        switch($user->roles[0]){
            case 'subscriber':
                if( $task == "new_abstract" ){
                    aaostracts_addAbstract($event_id);
                }
                else if ($task =="edit_abstract" ){
                    aaostracts_editAbstract($id);
                }
                else if ( $task == "delete_abstract" ){
                    aaostracts_deleteAbstract($id, true );
                    aaostracts_show_author_dashboard($user);
                }else{
                    aaostracts_show_author_dashboard($user);
                }
                break;
            case 'editor':
                if( $task == "new_abstract" ){
                    aaostracts_addAbstract($event_id);
                }
                else if ($task == "edit_abstract" ){
                    aaostracts_editAbstract($id);
                }
                else if ( $task == "delete_abstract" ){
                    aaostracts_deleteAbstract($id, true );
                    aaostracts_show_reviewer_dashboard($user);
                }
                else if ($task == "new_review"){
                    aaostracts_addReview($id);
                }
                else if ($task == "edit_review"){
                    aaostracts_editReview($id);
                }
                else if ($task == "delete_review"){
                    aaostracts_deleteReview($id, true);
                    aaostracts_show_reviewer_dashboard($user);
                }else{
                    aaostracts_show_reviewer_dashboard($user);
                }
                break;
            default:
                _e("You do not have permission to view this page. Please contact the site admin.", 'aaostracts');
                break;
            }
    }else{
        _e('You do not have permission to view this page.', 'aaostracts');
    }
}else{
    aaostracts_get_login();
}

function aaostracts_dashboard_header($user) { ?>
    <p class="aaostracts text-right"><strong><?php _e('Welcome back','aaostracts');?> <?php echo $user->display_name; ?></strong></p>
    <div class="aaostracts">
        <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#aaostracts-dash-menu">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="?dashboard"><span class="aaostracts glyphicon glyphicon-home" aria-hidden="true"></span> <?php echo apply_filters('aaostracts_title_filter', __('Dashboard','aaostracts'), 'dashboard');?></a>
            </div>
            <div class="collapse navbar-collapse" id="aaostracts-dash-menu">
                <ul class="nav navbar-nav">
                <?php if ($user->roles[0] == 'subscriber') { ?>
                      <li>
                          <a href="?task=new_abstract"><span class="aaostracts glyphicon glyphicon-plus" aria-hidden="true"></span> <?php echo apply_filters('aaostracts_title_filter', __('New Abstract','aaostracts'), 'new_abstract');?></a>
                      </li>
                <?php } else if ($user->roles[0] == 'editor' && get_option('aaostracts_reviewer_submit')){ ?>
                      <li>
                          <a href="?task=new_abstract"><span class="aaostracts glyphicon glyphicon-plus" aria-hidden="true"></span> <?php echo apply_filters('aaostracts_title_filter', __('New Abstract','aaostracts'), 'new_abstract');?></a>
                      </li>
                <?php } ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php ob_start();?>
                    <li>
                        <a href="<?php echo wp_logout_url(home_url()); ?>">
                            <span class="aaostracts glyphicon glyphicon-off" aria-hidden="true"></span> <?php echo apply_filters('aaostracts_title_filter', __('Logout','aaostracts'), 'logout');?>
                        </a>
                    </li>
                    <?php $right_links = ob_get_clean();?>
                    <?php echo apply_filters('aaostracts_dashboard_rightmenu', $right_links); ?>
                </ul>
            </div>
        </div>
    </nav>
    </div>
    <?php
}

function aaostracts_show_author_dashboard($user) {

    $abstracts = aaostracts_getAbstracts('user_id', $user->ID, 'NULL'); ?>
        <div class="aaostracts container-fluid">
            <div class="aaostracts panel panel-default">
                <div class="aaostracts panel-heading">
                    <h6 class="aaostracts panel-title"><?php echo apply_filters('aaostracts_title_filter', __('My Abstracts','aaostracts'), 'my_abstracts');?></h6>
                </div>

                <div class="aaostracts panel-body">
                    <div class="aaostracts table-responsive">
                        <table class="aaostracts table table-hover table-striped">
                        <thead>
                            <tr>
                                <th><?php _e('ID','aaostracts');?></th>
                                <th><?php _e('Title','aaostracts');?></th>
                                <?php if(get_option('aaostracts_show_reviews')){ ?>
                                <th><?php _e('Review','aaostracts');?></th>
                                <?php } ?>
                                <th><?php _e('Status','aaostracts');?></th>
                                <th><?php _e('Type','aaostracts');?></th>
                                <th><?php _e('Submitted','aaostracts');?></th>
                                <th style="text-align: center;"><img src="<?php echo plugins_url( '../images/attachment_icon.png' , __FILE__ ); ?>" title="Attachments"></th>
                                <th><?php _e('Action','aaostracts');?></th>
                            </tr>
                        </thead>
                        <?php
                            if (!count($abstracts)) { ?>
                                <tbody>
                                    <tr>
                                        <td colspan="8"><?php _e("You have NOT submitted any abstracts.", 'aaostracts'); ?><td>
                                    </tr>
                                </tbody>
                        <?php } else { ?>
                                <tbody>
                                <?php
                                foreach($abstracts as $abstract){ ?>
                                    <?php $reviews = aaostracts_getReviews('abstract_id', $abstract->abstract_id); ?>
                                    <?php $attachments = aaostracts_getAttachments('abstracts_id', $abstract->abstract_id); ?>
                                    <tr>
                                        <td><?php echo $abstract->abstract_id; ?></td>
                                        <td><?php echo $abstract->title; ?></td>
                                        <?php if(get_option('aaostracts_show_reviews')){ ?>
                                        <td><?php
                                            if(!count($reviews)){
                                                 _e("No reviews as yet.", 'aaostracts');
                                            }else{
                                                foreach($reviews as $review){
                                                    echo '<p>' . $review->comments . '</p>';
                                                }
                                            }?>
                                        </td>
                                         <?php } ?>
                                        <td><?php _e($abstract->status, 'aaostracts'); ?></td>
                                        <td><?php echo $abstract->presenter_preference; ?></td>
                                        <td><?php echo date_i18n(get_option('date_format') . ' ' . get_option('time_format'), strtotime($abstract->submit_date)); ?></td>
                                        <td style="text-align: center;"><?php echo count($attachments); ?></td>
                                        <?php
                                            if ($abstract->status == "Pending") { ?>
                                                <td><a href="?task=edit_abstract&id=<?php echo $abstract->abstract_id; ?>"><?php _e('Edit','aaostracts');?></a>
                                                    | <a href='javascript:aaostracts_delete_abstract(<?php echo $abstract->abstract_id; ?>)'><?php _e('Delete','aaostracts');?></a></td>
                                        <?php
                                            }else{ ?>
                                            <td><?php _e('Closed','aaostracts');?></td>
                                        <?php } ?>
                                    </tr>
                                <?php
                                } ?>
                                </tbody>
                           <?php } ?>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    <?php
}

function aaostracts_show_reviewer_dashboard($user) {
    global $wpdb;
    ob_start();
    if(get_option('aaostracts_reviewer_submit')){
        aaostracts_show_author_dashboard($user);
    }
    ?>
    <div class="aaostracts container-fluid">
        <div class="aaostracts panel panel-default">
            <div class="aaostracts panel-heading">
                <h6 class="aaostracts panel-title"><?php echo apply_filters('aaostracts_title_filter', __('Assigned Abstracts','aaostracts'), 'assigned_abstracts');?></h6>
            </div>
            <div class="aaostracts panel-body">
                <div class="aaostracts table-responsive">
                    <table class="aaostracts table table-hover">
            <thead>
             <tr>
                 <th><?php _e('ID','aaostracts');?></th>
                     <th><?php _e('Title','aaostracts');?></th>
                     <?php if(!get_option('aaostracts_blind_review')){ ?>
                        <th><?php _e('Author','aaostracts');?></th>
                     <?php } ?>
                     <th><?php _e('Type Request','aaostracts');?></th>
                     <th><?php _e('Date Submitted','aaostracts');?></th>
                     <th style="text-align: center;"><img src="<?php echo plugins_url( '../images/attachment_icon.png' , __FILE__ ); ?>" title="Attachments"></th>
                     <th><?php _e('Action', 'aaostracts'); ?></th>
                 </tr>
            </thead>
         <?php
         $abstracts = array();
                 for($i = 1; $i <= 3; $i++){ // lazy approach, will fix later
                    $sql = $wpdb->prepare("SELECT abstract_id, title, author, presenter_preference, submit_date
                                            FROM ".$wpdb->prefix."aaostracts_abstracts AS abstracts
                                            WHERE abstract_id NOT IN
                                                (SELECT abstract_id
                                                 FROM ".$wpdb->prefix."aaostracts_reviews AS reviews
                                                 WHERE abstracts.abstract_id = reviews.abstract_id
                                                 AND reviews.user_id = $user->ID)
                                            AND reviewer_id".$i. " = %d", $user->ID);
                    $temp = $wpdb->get_results($sql, ARRAY_A);
                    $abstracts = array_merge($abstracts, $temp);
                 }
         if(!($abstracts)){ ?>
            <tbody>
                <tr>
                    <td colspan="7"><?php _e('You have NO newly assigned abstracts','aaostracts');?><td>
                </tr>
            </tbody>
        <?php
        }
        else{
            foreach($abstracts as $abstract){
                $attachments = aaostracts_getAttachments('abstracts_id', $abstract['abstract_id']); ?>
                <tbody>
                <tr>
                    <td><?php echo $abstract['abstract_id']; ?></td>
                    <td><?php echo $abstract['title']; ?></td>
                    <?php if(!get_option('aaostracts_blind_review')){ ?>
                        <td><?php echo $abstract['author']; ?></td>
                     <?php } ?>

                    <td><?php echo $abstract['presenter_preference']; ?></td>
                    <td><?php echo date('m-d-y g:i a', strtotime($abstract['submit_date'])); ?></td>
                    <td style="text-align: center;"><?php echo count($attachments); ?></td>
                    <td>
                        <?php if (get_option('aaostracts_reviewer_edit')) { ?>
                        <a href="?task=edit_abstract&id=<?php echo $abstract['abstract_id']; ?>"><?php _e('Edit','aaostracts');?></a> |
                        <?php } ?>
                        <a href="?task=new_review&id=<?php echo $abstract['abstract_id']; ?>"><?php _e('Add Review','aaostracts');?></a>
                    </td>
                </tr>
           <?php
            }
        }
        ?>
            </tbody>
        </table>
                </div>
            </div>
        </div>

        <div class="aaostracts panel panel-default">
            <div class="aaostracts panel-heading">
                <h6 class="aaostracts panel-title"><?php echo apply_filters('aaostracts_title_filter', __('My Reviews','aaostracts'), 'my_reviews');?></h6>
            </div>
            <div class="aaostracts panel-body">
                <div class="aaostracts table-responsive">
                    <table class="aaostracts table table-hover">
            <thead>
             <tr>
                 <th><?php _e('ID','aaostracts');?></th>
                     <th><?php _e('Title','aaostracts');?></th>
                     <th><?php _e('Comments','aaostracts');?></th>
                     <th><?php _e('Status','aaostracts');?></th>
                     <th><?php _e('Relevance','aaostracts');?></th>
                     <th><?php _e('Quality','aaostracts');?></th>
                     <th><?php _e('Type','aaostracts');?></th>
                     <th><?php _e('Reviewed','aaostracts');?></th>
                     <th><?php _e('Action','aaostracts');?></th>
                 </tr>
            </thead>
         <?php
         $reviews = aaostracts_getReviews('user_id', $user->ID);
         if(!($reviews)){ ?>
            <tbody>
                <tr>
                    <td colspan="9"><?php _e('You have no COMPLETED reviews','aaostracts');?></td>
                </tr>
            </tbody>
        <?php
        }
        else{
            foreach($reviews as $review){
                $abstract_title = $wpdb->get_row("SELECT title FROM ".$wpdb->prefix."aaostracts_abstracts WHERE abstract_id = $review->abstract_id"); ?>
            <tbody>
            <tr>
                <td><?php echo $review->review_id; ?></td>
                <td><?php echo $abstract_title->title; ?></td>
                <td><?php echo $review->comments; ?></td>
                <td><?php echo $review->status; ?></td>
                <td><?php echo $review->relevance; ?></td>
                <td><?php echo $review->quality; ?></td>
                <td><?php echo $review->recommendation; ?></td>
                <td><?php echo date_i18n(get_option('date_format') . ' ' . get_option('time_format'), strtotime($review->review_date)); ?></td>
                <td>
                    <a href="?task=edit_review&id=<?php echo $review->review_id; ?>"><?php _e('Edit','aaostracts');?></a> |
                    <a href="javascript:aaostracts_delete_review(<?php echo $review->review_id; ?>)"><?php _e('Delete','aaostracts');?></a>
                </td>
            </tr>
        <?php
            }
        } ?>
            </tbody>
        </table>
                </div>
            </div>
        </div>
    </div>
  <?php
  $html = ob_get_clean();
  echo apply_filters('aaostracts_reviewer_dashboard', $html, $user);
}

