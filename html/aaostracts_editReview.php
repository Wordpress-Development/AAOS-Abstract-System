<div class="aaostracts container-fluid">
    <h3><?php echo apply_filters('aaostracts_title_filter', __('Edit Review','aaostracts'), 'edit_review'); ?></h3>
        <form method="post" enctype="multipart/form-data" id="wpabs_review_form">
            <div class="aaostracts row">
            <div class="aaostracts col-xs-12 col-sm-12 col-md-8">
                <div class="aaostracts panel panel-default">
                    <div class="aaostracts panel-heading">
                        <strong><?php echo esc_attr( htmlspecialchars($abstract['title'])); ?></strong>
                        <input type="hidden" name="abs_id" value="<?php echo $abstract['abstract_id']; ?>" />
                        <input type="hidden" name="abs_title" value="<?php echo $abstract['title']; ?>" />
                    </div>
                    <div class="aaostracts panel-body">
                        <?php echo wpautop(stripslashes($abstract['text'])); ?>
                    </div>
                 </div>
                <?php
                    if (get_option('aaostracts_show_author') && !get_option('aaostracts_blind_review')) { ?>
                        <div class="aaostracts panel panel-default">
                            <div class="aaostracts panel-heading">
                                <strong><?php echo apply_filters('aaostracts_title_filter', __('Author Information','aaostracts'), 'author_information'); ?></strong>
                            </div>
                            <div class="aaostracts panel-body">
                                <?php
                                    $authors_name = explode(" | ", $abstract['author']);
                                    $authors_emails = explode(",", $abstract['author_email']);
                                    $authors_affiliation = explode(",", $abstract['author_affiliation']);
                                    foreach ($authors_name as $id => $key) {
                                        $authors[$key] = array(
                                            'name'  => $authors_name[$id],
                                            'email' => $authors_emails[$id],
                                            'affiliation'    => $authors_affiliation[$id],
                                        );
                                    }
                                    foreach($authors as $author){ ?>
                                        <?php _e('Name','aaostracts');?>: <?php echo esc_attr($author['name']);?><br>
                                        <?php _e('Email', 'aaostracts'); ?>: <?php echo esc_attr($author['email']); ?><br>
                                        <<?php _e('Affiliation', 'aaostracts'); ?>: <?php echo esc_attr($author['affiliation']); ?><br>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="aaostracts panel panel-default">
                            <div class="aaostracts panel-heading">
                                <strong><?php echo apply_filters('aaostracts_title_filter', __('Presenter Information','aaostracts'), 'presenter_information'); ?></strong>
                            </div>
                            <div class="aaostracts panel-body">
                                    <?php _e('Name','aaostracts');?>: <?php echo esc_attr($abstract['presenter']);?><br>
                                    <?php _e('Email', 'aaostracts'); ?>: <?php echo esc_attr($abstract['presenter_email']); ?><br>
                                    <?php _e('Preference', 'aaostracts'); ?>: <?php echo esc_attr($abstract['presenter_preference']); ?><br>
                            </div>
                        </div>
                <?php } ?>



                <div class="aaostracts panel panel-default">
                    <div class="aaostracts panel-heading">
                        <strong><?php echo apply_filters('aaostracts_title_filter', __('Attachments','aaostracts'), 'attachments'); ?></strong>
                    </div>
                    <div class="aaostracts panel-body">
                        <?php
                            $attachments = aaostracts_getAttachments('abstracts_id', $abstract['abstract_id']);
                            if (count($attachments) < 1) { ?>
                                <p><?php _e('No Attachments submitted', 'aaostracts'); ?></p>
                            <?php }
                            else{
                                foreach($attachments as $attachment) { ?>
                                    <a href="?task=download&type=attachment&id=<?php echo $attachment->attachment_id; ?>">
                                    <?php echo $attachment->filename ?>
                                    <span class="aaostracts glyphicon glyphicon-download-alt" aria-hidden="true"></span></a><br>
                            <?php } ?>

                        <?php } ?>
                    </div>

                </div>


                <div class="aaostracts panel panel-default">
                    <div class="aaostracts panel-heading">
                         <strong><?php _e('Add Comments', 'aaostracts');?></strong>
                    </div>
                    <div class="aaostracts panel-body" id="abs_review_comments_error">
                        <?php
                            $settings = array( 'media_buttons' => false, 'wpautop'=>true, 'dfw' => true, 'editor_height' => 90, 'quicktags' => false);
                            wp_editor(stripslashes($review['comments']), 'abs_comments', $settings);
                        ?>

                    </div>

                </div>

            </div>

              <div class="aaostracts col-xs-12 col-md-4">
                <div class="aaostracts panel panel-default">
                    <div class="aaostracts panel-heading">
                         <strong><?php _e('Relevance','aaostracts');?></strong>
                    </div>
                    <div class="aaostracts panel-body" id="abs_relevance_error">
                        <div class="aaostracts radio">
                            <label class="aaostracts radio">
                                <input type='radio' name='abs_relevance' value='Excellent' <?php checked($review['relevance'], __('Excellent', 'aaostracts')); ?> /> <?php _e('Excellent', 'aaostracts'); ?>
                            </label>
                            <label class="aaostracts radio">
                                <input type='radio' name='abs_relevance'  value='Good' <?php checked($review['relevance'], "Good"); ?> /> <?php _e('Good', 'aaostracts'); ?>
                            </label>
                            <label class="aaostracts radio">
                                <input type='radio' name='abs_relevance' value='Average' <?php checked($review['relevance'], "Average"); ?> /> <?php _e('Average', 'aaostracts'); ?>
                            </label>
                            <label class="aaostracts radio">
                                <input type='radio' name='abs_relevance' value='Poor' <?php checked($review['relevance'], "Poor" ); ?> /> <?php _e('Poor', 'aaostracts'); ?>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="aaostracts panel panel-default">
                    <div class="aaostracts panel-heading">
                         <strong><?php _e('Quality','aaostracts');?></strong>
                    </div>
                    <div class="aaostracts panel-body" id="abs_quality_error">
                        <div class="aaostracts radio">
                            <label class="aaostracts radio">
                                <input type='radio' name='abs_quality' value='Excellent' <?php checked($review['quality'], "Excellent"); ?> /> <?php _e('Excellent', 'aaostracts'); ?>
                            </label>
                            <label class="aaostracts radio">
                                <input type='radio' name='abs_quality' value='Good' <?php checked($review['quality'], "Good"); ?> /> <?php _e('Good', 'aaostracts'); ?>
                            </label>
                            <label class="aaostracts radio">
                                <input type='radio' name='abs_quality' value='Average' <?php checked($review['quality'],"Average"); ?> /> <?php _e('Average', 'aaostracts'); ?>
                            </label>
                            <label class="aaostracts radio">
                                <input type='radio' name='abs_quality' value='Poor' <?php checked($review['quality'], "Poor"); ?> /> <?php _e('Poor', 'aaostracts'); ?>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="aaostracts panel panel-default">
                    <div class="aaostracts panel-heading">
                         <strong><?php _e('Suggest Type','aaostracts');?></strong>
                    </div>
                    <div class="aaostracts panel-body" id="abs_recommendation_error">
                        <div class="aaostracts radio">
                            <span style="color:#999;">(<?php _e('Request', 'aaostracts');?>: <?php echo $abstract['presenter_preference']; ?>)</span><br>
                            <?php
                                $presenter_preference = explode(',', get_option('aaostracts_presenter_preference'));
                                foreach($presenter_preference as $preference){ ?>
                            <label class="aaostracts radio"><input type='radio' name='abs_recommendation' value='<?php echo $preference; ?>' <?php checked($review['recommendation'], $preference); ?> /> <?php echo $preference; ?></label>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div class="aaostracts panel panel-default">
                    <div class="aaostracts panel-heading">
                         <strong><?php _e('Suggest Status','aaostracts');?></strong>
                    </div>
                    <div class="aaostracts panel-body" id="abs_status_error">
                        <div class="aaostracts radio">
                            <label class="aaostracts radio"><input type='radio' name='abs_status' value='Pending' <?php checked($review['status'] , "Pending")  ?> /> <?php _e('Pending', 'aaostracts');?></label>
                            <label class="aaostracts radio"><input type='radio' name='abs_status' value='Accepted' <?php checked($review['status'], "Accepted"); ?> /> <?php _e('Accepted', 'aaostracts');?></label>
                                <label class="aaostracts radio"><input type='radio' name='abs_status' value='Rejected' <?php checked($review['status'], "Rejected"); ?> /> <?php _e('Rejected', 'aaostracts');?></label>
                        </div>
                    </div>
                </div>

                <div class="aaostracts panel panel-default">
                    <div class="aaostracts panel-heading">
                         <strong><?php _e('Additional Information','aaostracts');?></strong>
                    </div>
                    <div class="aaostracts panel-body">
                        <div class="aaostracts form-group">
                            <strong><?php _e('Submitted', 'aaostracts'); ?>:  </strong><?php echo date_i18n(get_option('date_format') . ' ' . get_option('time_format'), strtotime($abstract['submit_date'])); ?><br>
                            <strong><?php _e('Event', 'aaostracts'); ?>: </strong><?php echo $event->name; ?><br>
                            <strong><?php _e('Topic', 'aaostracts'); ?>: </strong><?php echo $abstract['topic']; ?>
                        </div>
                    </div>
                </div>
        </div>
        </div>
            <button type="button" onclick="aaostracts_validateReview();" class="aaostracts btn btn-primary "><?php _e('Update Review', 'aaostracts'); ?></button>
        </form>

    </div>