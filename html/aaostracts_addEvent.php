<script type="text/javascript">
    jQuery(function() {
        jQuery( "#abs_event_start" ).datepicker({ dateFormat: "yy-mm-dd" });
        jQuery( "#abs_event_end" ).datepicker({ dateFormat: "yy-mm-dd" });
        jQuery( "#abs_event_deadline" ).datepicker({ dateFormat: "yy-mm-dd" });
    });
</script>
<div class="aaostracts container-fluid">
    <h4><?php echo apply_filters('aaostracts_title_filter', __('New Event','aaostracts'), 'new_event');?></h4>
        <form method="post" enctype="multipart/form-data" id="abs_event_form">
            <div class="aaostracts row">

            <div class="aaostracts col-xs-12 col-sm-12 col-md-8">
                <div class="aaostracts panel panel-default">
                    <div class="aaostracts panel-heading">
                        <h5><?php echo apply_filters('aaostracts_title_filter', __('Event Information','aaostracts'), 'event_information');?></h5>
                    </div>
                    <div class="aaostracts panel-body">
                        <input class="aaostracts form-control" type="text" name="abs_event_name" id="abs_event_name" placeholder="<?php echo apply_filters('aaostracts_title_filter', __('Event Title','aaostracts'), 'event_title');?>" />
                        <br>
                        <?php wp_editor('', 'abs_event_desc', true, true); ?>
                    </div>
                 </div>
            </div>

            <div class="aaostracts col-xs-12 col-md-4">
                <div class="aaostracts panel panel-default">
                    <div class="aaostracts panel-heading">
                        <h5><?php echo apply_filters('aaostracts_title_filter', __('Event Details','aaostracts'), 'event_details');?></h5>
                    </div>
                    <div class="aaostracts panel-body">
                        <div class="aaostracts form-group">
                            <label class="aaostracts control-label" for="abs_event_host"><?php _e('Host','aaostracts');?></label>
                            <input class="aaostracts form-control" type="text" name="abs_event_host" id="abs_event_host" />

                            <label class="aaostracts control-label" for="abs_event_address"><?php _e('Location','aaostracts');?></label>
                            <input class="aaostracts form-control" type="text" name="abs_event_address" id="abs_event_address" />

                            <label class="aaostracts control-label" for="abs_event_start"><?php _e('Start Date','aaostracts');?></label>
                            <input class="aaostracts form-control" type="text" name="abs_event_start" id="abs_event_start" />

                            <label class="aaostracts control-label" for="abs_event_end"><?php _e('End Date','aaostracts');?></label>
                            <input class="aaostracts form-control" type="text" name="abs_event_end" id="abs_event_end" />

                            <label class="aaostracts control-label" for="abs_event_deadline"><?php _e('Deadline','aaostracts');?></label>
                            <input class="aaostracts form-control" type="text" name="abs_event_deadline" id="abs_event_deadline" />
                        </div>
                    </div>
                </div>
                <div class="aaostracts panel panel-default">
                    <div class="aaostracts panel-heading">
                        <h5><?php echo apply_filters('aaostracts_title_filter', __('Topics','aaostracts'), 'topics');?></h5>
                    </div>
                    <div class="aaostracts panel-body">
                        <div class="aaostracts form-group" id="topics_table">
                            <p><input class="aaostracts form-control" type="text" name="topics[]" id="topics[]" value="" /></p>
                        </div>
                        <div class="inner_btns">
                            <a class="button-secondary" href="#add-topic" onclick="aaostracts_add_topic();" style="float: left;"><?php echo apply_filters('aaostracts_title_filter', __('add topic','aaostracts'), 'add_topic');?></a>
                            <a class="button-secondary" href="#delete-topic" onclick="aaostracts_delete_topic();" style="float: right;"><?php echo apply_filters('aaostracts_title_filter', __('delete topic','aaostracts'), 'delete_topic');?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button type="button" onclick="aaostracts_validateEvent();" class="aaostracts btn btn-primary"><?php echo apply_filters('aaostracts_title_filter', __('Save Event','aaostracts'), 'save_event');?></button>
        </form>
    </div>