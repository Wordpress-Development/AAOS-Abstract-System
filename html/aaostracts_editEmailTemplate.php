<div class="metabox-holder has-right-sidebar">
    <form method="post" enctype="multipart/form-data" id="emailtemplate">
        <div class="inner-sidebar">
            <div class="postbox">
                <h3><?php _e('Available Shortcodes','aaostracts'); ?></h3>
                <div class="inside">
                    <p>Display Name: {DISPLAY_NAME}</p>
                    <p>Username: {USERNAME}</p>
                    <p>Abstract Title: {ABSTRACT_TITLE}</p>
                    <p>Abstract ID: {ABSTRACT_ID}</p>
                    <p>Event Name: {EVENT_NAME}</p>
                    <p>Event Start Date: {EVENT_START}</p>
                    <p>Event End Date: {EVENT_END}</p>
                    <p>Presenter Pref: {PRESENTER_PREF}</p>
                    <p>Site Name: {SITE_NAME}</p>
                    <p>Site URL: {SITE_URL}</p>
                    <p>One Week Later: {ONE_WEEK_LATER}</p>
                    <p>Two Weeks Later: {TWO_WEEKS_LATER}</p>
                </div>
            </div>
        </div>
        <div id="post-body">
            <div id="post-body-content">
                <div class="postbox">
                    <h3><?php _e('Edit Email Template', 'aaostracts');?></h3>
                    <div class="inside">
                        <table class="widefat" style="border: none;">
                        <tr>
                            <td><?php _e('Template Name','aaostracts');?></td>
                            <td><input type="text" name="template_name" id="template_name" value="<?php echo esc_html($template->name);?>" class="regular-text"></td>
                        </tr>
                        <tr>
                            <td><?php _e('From Name','aaostracts');?></td>
                            <td><input type="text" name="from_name" id="from_name" value="<?php echo esc_html($template->from_name);?>" class="regular-text"></td>
                        </tr>
                        <tr>
                            <td><?php _e('From Email','aaostracts');?></td>
                            <td><input type="text" name="from_email" id="from_email" value="<?php echo esc_html($template->from_email);?>" class="regular-text"></td>
                        </tr>
                        <tr>
                            <td><?php _e('Email Subject','aaostracts');?></td>
                            <td><input type="text" name="email_subject" id="email_subject" value="<?php echo esc_html($template->subject);?>" class="regular-text"></td>
                        </tr>
                        <tr>
                            <td><?php _e('Email Body','aaostracts');?></td>
                            <td>
                                <?php
                                $text_settings = array( 'media_buttons' => false, 'wpautop'=>true, 'dfw' => true, 'editor_height' => 240);
                                wp_editor(stripslashes($template->message), 'email_body', $text_settings);
                            ?>
                            </td>
                        </tr>
                    </table>
                    </div>
                </div>
                 <div class="misc-pub-section">
                    <input type="button" onclick="aaostracts_validateTemplate();" class="button button-primary button-large btn btn-primary" value="<?php _e('Update Template','aaostracts');?>" />
                </div>
            </div>
        </div>
    </form>
</div>


