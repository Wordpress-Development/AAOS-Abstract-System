<script>
    jQuery(function (){
        aaostracts_updateWordCount();
    });
</script>
<div class="aaostracts container-fluid">
    <h3><?php echo apply_filters('aaostracts_title_filter', __('Edit Abstract','aaostracts'), 'edit_abstract');?></h3>
    <form method="post" enctype="multipart/form-data" id="abs_form">
        <div class="aaostracts row">
            <div class="aaostracts col-xs-12 col-sm-12 col-md-8">
                <div class="aaostracts panel panel-default">
                    <div class="aaostracts panel-heading">
                        <h5>
                            <?php echo apply_filters('aaostracts_title_filter', __('Abstract Information','aaostracts'), 'abstract_information');?>
                        </h5>
                    </div>
                    <div class="aaostracts panel-body">
                        <div class="aaostracts form-group">
                            <input class="aaostracts form-control" type="text" name="abs_title" placeholder="<?php _e('Enter title','aaostracts');?>" value="<?php echo esc_attr( htmlspecialchars( $abstract[0]->title ) ); ?>" id="title" />
                        </div>

                        <div class="aaostracts form-group">
                            <?php
                                $settings = array( 'media_buttons' => false, 'wpautop'=>true, 'dfw' => true, 'editor_height' => 360, 'quicktags' => false);
                                $content = $abstract[0]->text;
                                $editor_id = 'abstext';
                                wp_editor( stripslashes($content), $editor_id, $settings );
                            ?>
                            <span class="aaostracts max-word-count" style="display: none;"><?php echo get_option('aaostracts_chars_count'); ?></span>
                            <table id="post-status-info" cellspacing="0">
                                <tbody><tr><td id="wp-word-count"><?php printf( __( 'Word count: %s' ), '<span class="aaostracts abs-word-count">0</span>' ); ?></td></tr></tbody>
                            </table>
                        </div>

                        <?php if(get_option('aaostracts_show_keywords')){ ?>
                        <div class="aaostracts form-group">
                            <input class="aaostracts form-control" type="text" name="abs_keywords" id="abs_keywords" placeholder="<?php echo apply_filters('aaostracts_title_filter', __('Enter comma separated keywords','aaostracts'), 'enter_keywords');?>" value="<?php echo esc_attr( htmlspecialchars( $abstract[0]->keywords ) ); ?>" />
                        </div>
                        <?php } ?>
                    </div>
                 </div>

                <div class="aaostracts panel panel-default">
                    <div class="aaostracts panel-heading" id="manage_attachments">
                        <h5><?php echo apply_filters('aaostracts_title_filter', __('Manage Attachments','aaostracts'), 'manage_attachments');?></h5>
                    </div>
                    <div class="aaostracts panel-body">
                        <?php
                        if(count($attachments) < 1) {
                              _e('No Attachments uploaded', 'aaostracts');
                        }
                        else{
                            foreach($attachments as $attachment) { ?>
                                    <p><span id="attachment_<?php echo $attachment->attachment_id;?>"><strong><?php echo $attachment->filename; ?></strong> [<?php echo number_format(($attachment->filesize/1048576), 2);?>MB]
                                    <a class="aaostracts btn btn-danger" href="#removeAttachment" onclick="aaostracts_remove_attachment('<?php echo $attachment->attachment_id; ?>');">Remove</a></span></p>
                                <?php
                            }
                        } ?>
                    </div>

                </div>
                <?php if(get_option('aaostracts_change_ownership')){?>
                <?php
                    $currentUser = wp_get_current_user();
                    if($currentUser->roles[0]=='administrator' || $currentUser->roles[0]=='editor'){ ?>

                    <div class="aaostracts panel panel-default">
                        <div class="aaostracts panel-heading">
                            <h5><?php echo apply_filters('aaostracts_title_filter', __('Manage Ownership','aaostracts'), 'manage_ownership');?></h5>
                        </div>
                        <div class="aaostracts panel-body">
                            <p><?php _e('Use this box to assign a new user / owner of this submission','aaostracts');?></p>
                                <?php
                                    $users = get_users('role=subscriber');
                                    $current_user = get_userdata($abstract[0]->submit_by);
                                ?>
                                <select class="aaostracts form-control" name="abs_user" id="abs_user">
                                    <option value="<?php echo esc_attr($current_user->ID);?>" style="display:none;"><?php echo esc_attr($current_user->display_name);?></option>
                                  <?php foreach($users as $user){ ?>
                                    <option value="<?php echo esc_attr($user->ID);?>"><?php echo esc_attr($user->display_name);?></option>
                                  <?php } ?>
                                </select>
                        </div>

                    </div>
                <?php }

                } ?>
                <button type="button" onclick="aaostracts_validateAbstract();" class="aaostracts btn btn-primary"><?php _e('Submit','aaostracts');?></button>
            </div>

            <div class="aaostracts col-xs-12 col-md-4">

                <div class="aaostracts panel panel-default">
                    <div class="aaostracts panel-heading">
                        <h5><?php echo apply_filters('aaostracts_title_filter', __('Event Information','aaostracts'), 'event_information');?></h5>
                    </div>

                    <div class="aaostracts panel-body">

                        <div class="aaostracts form-group">
                            <label class="aaostracts control-label" for="abs_event"><?php echo apply_filters('aaostracts_title_filter', __('Event','aaostracts'), 'event');?></label>
                            <?php if(is_admin()){ ?>
                                <select name="abs_event" id="abs_event" class="aaostracts form-control" onchange="aaostracts_getTopics(this.value);">
                                    <option value="<?php echo esc_html($event['event_id']);?>" style="display:none;"><?php echo esc_attr($event['name']);?></option>
                                    <?php foreach($events as $event){ ?>
                                        <option value="<?php echo esc_attr($event->event_id);?>"><?php echo esc_attr($event->name);?></option>
                                    <?php } ?>
                                </select>
                            <?php } else { ?>
                                <div class="aaostracts form-group">
                                    <?php echo esc_attr($event['name']);?>
                                    <input type="hidden" name="abs_event" value="<?php echo esc_attr($event['event_id']);?>">
                                </div>
                            <?php } ?>
                                <div class="aaostracts form-group">
                                    <label class="aaostracts control-label" for="abs_topic"><?php echo apply_filters('aaostracts_title_filter', __('Topic','aaostracts'), 'topic');?></label>
                                    <select name="abs_topic" id="abs_topic" class="aaostracts form-control">
                                        <option value="<?php echo esc_attr($abstract[0]->topic) ;?>" style="display:none;"><?php echo esc_attr($abstract[0]->topic) ;?></option>
                                        <?php foreach($topics as $topic){ ?>
                                        <option value="<?php echo esc_attr($topic);?>"><?php echo esc_attr($topic);?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                        </div>
                    </div>
                </div>

                <?php if(get_option('aaostracts_show_author')){?>
                    <div class="aaostracts panel panel-default">

                    <div class="aaostracts panel-heading">
                        <h5>
                            <?php echo apply_filters('aaostracts_title_filter', __('Author Information','aaostracts'), 'author_information');?>
                            <span class="aaostracts glyphicon glyphicon-minus-sign delete_author" onclick="aaostracts_delete_coauthor();"></span>
                            <span class="aaostracts glyphicon glyphicon-plus-sign add_author" onclick="aaostracts_add_coauthor();"></span>
                        </h5>
                    </div>

                    <div class="aaostracts panel-body" id="coauthors_table">

                    <?php
                        $authors_name = explode(' | ', $abstract[0]->author);
                        $authors_emails = explode(' | ', $abstract[0]->author_email);
                        $authors_affiliation = explode(' | ', $abstract[0]->author_affiliation);

                        foreach($authors_name as $id => $author){ ?>

                            <div class="aaostracts form-group author_box">
                                <label class="aaostracts control-label" for="abs_author[]"><?php _e('Name','aaostracts');?></label>
                                <input class="aaostracts form-control" type="text" name="abs_author[]" id="abs_author[]" value="<?php echo esc_attr($authors_name[$id]); ?>"/>

                                <label class="aaostracts control-label" for="abs_author_email[]"><?php _e('Email','aaostracts');?></label>
                                <input class="aaostracts form-control" type="text" name="abs_author_email[]" id="abs_author_email[]" value="<?php echo esc_attr($authors_emails[$id]); ?>" />

                                <label class="aaostracts control-label" for="abs_author_affiliation[]"><?php _e('Affiliation','aaostracts');?></label>
                                <input class="aaostracts form-control" type="text" name="abs_author_affiliation[]" id="abs_author_affiliation[]" value="<?php echo esc_attr($authors_affiliation[$id]); ?>" />
                            </div>
                        <?php } ?>
                    </div>

                </div>
                <?php } ?>

                <?php if(get_option('aaostracts_show_presenter')){ ?>
                    <div class="aaostracts panel panel-default">

                    <div class="aaostracts panel-heading">
                        <h5><?php echo apply_filters('aaostracts_title_filter', __('Presenter Information','aaostracts'), 'presenter_information');?></h5>
                    </div>

                    <div class="aaostracts panel-body">

                        <div class="aaostracts form-group">
                            <label class="aaostracts control-label" for="abs_presenter"><?php _e('Name','aaostracts');?></label>
                            <input class="aaostracts form-control" type="text" name="abs_presenter" id="abs_presenter" value="<?php echo esc_attr($abstract[0]->presenter);?>"/>
                        </div>

                        <div class="aaostracts form-group">
                            <label class="aaostracts control-label" for="abs_presenter_email"><?php _e('Email','aaostracts');?></label>
                            <input class="aaostracts form-control" type="text" name="abs_presenter_email" id="abs_presenter_email" value="<?php echo esc_attr($abstract[0]->presenter_email);?>"/>
                        </div>

                        <div class="aaostracts form-group">
                            <label class="aaostracts control-label" for="abs_presenter_preference"><?php _e('Presenter Preference','aaostracts');?></label>
                            <select class="aaostracts form-control" name="abs_presenter_preference" id="abs_presenter_preference" >
                                <option value="<?php echo $abstract[0]->presenter_preference;?>" selected style="display:none;"><?php echo esc_attr($abstract[0]->presenter_preference);?></option>
                                <?php
                                    $presenter_preference = explode(',', get_option('aaostracts_presenter_preference'));
                                    foreach($presenter_preference as $preference){ ?>
                                        <option value="<?php echo $preference; ?>"><?php echo $preference; ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    </div>
                </div>
                <?php } ?>

                <?php if(get_option('aaostracts_show_attachments')){ ?>
                    <div class="aaostracts panel panel-default">

                        <div class="aaostracts panel-heading">
                            <h5><?php echo apply_filters('aaostracts_title_filter', __('Attachments','aaostracts'), 'attachments');?></h5>
                        </div>

                        <div class="aaostracts panel-body">
                            <div class="aaostracts form-group">
                            <?php $attachments_remaining = get_option('aaostracts_upload_limit') - count($attachments); ?>
                            <?php if($attachments_remaining > 0) { ?>
                                <?php _e('Use this form to upload your images, photos or tables.', 'aaostracts'); ?>
                                <?php _e('Supported formats', 'aaostracts'); ?>: <strong><?php echo implode(' ', explode(' ', get_option('aaostracts_permitted_attachments'))); ?></strong>
                                <?php _e('Maximum attachment size', 'aaostracts'); ?>: <strong><?php echo number_format((get_option('aaostracts_max_attach_size') / 1048576)); ?>MB</strong>
                            <?php } else{
                                _e('You have used all your attachment slots allowed.', 'aaostracts');
                            }?>
                            </div>

                            <div class="aaostracts form-group">
                                <?php
                                    for($i = 0; $i < $attachments_remaining; $i++){ ?>
                                        <div>
                                            <input type="file" name="attachments[]">
                                        </div>
                                <?php } ?>
                            </div>

                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </form>
</div>