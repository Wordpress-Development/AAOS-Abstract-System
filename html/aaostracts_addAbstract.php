<script>
    jQuery(document).ready(function (){
        aaostracts_updateWordCount();
    });
</script>
<?php
    if($id && !aaostracts_is_event_active($id)){
        aaostracts_showMessage(__('Abstract submission for this event has past', 'aaostracts'), 'alert-danger');
        return;
    }
    $event = (!is_admin()) ? aaostracts_getEvents('event_id', $id, 'ARRAY_A') : 0;
?>
<div class="aaostracts container-fluid">
    <h3><?php echo apply_filters('aaostracts_title_filter', __('New Abstract','aaostracts'), 'new_abstract'); ?></h3>
    <form method="post" enctype="multipart/form-data" id="abs_form">
        <div class="aaostracts row">
            <div class="aaostracts col-xs-12 col-sm-12 col-md-8">
                <div class="aaostracts panel panel-default">
                    <div class="aaostracts panel-heading">
                        <h5>
                            <?php echo apply_filters('aaostracts_title_filter', __('Abstract Information','aaostracts'), 'abstract_information');?>
                        </h5>
                    </div>
                    <div class="aaostracts panel-body">
                        <div class="aaostracts form-group">
                            <input class="aaostracts form-control" type="text" name="abs_title" placeholder="<?php echo apply_filters('aaostracts_title_filter', __('Enter Title','aaostracts'), 'enter_title');?>" value="" id="title" />
                        </div>
                        <div class="aaostracts form-group">
                            <?php $settings = array( 'media_buttons' => false, 'wpautop'=>true, 'dfw' => true, 'editor_height' => 360, 'quicktags' => false); ?>
                            <?php wp_editor(stripslashes(get_option('aaostracts_author_instructions')), 'abstext', $settings); ?>
                            <span class="aaostracts max-word-count" style="display: none;"><?php echo get_option('aaostracts_chars_count'); ?></span>
                            <table id="post-status-info" cellspacing="0">
                                <tbody><tr><td id="wp-word-count"><?php printf( __( 'Word count: %s' ), '<span class="aaostracts abs-word-count">0</span>' ); ?></td></tr></tbody>
                            </table>
                        </div>
                        <?php if(get_option('aaostracts_show_keywords')){ ?>
                        <div class="aaostracts form-group">
                            <input class="aaostracts form-control" type="text" name="abs_keywords" id="abs_keywords" placeholder="<?php echo apply_filters('aaostracts_title_filter', __('Enter comma separated keywords','aaostracts'), 'enter_keywords');?>" value="" />
                        </div>
                        <?php } ?>
                    </div>
                 </div>
                <?php if(get_option('aaostracts_show_attachments')){ ?>
                    <div class="aaostracts panel panel-default">

                        <div class="aaostracts panel-heading">
                           <h5><?php echo apply_filters('aaostracts_title_filter', __('Attachments','aaostracts'), 'attachments');?></h5>
                        </div>

                        <div class="aaostracts panel-body">

                            <dl class="aaostracts form-group">
                                <dd><?php _e('Use this form to upload your images, photos or tables.', 'aaostracts'); ?></dd>
                                <dd><?php _e('Supported formats', 'aaostracts'); ?>: <strong><?php echo implode(' ', explode(' ', get_option('aaostracts_permitted_attachments'))); ?></strong></dd>
                                <dd><?php _e('Maximum attachment size', 'aaostracts'); ?>: <strong><?php echo number_format((get_option('aaostracts_max_attach_size') / 1048576)); ?>MB</strong></dd>
                            </dl>
                            <dl class="aaostracts form-group">
                                <?php
                                    for($i = 0; $i < get_option('aaostracts_upload_limit'); $i++){ ?>
                                        <dd>
                                            <input type="file" name="attachments[]">
                                        </dd>
                                <?php } ?>
                            </dl>
                        </div>
                    </div>
                <?php } ?>

                <?php if(get_option('aaostracts_show_conditions')){ ?>
                <div class="aaostracts panel panel-default">

                    <div class="aaostracts panel-heading">
                        <h5>
                            <?php echo apply_filters('aaostracts_title_filter', __('Terms and Conditions','aaostracts'), 'terms_conditions');?>
                            <input type="checkbox" name="abs_terms"  id="abs_terms"/>
                        </h5>
                    </div>

                    <div class="aaostracts panel-body">
                        <div class="aaostracts terms_conditons">
                            <?php echo stripslashes(get_option('aaostracts_terms_conditions'));?>
                        </div>
                    </div>
                </div>
            <?php } ?>
                <button type="button" onclick="aaostracts_validateAbstract();" class="aaostracts btn btn-primary"><?php _e('Submit','aaostracts');?></button>
            </div>
            <div class="aaostracts col-xs-12 col-md-4">
                <div class="aaostracts panel panel-default">

                    <div class="aaostracts panel-heading">
                        <h5><?php echo apply_filters('aaostracts_title_filter', __('Event Information','aaostracts'), 'event_information');?></h5>
                    </div>

                    <div class="aaostracts panel-body">

                        <div class="aaostracts form-group">
                            <label class="aaostracts control-label" for="abs_event"><?php echo apply_filters('aaostracts_title_filter', __('Event','aaostracts'), 'event');?></label>
                            <?php if(is_admin()){ ?>
                            <select name="abs_event" id="abs_event" class="aaostracts form-control" onchange="aaostracts_getTopics(this.value);">
                                    <option value="" style="display:none;"><?php echo apply_filters('aaostracts_title_filter', __('Select an Event','aaostracts'), 'select_event');?></option>
                                    <?php foreach($events as $event){ ?>
                                        <option value="<?php echo esc_attr($event->event_id);?>"><?php echo esc_attr($event->name);?></option>
                                    <?php } ?>
                            </select>
                            <div class="aaostracts form-group">
                                <label class="aaostracts control-label" for="abs_topic"><?php echo apply_filters('aaostracts_title_filter', __('Topic','aaostracts'), 'topic');?></label>
                                <select name="abs_topic" id="abs_topic" class="aaostracts form-control">
                                    <option value="" style="display:none;"><?php echo apply_filters('aaostracts_title_filter', __('Select a Topic','aaostracts'), 'select_topic');?></option>
                                </select>
                            </div>
                            <?php } else { ?>
                                <div class="aaostracts form-group">
                                    <?php echo esc_attr($event['name']);?>
                                    <input type="hidden" name="abs_event" value="<?php echo esc_attr($event['event_id']);?>">
                                </div>
                                <div class="aaostracts form-group">
                                    <label class="aaostracts control-label" for="abs_topic"><?php echo apply_filters('aaostracts_title_filter', __('Topic','aaostracts'), 'topic');?></label>
                                    <select name="abs_topic" id="abs_topic" class="aaostracts form-control">
                                        <option value="" style="display:none;"><?php echo apply_filters('aaostracts_title_filter', __('Select a Topic','aaostracts'), 'select_topic');?></option>
                                        <?php $topics = explode(',', $event['topics']); ?>
                                        <?php foreach($topics as $topic){ ?>
                                        <option value="<?php echo esc_attr($topic);?>"><?php echo esc_attr($topic);?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php if(get_option('aaostracts_show_author')){?>
                    <div class="aaostracts panel panel-default">

                        <div class="aaostracts panel-heading">
                            <h5>
                                <?php echo apply_filters('aaostracts_title_filter', __('Author Information','aaostracts'), 'author_information');?>
                                <span class="aaostracts glyphicon glyphicon-minus-sign delete_author" onclick="aaostracts_delete_coauthor();"></span>
                                <span class="aaostracts glyphicon glyphicon-plus-sign add_author" onclick="aaostracts_add_coauthor();"></span>
                            </h5>
                        </div>

                        <div class="aaostracts panel-body" id="coauthors_table">

                            <div class="aaostracts form-group author_box">
                                <label class="aaostracts control-label" for="abs_author[]"><?php _e('Name','aaostracts');?></label>
                                <input class="aaostracts form-control" type="text" name="abs_author[]" id="abs_author[]"  />

                                <label class="aaostracts control-label" for="abs_author_email[]"><?php _e('Email','aaostracts');?></label>
                                <input class="aaostracts form-control" type="text" name="abs_author_email[]" id="abs_author_email[]" />

                                <label class="aaostracts control-label" for="abs_author_affiliation[]"><?php _e('Affiliation','aaostracts');?></label>
                                <input class="aaostracts form-control" type="text" name="abs_author_affiliation[]" id="abs_author_affiliation[]"/>

                            </div>
                        </div>

                    </div>
                <?php } ?>

                <?php if(get_option('aaostracts_show_presenter')){ ?>
                <div class="aaostracts panel panel-default">

                    <div class="aaostracts panel-heading">
                        <h5><?php echo apply_filters('aaostracts_title_filter', __('Presenter Information','aaostracts'), 'presenter_information');?></h5>
                    </div>

                    <div class="aaostracts panel-body">

                        <div class="aaostracts form-group">
                            <label class="aaostracts control-label" for="abs_presenter"><?php _e('Name','aaostracts');?></label>
                            <input class="aaostracts form-control" type="text" name="abs_presenter" id="abs_presenter"/>
                        </div>

                        <div class="aaostracts form-group">
                            <label class="aaostracts control-label" for="abs_presenter_email"><?php _e('Email','aaostracts');?></label>
                            <input class="aaostracts form-control" type="text" name="abs_presenter_email" id="abs_presenter_email"/>
                        </div>

                        <div class="aaostracts form-group">
                            <label class="aaostracts control-label" for="abs_presenter_preference"><?php echo apply_filters('aaostracts_title_filter', __('Presenter Preference','aaostracts'), 'presenter_preference');?></label>
                            <select class="aaostracts form-control" name="abs_presenter_preference" id="abs_presenter_preference">
                                <option value="" style="display:none;"><?php _e('Preference','aaostracts');?></option>
                                <?php
                                    $presenter_preference = explode(',', get_option('aaostracts_presenter_preference'));
                                    foreach($presenter_preference as $preference){ ?>
                                        <option value="<?php echo $preference; ?>"><?php echo $preference; ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    </div>
                </div>
            <?php } ?>
            </div>
        </div>
    </form>
</div>

