<?php
/**
 * New User HTML Template
 */
?>
<div class="aaostracts container-fluid">

    <div class="aaostracts alert alert-warning" role="alert">
        <h5><span class="glyphicon glyphicon-info-sign"></span> We could not find a profile for you. Please take a few minutes to create one now.</h5>
    </div>
    <h4><span class="aaostracts glyphicon glyphicon-plus"></span> Create Profile</h4>
    <form method="post" enctype="multipart/form-data" id="wpa_account_form">
        <div class="aaostracts panel panel-default">
            <div class="aaostracts panel-heading">
              <h3 class="aaostracts panel-title">General Information</h3>
            </div>
            <div class="aaostracts panel panel-body">
                <div class="aaostracts form-group col-xs-4 required">
                    <label class="aaostracts control-label" for="first_name">First Name</label>
                    <input type="text" name="first_name" class="aaostracts form-control" id="first_name">
                </div>
                <div class="aaostracts form-group col-xs-4 required">
                    <label class="aaostracts control-label" for="last_name">Surname</label>
                    <input type="text" name="last_name" class="aaostracts form-control" id="last_name">
                </div>
                <div class="aaostracts form-group col-xs-4 required">
                    <label class="aaostracts control-label" for="reg_type">Registration Type</label>
                    <select name="reg_type" class="aaostracts form-control" id="reg_type">
                          <option value="">Select an option</option>
                          <option value="Professional">Professional</option>
                          <option value="Professional 2 Days">Professional 2 Days</option>
                          <option value="Student">Student</option>
                          <option value="Retired">Retired</option>
                          <option value="Accompanying Person">Accompanying Person</option>
                      </select>
                </div>
                <div class="aaostracts form-group col-xs-3">
                    <label class="aaostracts control-label" for="title">Title</label>
                    <select name="title" class="aaostracts form-control" id="title">
                        <option value="">Select an option</option>
                        <option value="Prof">Prof</option>
                        <option value="Dr">Dr</option>
                        <option value="Mr">Mr</option>
                        <option value="Mrs">Mrs</option>
                    </select>
                </div>
                <div class="aaostracts form-group col-xs-3">
                    <label class="aaostracts control-label" for="gender">Gender</label>
                    <select name="gender" class="aaostracts form-control" id="gender">
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                    </select>
                </div>
                <div class="aaostracts panel panel-body">
                <div class="aaostracts form-group col-xs-3">
                    <label class="aaostracts control-label" for="telephone">Phone number</label>
                    <input type="text" name="telephone" class="aaostracts form-control" id="telephone" placeholder="Ex. 0039123456789">
                </div>
                <div class="aaostracts form-group col-xs-3">
                    <label class="aaostracts control-label" for="alt_email">Alternative email</label>
                    <input type="text" name="alt_email" class="aaostracts form-control" id="alt_email">
                </div>
            </div>
            </div>
        </div>
        <div class="aaostracts panel panel-default">
            <div class="aaostracts panel-heading">
              <h3 class="aaostracts panel-title">Organization</h3>
            </div>
            <div class="aaostracts panel panel-body">
                <div class="aaostracts form-group col-xs-6">
                    <label class="aaostracts control-label" for="organization">Institution / Company</label>
                    <input type="text" name="organization" class="aaostracts form-control" id="organization">
                </div>
                <div class="aaostracts form-group col-xs-6 required">
                    <label class="aaostracts control-label" for="country">Country</label>
                    <input type="text" name="country" class="aaostracts form-control" id="country">
                </div>
            </div>
        </div>

        <div class="aaostracts panel panel-default">
            <div class="aaostracts panel-heading">
              <h3 class="aaostracts panel-title">Conference Information</h3>
            </div>
            <div class="aaostracts panel panel-body">
                <div class="aaostracts form-group col-xs-3 required">
                    <label class="aaostracts control-label" for="oral_presenter">Are you an oral presenter?</label>
                    <select name="oral_presenter" class="aaostracts form-control" id="oral_presenter">
                          <option value="">Select an option</option>
                          <option value="Yes">Yes</option>
                          <option value="No">No</option>
                          <option value="Maybe">Maybe</option>
                      </select>
                </div>
                <div class="aaostracts form-group col-xs-3">
                    <label class="aaostracts control-label" for="oral_code">If YES, what is the abstract code?</label>
                    <input type="text" name="oral_code" class="aaostracts form-control" id="oral_code" placeholder="Ex.: HE01_234">
                </div>
                <div class="aaostracts form-group col-xs-3 required">
                    <label class="aaostracts control-label" for="poster_presenter">Are you an Poster presenter?</label>
                    <select name="poster_presenter" class="aaostracts form-control" id="poster_presenter">
                          <option value="">Select an option</option>
                          <option value="Yes">Yes</option>
                          <option value="No">No</option>
                          <option value="Maybe">Maybe</option>
                      </select>
                </div>
                <div class="aaostracts form-group col-xs-3">
                    <label class="aaostracts control-label" for="poster_code">If YES, what is the abstract code?</label>
                    <input type="text" name="poster_code" class="aaostracts form-control" id="poster_code" placeholder="Ex.: HE01_234">
                </div>
                <div class="aaostracts form-group col-xs-4 required">
                    <label class="aaostracts control-label" for="badge_name">Name on your badge</label>
                    <input type="text" name="badge_name" class="aaostracts form-control" id="badge_name">
                </div>
                <div class="aaostracts form-group col-xs-4 required">
                    <label class="aaostracts control-label" for="badge_org">Organization on your badge</label>
                    <input type="text" name="badge_org" class="aaostracts form-control" id="badge_org">
                </div>
                <div class="aaostracts form-group col-xs-4 required">
                    <label class="aaostracts control-label" for="printed_abstracts">Printed copy of book of abstracts</label>
                    <select name="printed_abstracts" class="aaostracts form-control" id="printed_abstracts">
                        <option value="">Select an option</option>
                          <option value="Yes">Yes</option>
                          <option value="No">No</option>
                      </select>
                </div>
            </div>
        </div>

        <div class="aaostracts panel panel-default">
            <div class="aaostracts panel-heading">
              <h3 class="aaostracts panel-title">Other Information</h3>
            </div>
            <div class="aaostracts panel panel-body">
                <div class="aaostracts form-group col-xs-3">
                    <label class="aaostracts control-label" for="hotel_internal">My hotel in Nova Yardinia</label>
                    <select name="hotel_internal" class="aaostracts form-control" id="hotel_internal">
                          <option value="">Select an option</option>
                          <option value="Kalidria Hotel">Kalidria Hotel</option>
                          <option value="Alborea Hotel">Alborea Hotel</option>
                          <option value="Calene Hotel">Calene Hotel</option>
                          <option value="Valentino Hotel">Valentino Hotel</option>
                      </select>
                </div>
                <div class="aaostracts form-group col-xs-3">
                    <label class="aaostracts control-label" for="hotel_external">My Hotel outside Nova Yardinia</label>
                    <input type="text" name="hotel_external" class="aaostracts form-control" id="hotel_external">
                </div>
                <div class="aaostracts form-group col-xs-3">
                    <label class="aaostracts control-label" for="flight_arrival">My flight arrives Sunday at [hh:mm]</label>
                    <input type="text" name="flight_arrival" class="aaostracts form-control" id="flight_arrival">
                </div>
                <div class="aaostracts form-group col-xs-3">
                    <label class="aaostracts control-label" for="flight_departure">My flight leaves Saturday at [hh:mm]</label>
                    <input type="text" name="flight_departure" class="aaostracts form-control" id="flight_departure">
                </div>
                <div class="aaostracts form-group col-xs-12">
                    <label class="aaostracts control-label" for="dietary_restrictions">Dietary Restrictions</label>
                    <textarea name="dietary_restrictions" class="aaostracts form-control" id="dietary_restrictions"></textarea>
                </div>
            </div>

        </div>

<!--        <div class="aaostracts panel panel-default">
            <div class="aaostracts panel-heading">
              <h3 class="aaostracts panel-title">Contacts</h3>
            </div>
            <div class="aaostracts panel panel-body">
                <div class="aaostracts form-group col-xs-6">
                    <label class="aaostracts control-label" for="telephone">Phone number</label>
                    <input type="text" name="telephone" class="aaostracts form-control" id="telephone" placeholder="Ex. 0039123456789">
                </div>
                <div class="aaostracts form-group col-xs-6">
                    <label class="aaostracts control-label" for="alt_email">Alternative email</label>
                    <input type="text" name="alt_email" class="aaostracts form-control" id="alt_email">
                </div>
            </div>
        </div>-->
         <div class="aaostracts panel panel-default">
            <div class="aaostracts panel-heading">
              <h3 class="aaostracts panel-title">Terms of Participation</h3>
            </div>
            <div class="aaostracts panel panel-body">
                <div class="aaostracts form-group required">
                    <span class="aaostracts help-block">
                        Conferences organized by the Associazione Italiana per lo Studio delle Argille (AISA), a no-profit scientific society, can be attended by its members only. Therefore, to participate in the DUST 2016, you must be a member of AISA and the payment of the conference fee is intended as a declaration that you are asking to be member of AISA. On the basis of the Italian laws, we have to register you in our Book of Members for the 2016. This doesn???t imply any commitment from your part and all the information stored in our systems will be deleted after the conference.
                    </span>
                    <label class="aaostracts control-label" for="terms_conditions">I agree</label>
                    <input type="checkbox" name="terms_conditions" id="terms_conditions">
                </div>
            </div>
        </div>


        <button type="button" onclick="aaostracts_validateUser();" class="btn btn-primary btn-lg">Save Profile</button>

    </form>
</div>
