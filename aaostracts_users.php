<?php
defined('ABSPATH') or die("ERROR: You do not have permission to access this page");

if(!class_exists('AAOStracts_Users')){
    require_once( AAOSTRACTS_PLUGIN_DIR . 'inc/aaostracts_classes.php' );
}

if(is_admin() && isset($_GET['tab']) && ($_GET["tab"]=="users")){
    if(isset($_GET['task'])){
        $task = sanitize_text_field($_GET['task']);
        $id = intval($_GET['id']);
        switch($task){
            case 'delete':
                aaostracts_deleteUser($id);
            default :
                aaostracts_showUsers();
                break;
        }
    }else{
        aaostracts_showUsers();
    }
}

function aaostracts_showUsers(){ ?>
        <form id="showUsers" name="user_list" method="get">
            <input type="hidden" name="page" value="aaostracts" />
            <input type="hidden" name="tab" value="users" />
            <?php
            $showUsers = new AAOStracts_Users();
            $showUsers->prepare_items();
            $showUsers->display(); ?>
            </form>
    <?php
}



function aaostracts_deleteUser($id){
    if(wp_delete_user($id)){
        echo '<div id="message" class="updated fade"><p><strong>Successfully deleted User ID ' . $id . '!</strong></p></div>';
    }
}
