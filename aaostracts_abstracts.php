<?php
defined('ABSPATH') or die("ERROR: You do not have permission to access this page");

if(!class_exists('AAOStract_Abstracts_Table')){
    require_once( AAOSTRACTS_PLUGIN_DIR . 'inc/aaostracts_classes.php' );
}
if(!class_exists('AAOStracts_Emailer')){
    require_once( AAOSTRACTS_PLUGIN_DIR . 'inc/aaostracts_emailer.php' );
}
if(is_admin() && isset($_GET['tab']) && $_GET["tab"]=="abstracts"){
    if(isset($_GET["task"])){
        $task = sanitize_text_field($_GET["task"]);
        $id = isset($_GET["id"]) ? intval($_GET["id"]) : 0;

        switch($task){
            case 'new':
                aaostracts_addAbstract();
                break;
            case 'edit':
                aaostracts_editAbstract($id);
                break;
            case 'accept':
                aaostracts_changeStatus($id,"Accepted");
                aaostracts_showAbstracts();
                break;
            case 'pending':
                aaostracts_changeStatus($id,"Pending");
                aaostracts_showAbstracts();
                break;
            case 'reject':
                aaostracts_changeStatus($id,"Rejected");
                aaostracts_showAbstracts();
                break;
            case 'assign':
                if(isset($_POST['rid']) && isset($_POST['aid'])){
                    do_action('aaostracts_assign_reviewer', intval($_POST['aid']), $_POST['rid']);
                    aaostracts_assignReviewer(intval($_POST['aid']), $_POST['rid']);
                }
                aaostracts_showAbstracts();
                break;
            case 'delete':
                if(current_user_can(AAOSTRACTS_ACCESS_LEVEL)){
                    aaostracts_deleteAbstract($id, true );
                    aaostracts_showAbstracts();
                }else{
                    _e('You do not have permission to delete abstracts.', 'aaostracts');
                }
                break;
            default :
                if(has_action('aaostracts_page_render')){
                    do_action('aaostracts_page_render');
                }else{
                    aaostracts_showAbstracts();
                }
                break;
        }

    }else{
        aaostracts_showAbstracts();
    }
}

function aaostracts_addAbstract($event_id = null) {
    if($_POST){

        $redirect = (is_super_admin()) ? '?page=aaostracts&tab=abstracts' : '?dashboard';

        // inserts submission to DB
        $id = aaostracts_manage_abstracts(0, 'insert');
        // sends author email if option is enabled
        if(get_option('aaostracts_email_author')){
            $user_id =  get_current_user_id();
            $emailer = new AAOStracts_Emailer($id, $user_id, get_option('aaostracts_submit_templateId'));
            $emailer->send();
        }
         // sends system admin an email if enabled
        if(get_option('aaostracts_email_admin')){
            $super_admins = get_users( array('role'=>'administrator', 'fields'=>'ID') );
            foreach ($super_admins as $super_admin_id) {
                $emailer = new AAOStracts_Emailer($id, $super_admin_id, get_option('aaostracts_admin_templateId'));
                $emailer->send();
            }
        }
        if($_FILES){
            aaostracts_manageAttachments($id, $_FILES, 'insert');
        }
        aaostracts_redirect($redirect);
    }
    else {
        aaostracts_getAddView('Abstract', $event_id);
    }
}

function aaostracts_editAbstract($id) {

    $redirect = (is_super_admin()) ? '?page=aaostracts&tab=abstracts' : '?dashboard';

    if ($_POST) {
        aaostracts_manage_abstracts($id, 'update');

        if($_FILES){
            aaostracts_manageAttachments($id, $_FILES, 'insert');
        }
        aaostracts_redirect($redirect);
    }else{
        aaostracts_getEditView('Abstract', $id);
    }
 }

function aaostracts_getReviewers(){
    ob_start();
    $users = get_users();
    $id = intval($_POST['aid']);
    $abstract = aaostracts_getAbstracts('abstract_id', $id, null);
    $reviewers = array();
    $unassigned = __("-- Not Assigned --",'aaostracts');
    foreach($users as $user){ // ensure user is admin or editor and not the person who submitted this abstract
        if (($user->roles[0] == 'administrator' OR $user->roles[0] == 'editor') && ($user->ID != $abstract[0]->submit_by)){
            $reviewers[] = $user;
        }
    }
    if($abstract[0]->reviewer_id1){
        $reviewer_1 = get_userdata($abstract[0]->reviewer_id1);
        if($reviewer_1){
            $reviewer_1_id = $reviewer_1->ID;
            $reviewer_1_name = $reviewer_1->display_name;
        }
        else{
            $reviewer_1_name = __("Error: Reviewer Deleted",'aaostracts');
        }
    }else{
        $reviewer_1_id = 0;
        $reviewer_1_name = $unassigned;
    }
    if($abstract[0]->reviewer_id2){
        $reviewer_2 = get_userdata($abstract[0]->reviewer_id2);
        if($reviewer_2){
            $reviewer_2_id = $reviewer_2->ID;
            $reviewer_2_name = $reviewer_2->display_name;
        }
        else{
            $reviewer_2_name = __("Error: Reviewer Deleted",'aaostracts');
        }
    }else{
        $reviewer_2_id = 0;
        $reviewer_2_name = $unassigned;
    }
    if($abstract[0]->reviewer_id3){
        $reviewer_3 = get_userdata($abstract[0]->reviewer_id3);
        if($reviewer_3){
            $reviewer_3_id = $reviewer_3->ID;
            $reviewer_3_name = $reviewer_3->display_name;
        }
        else{
            $reviewer_3_name = __("Error: Reviewer Deleted",'aaostracts');
        }
    }else{
        $reviewer_3_id = 0;
        $reviewer_3_name = $unassigned;
    }
    $is_admin = strpos($_SERVER['HTTP_REFERER'], "wp-admin");
    $form_action = ($is_admin) ? $_SERVER['HTTP_REFERER'] . '&task=assign' : '?task=assign';
    ?>
<div class="aaostracts container-fluid">
    <form method="post" id="assign_form" action="<?php echo $form_action;?>">
    <div class="aaostracts modal-body">
        <div>
            <div class="aaostracts col-sm-8"><?php _e('Select Reviewer', 'aaostracts');?></div>
            <div class="aaostracts col-sm-4"><?php _e('Send Email', 'aaostracts');?></div>
        </div>
        <div class="aaostracts row">
            <div class="aaostracts col-sm-3">
                <?php _e('Reviewer', 'aaostracts'); ?> # 1
            </div>
            <div class="aaostracts col-sm-5">
                <select class="aaostracts form-control" name="rid[1]">
                <option value=""><?php echo $unassigned; ?></option>
                <option selected value="<?php echo $reviewer_1_id;?>" disabled style="display:none"><?php echo $reviewer_1_name;?></option>
                    <?php
                    if(is_super_admin()){
                        foreach($reviewers as $reviewer){ ?>
                            <option value="<?php echo $reviewer->ID;?>"><?php echo $reviewer->display_name;?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="aaostracts col-sm-4">
                <input type="checkbox" class="wpabs_email" name="aaostracts_email_reviewer1" value="true">
            </div>
        </div>
        <div class="aaostracts row">
            <div class="aaostracts col-sm-3">
                <?php _e('Reviewer', 'aaostracts');?> # 2
            </div>
            <div class="aaostracts col-sm-5">
                <select class="aaostracts form-control" name="rid[2]">
                    <option value=""><?php echo $unassigned; ?></option>
                    <option selected value="<?php echo $reviewer_2_id;?>" disabled style="display:none"><?php echo $reviewer_2_name;?></option>
                        <?php
                        if(is_super_admin()){
                            foreach($reviewers as $reviewer){ ?>
                                <option value="<?php echo $reviewer->ID;?>"><?php echo $reviewer->display_name;?></option>
                            <?php
                            }
                        }
                        ?>
                </select>
            </div>
            <div class="aaostracts col-sm-4">
                <input type="checkbox" class="wpabs_email" name="aaostracts_email_reviewer2" value="true">
            </div>
        </div>
        <div class="aaostracts row">
            <div class="aaostracts col-sm-3">
                <?php _e('Reviewer', 'aaostracts');?> # 3
            </div>
            <div class="aaostracts col-sm-5">
                <select class="aaostracts form-control" name="rid[3]">
                    <option value=""><?php echo $unassigned; ?></option>
                    <option selected value="<?php echo $reviewer_3_id;?>" disabled style="display:none"><?php echo $reviewer_3_name;?></option>
                        <?php
                        if(is_super_admin()){
                            foreach($reviewers as $reviewer){ ?>
                                <option value="<?php echo $reviewer->ID;?>"><?php echo $reviewer->display_name;?></option>
                            <?php
                            }
                        }
                        ?>
                </select>
            </div>
            <div class="aaostracts col-sm-4">
                 <input type="checkbox" class="wpabs_email" name="aaostracts_email_reviewer3" value="true">
            </div>
        </div>
        <br>
        <div class="aaostracts col-xs-12">
            <input type="hidden" id="aid" name="aid" value="<?php echo $id; ?>">
        </div>
    </div>

    </form>
</div>
<?php
    $html = ob_get_contents();
    ob_end_clean();
    echo apply_filters('aaostracts_get_reviewers', $html);
die();
}

function aaostracts_assignReviewer($aid, $rid){
    global $wpdb;
    $wpdb->show_errors();
    $abstract_id = intval($aid);
    $reviewers = $rid;
    foreach((Array)$reviewers AS $key => $reviewer_id){
        if($key < 4){
            if(is_numeric($reviewer_id) && $reviewer_id > 0){
            $wpdb->query("UPDATE ".$wpdb->prefix."aaostracts_abstracts
                      SET reviewer_id" .$key. " = " . $reviewer_id . "
                      WHERE abstract_id = " . $abstract_id);
            }else{ // remove
                $wpdb->query("UPDATE ".$wpdb->prefix."aaostracts_abstracts
                          SET reviewer_id" .$key. " = ''
                          WHERE abstract_id = " . $abstract_id);
            }
        }
        //send email
        if(isset($_POST['aaostracts_email_reviewer'.$key]) && $reviewer_id > 0){
            $emailer = new AAOStracts_Emailer($abstract_id, $reviewer_id, get_option('aaostracts_assignment_templateId'));
            $emailer->send();
        }
    }
}

function aaostracts_changeStatus($id, $status){
    global $wpdb;
    $data = array('status' => $status);
    $where = array('abstract_id' => $id);
    $wpdb->show_errors();
    $wpdb->update($wpdb->prefix."aaostracts_abstracts", $data, $where);

    if(get_option('aaostracts_status_notification')){
        $abstract = aaostracts_getAbstracts('abstract_id', $id, 'ARRAY_N');
        $templateId = ($status == "Approved" || $status == "Accepted") ? get_option('aaostracts_approval_templateId') : get_option('aaostracts_rejected_templateId');
        if($status != 'Pending'){
            $emailer = new AAOStracts_Emailer($id, $abstract[0]->submit_by, $templateId);
            $success = $emailer->send();
        }
        if($status != 'Pending' && $success){
            aaostracts_showMessage('Notification email was sent successfully', 'alert-success');
        }

    }
}

function aaostracts_deleteAbstract($id, $message){
    aaostracts_manage_abstracts($id, 'delete');
    if($message){
        aaostracts_showMessage("Abstract ID ". $id . " was successfully deleted", 'alert-success');
    }
}

function aaostracts_showAbstracts(){ ?>
    <div class="aaostracts container-fluid aaostracts-admin-container">
        <h3><?php echo apply_filters('aaostracts_title_filter', __('Abstracts','aaostracts'), 'abstracts');?>  <a href="?page=aaostracts&tab=abstracts&task=new" role="button" class="aaostracts btn btn-primary"><?php _e('Add New', 'aaostracts');?></a></h3>
    </div>
    <div class="aaostracts-assign-modal"></div>
    <form id="showsAbstracts" name="abstracts_list" method="get">
        <input type="hidden" name="page" value="aaostracts" />
        <input type="hidden" name="tab" value="abstracts" />
           <?php
                $showAbtracts = new AAOStract_Abstracts_Table();
                $showAbtracts ->prepare_items();
                $showAbtracts ->display();
            ?>
    </form>
    <?php
}