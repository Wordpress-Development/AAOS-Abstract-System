<?php
defined('ABSPATH') or die("ERROR: You do not have permission to access this page");

if(!class_exists('AAOStract_Abstracts_Table')){
    require_once( AAOSTRACTS_PLUGIN_DIR . 'inc/aaostracts_classes.php' );
}
if(!class_exists('AAOStracts_Emailer')){
    require_once( AAOSTRACTS_PLUGIN_DIR . 'inc/aaostracts_emailer.php' );
}

if(is_admin() && isset($_GET['tab']) && ($_GET["tab"]=="reviews")){
    if(isset($_GET['task'])){
        $task = sanitize_text_field($_GET['task']);
        $id = intval($_GET['id']);

        switch($task){
            case 'new':
                aaostracts_addReview($id);
                break;
            case 'edit':
                aaostracts_editReview($id);
                break;
            case 'view':
                aaostracts_viewReviews($id);
                break;
            case 'delete':
                aaostracts_deleteReview($id, true);
            default :
                aaostracts_showReviews();
                break;
        }
    }else{
        aaostracts_showReviews();
    }
}

function aaostracts_addReview($abstract_id) {
    global $wpdb;
    if($_POST){

        $redirect = (is_super_admin()) ? '?page=aaostracts&tab=reviews' : '?dashboard';

        $aid = intval($abstract_id);
        $user_id = get_current_user_id();
        $abs_status = (isset($_POST['abs_status'])) ? sanitize_text_field($_POST['abs_status']) : '';
        $abs_relevance = (isset($_POST['abs_relevance'])) ? sanitize_text_field($_POST['abs_relevance']) : '';
        $abs_quality = (isset($_POST['abs_quality'])) ? sanitize_text_field($_POST['abs_quality']) : '';
        $abs_comments = (isset($_POST['abs_comments'])) ? wp_kses_post($_POST['abs_comments']) : '';
        $abs_recommendation = (isset($_POST['abs_recommendation'])) ? sanitize_text_field($_POST['abs_recommendation']) : '';
        $review_time = current_time( 'mysql' );

        $data = array(
            'abstract_id' => $aid,
            'user_id' => $user_id,
            'quality' => $abs_quality,
            'relevance' => $abs_relevance,
            'recommendation' => $abs_recommendation,
            'status' => $abs_status,
            'comments' => $abs_comments,
            'review_date' => $review_time,
            'customized' => 0
        );

        $filter_data = apply_filters('aaostracts_add_review', $data, $_POST, $aid);

        $wpdb->show_errors();
        $wpdb->insert($wpdb->prefix."aaostracts_reviews", $filter_data);

        // update abstracts with status from review (if enabled)
        if(get_option('aaostracts_sync_status')){
            $wpdb->update($wpdb->prefix."aaostracts_abstracts", array('status' => $abs_status), array('abstract_id' => $aid));
        }

        // notify author if enabled
        if(get_option('aaostracts_review_notification')){
            aaostracts_send_notification($aid);
        }

        aaostracts_redirect($redirect);
    }
    else{
        aaostracts_getAddView('Review', $abstract_id);
    }
 }

function aaostracts_editReview($id) {
    global $wpdb;
    if($_POST){

        $redirect = (is_super_admin()) ? '?page=aaostracts&tab=reviews' : '?dashboard';

        $aid = (isset($_POST['abs_id'])) ? intval($_POST['abs_id']) : '';
        $abs_status = (isset($_POST['abs_status'])) ? sanitize_text_field($_POST['abs_status']) : '';
        $abs_relevance = (isset($_POST['abs_relevance'])) ? sanitize_text_field($_POST['abs_relevance']) : '';
        $abs_quality = (isset($_POST['abs_quality'])) ? sanitize_text_field($_POST['abs_quality']) : '';
        $abs_comments = (isset($_POST['abs_comments'])) ? wp_kses_post($_POST['abs_comments']) : '';
        $abs_recommendation = (isset($_POST['abs_recommendation'])) ? sanitize_text_field($_POST['abs_recommendation']) : '';
        $review_date = current_time( 'mysql' );

        $data = array(
            'quality' => $abs_quality,
            'relevance' => $abs_relevance,
            'recommendation' => $abs_recommendation,
            'status' => $abs_status,
            'comments' => $abs_comments,
            'review_date' => $review_date,
            'customized' => 0
        );

        $filter_data = apply_filters('aaostracts_edit_review', $data, $_POST);
        $wpdb->show_errors();
        $wpdb->update($wpdb->prefix."aaostracts_reviews", $filter_data, array('review_id' => intval($id)));

        // update abstracts with status - if enabled
        if(get_option('aaostracts_sync_status')){
            $wpdb->update($wpdb->prefix."aaostracts_abstracts", array('status' => $abs_status), array('abstract_id' => $aid));
        }

        // notify author if enabled
        if(get_option('aaostracts_review_notification')){
            aaostracts_send_notification($aid);
        }
        aaostracts_redirect($redirect);
    }
    else{
        aaostracts_getEditView("Review", $id);
    }
 }

function aaostracts_send_notification($aid){
    // sends author email if option is enabled
    if(get_option('aaostracts_review_notification')){
        $abstract = aaostracts_getAbstracts('abstract_id', $aid, ARRAY_A);
        $user_id =  $abstract[0]->submit_by;
        $emailer = new AAOStracts_Emailer($aid, $user_id, get_option('aaostracts_reviewed_templateId'));
        $emailer->send();
    }
 }

function aaostracts_deleteReview($id, $showMessage) {
    global $wpdb;
    $wpdb->show_errors();
    $wpdb->query("DELETE FROM " . $wpdb->prefix."aaostracts_reviews" . " WHERE review_id = " . $id);
    if($showMessage){
        aaostracts_showMessage("Review $id successfully deleted", 'alert-success');
    }
}

function aaostracts_viewReviews($id){ ?>
<?php ob_start(); ?>
<div class="aaostracts container-fluid aaostracts-admin-container">
    <h4><?php echo apply_filters('aaostracts_title_filter', __('Reviews for Abstract ID','aaostracts'), 'events'); ?>: <?php echo $id; ?>
    <a href="?page=aaostracts&tab=reviews&task=new&id=<?php echo $id; ?>" class="aaostracts btn btn-primary" /><?php _e('Add New', 'aaostracts'); ?></a>
    <a href="?page=aaostracts&tab=reviews" class="aaostracts btn btn-primary" /><?php _e('All Reviews', 'aaostracts'); ?></a></h4>
</div>

    <form id="viewReviews" method="get">
        <input type="hidden" name="page" value="aaostracts" />
        <input type="hidden" name="tab" value="reviews" />
        <?php
        $viewReviews = new AAOStract_singleItem_Reviews_Table($id);
        $viewReviews->prepare_items();
        $viewReviews->display(); ?>
    </form>
    <?php
    $html = ob_get_contents();
    ob_end_clean();
    echo apply_filters('aaostracts_view_single_review', $html, $id);
}

function aaostracts_showReviews(){ ?>
    <?php ob_start(); ?>
    <form id="showReviews" method="get">
        <input type="hidden" name="page" value="aaostracts" />
        <input type="hidden" name="tab" value="reviews" />
        <?php
        $showReviews = new AAOStract_Reviews_Table();
        $showReviews ->prepare_items();
        $showReviews ->display(); ?>
    </form>
    <?php
    $html = ob_get_contents();
    ob_end_clean();
    echo apply_filters('aaostracts_view_all_reviews', $html);
}

