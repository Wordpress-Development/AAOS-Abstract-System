<?php
defined('ABSPATH') or die("ERROR: You do not have permission to access this page");

function aaostracts_save_option($option) {
    if ($option == 'aaostracts_permitted_attachments') {
        $_POST['options'][$option] = str_replace(" ", "", $_POST['options'][$option]);
    }else if($option == 'aaostracts_author_instructions'){
        $_POST['options'][$option] = wp_kses_post($_POST['options'][$option]);
    }
    else if($option == 'aaostracts_terms_conditions'){
        $_POST['options'][$option] = wp_kses_post($_POST['options'][$option]);
    }
    update_option($option, $_POST['options'][$option]);
}

if ($_POST) {
    foreach ($_POST['options'] as $option => $value) {
        aaostracts_save_option($option);
    }
    ?>
        <div id="message" class="updated fade"><p><strong><?php _e('Settings saved', 'aaostracts'); ?></strong></p></div>
    <?php
}
?>

<div class="aaostracts container-fluid aaostracts-admin-container">
    <form method="post" id="aaostracts_settings" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <h3><?php _e('Settings', 'aaostracts'); ?> <input type="submit" name="Submit" class="aaostracts btn btn-primary" value="<?php _e('Save Changes', 'aaostracts'); ?>" /></h3>

    <div class="aaostracts row">

        <div class="aaostracts col-xs-4">
            <div class="aaostracts panel panel-primary">
                <div class="aaostracts panel-heading">
                    <h6 class="aaostracts panel-title"><?php _e('Abstracts Configuration', 'aaostracts'); ?></h6>
                </div>

                <div class="aaostracts panel-body">

                    <div class="aaostracts form-group col-xs-12">
                        <?php _e('Show Author Fields', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Use this setting to hide the author box completely from the submission page.', 'aaostracts'); ?>">
                            <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                        </span>
                        <select name="options[aaostracts_show_author]" class="aaostracts pull-right">
                            <option value="1" <?php selected(get_option('aaostracts_show_author'), 1); ?>><?php _e('Yes', 'aaostracts'); ?></option>
                            <option value="0"  <?php selected(get_option('aaostracts_show_author'), 0); ?>><?php _e('No', 'aaostracts'); ?></option>
                        </select>
                    </div>

                    <div class="aaostracts form-group col-xs-12">
                        <?php _e('Show Attachment Uploads', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Use this setting to hide the attachment box completely from the submission page.', 'aaostracts'); ?>">
                            <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                        </span>
                        <select name="options[aaostracts_show_attachments]" class="aaostracts pull-right">
                            <option value="1" <?php selected(get_option('aaostracts_show_attachments'), 1); ?>><?php _e('Yes', 'aaostracts'); ?></option>
                            <option value="0"  <?php selected(get_option('aaostracts_show_attachments'), 0); ?>><?php _e('No', 'aaostracts'); ?></option>
                        </select>
                    </div>

                    <div class="aaostracts form-group col-xs-12">
                        <?php _e('Show Presenter Preferences', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Use this setting to hide the presenter preference box completely from the submission and edit pages.', 'aaostracts'); ?>">
                            <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                        </span>
                        <select name="options[aaostracts_show_presenter]" class="aaostracts pull-right">
                            <option value='1' <?php selected(get_option('aaostracts_show_presenter'), 1); ?>><?php _e('Yes', 'aaostracts'); ?></option>
                            <option value='0' <?php selected(get_option('aaostracts_show_presenter'), 0); ?>><?php _e('No', 'aaostracts'); ?></option>
                        </select>
                    </div>

                    <div class="aaostracts form-group form-group col-xs-12">
                        <?php _e('Show Abstract Keywords', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Enable this to display the keywords input on the submission page. Enabling this makes keywords required.', 'aaostracts'); ?>">
                            <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                        </span>
                        <select name="options[aaostracts_show_keywords]" class="aaostracts pull-right">
                            <option value="1" <?php selected(get_option('aaostracts_show_keywords'), 1); ?>><?php _e('Yes', 'aaostracts'); ?></option>
                            <option value="0" <?php selected(get_option('aaostracts_show_keywords'), 0); ?>><?php _e('No', 'aaostracts'); ?></option>
                        </select>
                    </div>

                    <div class="aaostracts form-group form-group col-xs-12">
                        <?php _e('Show Terms & Conditions', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Enable this to display your terms and conditions on the submission page. Enabling this makes the checkbox required.', 'aaostracts'); ?>">
                            <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                        </span>
                        <select name="options[aaostracts_show_conditions]" class="aaostracts pull-right">
                            <option value="1" <?php selected(get_option('aaostracts_show_conditions'), 1); ?>><?php _e('Yes', 'aaostracts'); ?></option>
                            <option value="0" <?php selected(get_option('aaostracts_show_conditions'), 0); ?>><?php _e('No', 'aaostracts'); ?></option>
                        </select>
                    </div>

                    <div class="aaostracts form-group col-xs-12">
                        <?php _e('Allow Change Ownership', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Enable this to allow reviewers to change ownership (the author) of a submission (useful if a reviewer submits an abstract on behalf of an author but the option above to enable reviewers to edit abstracts must be enabled for this to work).', 'aaostracts'); ?>">
                                <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                            </span>
                        <select name="options[aaostracts_change_ownership]" class="aaostracts pull-right">
                            <option value="1" <?php selected(get_option('aaostracts_change_ownership'), 1); ?>><?php _e('Yes', 'aaostracts'); ?></option>
                            <option value="0" <?php selected(get_option('aaostracts_change_ownership'), 0); ?>><?php _e('No', 'aaostracts'); ?></option>
                        </select>
                    </div>

                    <div class="aaostracts form-group col-xs-12">
                        <?php _e('Sync Review Status to Abstracts', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Enable this to allow reviewer status selection to update the abstract status. This works best when one reviewer is assigned to the abstract. If more than one reviews are submitted the last one wins.', 'aaostracts'); ?>">
                                <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                            </span>
                        <select name="options[aaostracts_sync_status]" class="aaostracts pull-right">
                            <option value="1" <?php selected(get_option('aaostracts_sync_status'), 1); ?>><?php _e('Yes', 'aaostracts'); ?></option>
                            <option value="0" <?php selected(get_option('aaostracts_sync_status'), 0); ?>><?php _e('No', 'aaostracts'); ?></option>
                        </select>
                    </div>

                    <div class="aaostracts form-group form-group col-xs-12">
                        <?php _e('Maximum Word Count', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Maximum character count allowed in a submission.', 'aaostracts'); ?>">
                            <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                        </span>
                        <input name="options[aaostracts_chars_count]" type="text" id="charscount" value="<?php echo get_option('aaostracts_chars_count'); ?>" class="aaostracts form-control" />
                    </div>

                    <div class="aaostracts form-group form-group col-xs-12">
                        <?php _e('Maximum Attachments', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Set the maximum attachment upload allowed per submission.', 'aaostracts'); ?>">
                                <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                            </span>
                        <input name="options[aaostracts_upload_limit]" type="text" value="<?php echo get_option('aaostracts_upload_limit'); ?>" class="aaostracts form-control" />
                    </div>

                    <div class="aaostracts form-group form-group col-xs-12">
                        <?php _e('Maximum Attachment Size', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Maxmium size allowed for attachments (in bytes).', 'aaostracts'); ?>">
                            <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                        </span>
                        <input name="options[aaostracts_max_attach_size]" type="text" value="<?php echo get_option('aaostracts_max_attach_size'); ?>" class="aaostracts form-control"/>
                    </div>

                    <div class="aaostracts form-group form-group col-xs-12">
                        <?php _e('Permitted Attachments', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('File extentions allowed for uploading (separate extentions with a comma).', 'aaostracts'); ?>">
                                <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                            </span>
                        <input name="options[aaostracts_permitted_attachments]" type="text" id="attachments_permitted" value="<?php echo get_option('aaostracts_permitted_attachments'); ?>" class="aaostracts form-control" />
                    </div>

                    <div class="aaostracts form-group form-group col-xs-12">
                        <?php _e('Set Presenter Preferences', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Set the types of presentation allowed (separated by commas), Eg. Poster, Panel, Round Table', 'aaostracts'); ?>">
                            <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                        </span>
                        <input name="options[aaostracts_presenter_preference]" type="text" value="<?php echo get_option('aaostracts_presenter_preference'); ?>" class="aaostracts form-control"/>
                    </div>

                </div>

            </div>
        </div>

        <div class="aaostracts col-xs-4">
            <div class="aaostracts panel panel-primary">
                <div class="aaostracts panel-heading">
                    <h6 class="aaostracts panel-title"><?php _e('Reviewers Configuration', 'aaostracts'); ?></h6>
                </div>

                <div class="aaostracts panel-body">

                    <div class="aaostracts form-group col-xs-12">
                        <?php _e('Reviewers can Submit Abstracts', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Enable this to allow reviewers to submit new abstracts.', 'aaostracts'); ?>">
                                <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                            </span>
                        <select name="options[aaostracts_reviewer_submit]" class="aaostracts pull-right">
                            <option value="1" <?php selected(get_option('aaostracts_reviewer_submit'), 1); ?>><?php _e('Yes', 'aaostracts'); ?></option>
                            <option value="0" <?php selected(get_option('aaostracts_reviewer_submit'), 0); ?>><?php _e('No', 'aaostracts'); ?></option>
                        </select>
                    </div>

                    <div class="aaostracts form-group col-xs-12">
                        <?php _e('Reviewers can Edit Abstracts', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Enable this to allow reviewers to edit abstracts they are assigned.', 'aaostracts'); ?>">
                            <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                        </span>
                        <select name="options[aaostracts_reviewer_edit]" class="aaostracts pull-right">
                            <option value="1" <?php selected(get_option('aaostracts_reviewer_edit'), 1); ?>><?php _e('Yes', 'aaostracts'); ?></option>
                            <option value="0" <?php selected(get_option('aaostracts_reviewer_edit'), 0); ?>><?php _e('No', 'aaostracts'); ?></option>
                        </select>
                    </div>

                    <div class="aaostracts form-group col-xs-12">
                        <?php _e('Enable Blind Reviews', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Enable this to hide author information from reviewers (does not apply to admin).', 'aaostracts'); ?>">
                            <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                        </span>
                        <select name="options[aaostracts_blind_review]" class="aaostracts pull-right">
                            <option value="1" <?php selected(get_option('aaostracts_blind_review'), 1); ?>><?php _e('Yes', 'aaostracts'); ?></option>
                            <option value="0"  <?php selected(get_option('aaostracts_blind_review'), 0); ?>><?php _e('No', 'aaostracts'); ?></option>
                        </select>
                    </div>

                    <div class="aaostracts form-group col-xs-12">
                        <?php _e('Show Reviewer Comments', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Disable this to hide reviewer comments from Authors.', 'aaostracts'); ?>">
                            <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                        </span>
                        <select name="options[aaostracts_show_reviews]" class="aaostracts pull-right">
                            <option value="1" <?php selected(get_option('aaostracts_show_reviews'), 1); ?>><?php _e('Yes', 'aaostracts'); ?></option>
                            <option value="0"  <?php selected(get_option('aaostracts_show_reviews'), 0); ?>><?php _e('No', 'aaostracts'); ?></option>
                        </select>
                    </div>

                </div>
            </div>
        </div>

        <div class="aaostracts col-xs-4">
            <div class="aaostracts panel panel-primary">
                <div class="aaostracts panel-heading">
                    <h6 class="aaostracts panel-title"><?php _e('Email Configuration', 'aaostracts'); ?></h6>
                </div>

                <div class="aaostracts panel-body">

                    <div class="aaostracts form-group col-xs-12">
                        <?php _e('Email Admin on Submission', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Enable this to send an email to the site admin on submission of a new abstract.', 'aaostracts'); ?>">
                            <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                        </span>
                        <select name="options[aaostracts_email_admin]" class="aaostracts pull-right">
                            <option value="1" <?php selected(get_option('aaostracts_email_admin'), 1); ?>><?php _e('Yes', 'aaostracts'); ?></option>
                            <option value="0" <?php selected(get_option('aaostracts_email_admin'), 0); ?>><?php _e('No', 'aaostracts'); ?></option>
                        </select>
                    </div>

                    <div class="aaostracts form-group col-xs-12">
                        <?php _e('Email Author on Submission', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Enable this to send an email to authors when they submit an abstract (email is sent to Author\'s email).', 'aaostracts'); ?>">
                                <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                        </span>
                        <select name="options[aaostracts_email_author]" class="aaostracts pull-right">
                            <option value="1" <?php selected(get_option('aaostracts_email_author'), 1); ?>><?php _e('Yes', 'aaostracts'); ?></option>
                            <option value="0" <?php selected(get_option('aaostracts_email_author'), 0); ?>><?php _e('No', 'aaostracts'); ?></option>
                        </select>
                    </div>

                    <div class="aaostracts form-group col-xs-12">
                         <?php _e('Email Author on Status Change', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Enable this to send an email to authors when their abstracts has been approved or rejected (email is sent to submitter\'s email only).', 'aaostracts'); ?>">
                            <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                            </span>
                        <select name="options[aaostracts_status_notification]" class="aaostracts pull-right">
                            <option value="1" <?php selected(get_option('aaostracts_status_notification'), 1); ?>><?php _e('Yes', 'aaostracts'); ?></option>
                            <option value="0" <?php selected(get_option('aaostracts_status_notification'), 0); ?>><?php _e('No', 'aaostracts'); ?></option>
                        </select>
                    </div>

                    <div class="aaostracts form-group col-xs-12">
                         <?php _e('Email Author on Review Submission', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Enable this to send an email to authors when their abstracts has been reviewed (email is sent to submitter\'s email only).', 'aaostracts'); ?>">
                            <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                            </span>
                        <select name="options[aaostracts_review_notification]" class="aaostracts pull-right">
                            <option value="1" <?php selected(get_option('aaostracts_review_notification'), 1); ?>><?php _e('Yes', 'aaostracts'); ?></option>
                            <option value="0" <?php selected(get_option('aaostracts_review_notification'), 0); ?>><?php _e('No', 'aaostracts'); ?></option>
                        </select>
                    </div>

                </div>
            </div>
        </div>



        <div class="aaostracts col-xs-8">
            <div class="aaostracts panel panel-primary">
                <div class="aaostracts panel-heading">
                    <h6 class="aaostracts panel-title"><?php _e('Frontend Dashboard Configuration', 'aaostracts'); ?></h6>
                </div>

                <div class="aaostracts panel-body">

                    <div class="aaostracts form-group col-xs-6">
                        <?php _e('Allow WP Admin Access', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Disables users from accessing Wordpress Admin dashboard. Enable this if you want to allow frontend access only.', 'aaostracts'); ?>">
                                <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                            </span>
                        <select name="options[aaostracts_frontend_dashboard]" class="aaostracts pull-right">
                            <option value="1" <?php selected(get_option('aaostracts_frontend_dashboard'), 1); ?>><?php _e('Yes', 'aaostracts'); ?></option>
                            <option value="0" <?php selected(get_option('aaostracts_frontend_dashboard'), 0); ?>><?php _e('No', 'aaostracts'); ?></option>
                        </select>
                    </div>

                    <div class="aaostracts form-group col-xs-6">
                        <?php _e('Show WP Admin Bar', 'aaostracts'); ?>
                        <span class="aaostracts pull-right " data-tip="<?php _e('Disables users from seeing the Wordpress Admin Bar after sign in.', 'aaostracts'); ?>">
                            <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                        </span>
                        <select name="options[aaostracts_show_adminbar]" class="aaostracts pull-right">
                            <option value="1" <?php selected(get_option('aaostracts_show_adminbar'), 1); ?>><?php _e('Yes', 'aaostracts'); ?></option>
                            <option value="0"  <?php selected(get_option('aaostracts_show_adminbar'), 0); ?>><?php _e('No', 'aaostracts'); ?></option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="aaostracts col-xs-8">
            <div class="aaostracts panel panel-primary">
                <div class="aaostracts panel-heading">
                    <h6 class="aaostracts panel-title">
                        <?php _e('Author Instructions', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Enter specific instructions for authors to follow for submissions', 'aaostracts'); ?>">
                            <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                        </span>
                    </h6>
                </div>
                <div>
                    <?php
                        $settings = array( 'media_buttons' => false, 'textarea_name' => 'options[aaostracts_author_instructions]', 'wpautop'=>true, 'dfw' => true, 'editor_height' => 100, 'quicktags' => false);
                        wp_editor(stripslashes(get_option('aaostracts_author_instructions')), 'aaostracts_author_instructions', $settings);
                    ?>
                </div>
            </div>

             <div class="aaostracts panel panel-primary">
                <div class="aaostracts panel-heading">
                    <h6 class="aaostracts panel-title">
                        <?php _e('Terms and Conditions', 'aaostracts'); ?>
                        <span class="settings_tip" data-tip="<?php _e('Enter your terms and contitions for authors to agree.', 'aaostracts'); ?>">
                            <img src="<?php echo plugins_url('images/settings_help.png', __FILE__); ?>" height="16" width="16" alt="Help">
                        </span>
                    </h6>
                </div>
                <div>
                    <?php
                        $settings = array( 'media_buttons' => false, 'textarea_name' => 'options[aaostracts_terms_conditions]', 'wpautop'=>true, 'dfw' => true, 'editor_height' => 100, 'quicktags' => false);
                        wp_editor(stripslashes(get_option('aaostracts_terms_conditions')), 'aaostracts_terms_conditions', $settings);
                    ?>
                </div>
            </div>
        </div>

    </div>

    </form>
</div>